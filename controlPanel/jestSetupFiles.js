// Polyfills and Other Patches /////////////////////////////////////////////////////////////////////////////

require( 'jest-canvas-mock' )

// Import polyfill for fetch() method
if( typeof fetch !== 'function' ) {
    global.fetch = require( 'node-fetch-polyfill' )
}

// Import polyfill for Object.fromEntries() method
if( typeof Object.fromEntries !== 'function' ) {
    Object.fromEntries = require( 'object.fromentries' )
}

// Testing library ////////////////////////////////////////////////a////////////////////////////////////////////////
require('@testing-library/jest-dom/dist/extend-expect')
require( '@testing-library/dom' )
require( '@testing-library/jest-dom' )

// JQuery & JQuery-UI ////////////////////////////////////////////////////////////////////////////////////////////////
global.$ = global.jQuery = require( './js/libraries/jquery-3.6.0' )
require( 'jquery-ui-dist/jquery-ui' )

// Lodash ////////////////////////////////////////////////////////////////////////////////////////////////
global._ = require( './js/libraries/external/lodash-4.17.15/lodash.js' )

// JSZip ////////////////////////////////////////////////////////////////////////////////////////////////
global.JSZip = require( './js/libraries/jszip-3.7.1/dist/jszip' )

// is_js and Must.js ////////////////////////////////////////////////////////////////////////////////////////////////
global.is = require( './js/libraries/external/is-0.9.0/is' )
require( 'must' )

// Internal Modules ////////////////////////////////////////////////////////////////////////////////////////////////
global.fileManifest = require( './js/fileManifest' )
global.imageManifest = require( './js/imageManifest' )
global.groupManifest = require( './js/groupManifest' )
global.packager = require( './js/packager' )
global.controlPanel = require( './js/controlPanel' )
global.selectionCounts = controlPanel.selectionCounts

// Jest Helper Functions ///////////////////////////////////////////////////////////////////////////////////

const {Helper} = require( './tests/helperFunctions' )
global.Helper = Helper

// JestUtils //////////////////////////////////////////////////////////////////////////////////////////////

const jestConsole = require( './js/libraries/jest-utils-1.0/jest-console' )
global.expectTable = jestConsole.expectTable
// global.expectConsoleHistory = jestConsole.expectConsoleHistory
// global.clearConsoleHistory = jestConsole.clearConsoleHistory

const jestDom = require( './js/libraries/jest-utils-1.0/jest-dom' )
global.initializeDomWithSvg = jestDom.initializeDomWithSvg
global.writeDomToFile = jestDom.writeDomToFile
