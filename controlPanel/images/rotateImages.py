from typing import Any

from PIL import Image
import os as os
from pathlib import Path
from termcolor import colored

# Get file paths for three image categories.
alcoholPaths = []
softDrinkPaths = []
surrenderPaths = []
for root, directories, files in os.walk('./', topdown=True):
    for fileName in files:
        notInASubfolder = len(root.split('/')) <= 2
        filePath = os.path.join(root, fileName)
        if 'alcohol' in fileName and notInASubfolder :
            alcoholPaths.append(filePath)
        if 'softDrink' in fileName and notInASubfolder:
            softDrinkPaths.append(filePath)
        if 'surrender' in fileName and notInASubfolder:
            surrenderPaths.append(filePath)


# Interim feedback to user
print('\n')
print(colored('Images found:\n', 'green'))
print(alcoholPaths)
print(softDrinkPaths)
print(surrenderPaths)
print(colored('\nRotating images...', 'green'))


# Rotate
def rotateAndSave(pathList, degrees=12.9):

    direction = ''
    if degrees < 0:
        direction = 'right'
    if degrees >= 0:
        direction = 'left'

    for imagePath in pathList:

        # Open
        image = Image.open(imagePath)

        # Convert to RGB, just in case
        RGBImage = image.convert('RGB')  # some jpgs have alpha in their metadata, even though jpgs can't be transparent

        # Rotate
        white = (255, 255, 255)
        rotatedImage = RGBImage.rotate(degrees, expand=True, fillcolor=white)

        # Get image name
        imageFolderName = imagePath.split('/')[-2]
        imageNameWithExtension = imagePath.split('/')[-1]
        imageName = imageNameWithExtension.split('.')[0]

        # Create subdirectory for rotated images if it doesn't exist
        Path(f'./{imageFolderName}/{direction}').mkdir(exist_ok=True)

        # Save
        rotatedImage.save(f'./{imageFolderName}/{direction}/{imageName}-{direction}.jpg')



# Rotate left
rotateAndSave(alcoholPaths, 12.9)
rotateAndSave(softDrinkPaths, 12.9 )
rotateAndSave(surrenderPaths, 12.9)

# Rotate right
rotateAndSave(alcoholPaths, -12.9)
rotateAndSave(softDrinkPaths, -12.9 )
rotateAndSave(surrenderPaths, -12.9)

print(colored('\nDone. Images rotated and saved.\n', 'green'))