//// UMD HEAD ////////////////////////////////////////////////////////////////////////
// UMD head and foot patterns adapted from d3.js
( function ( global, factory ) {
    // noinspection JSUnresolvedVariable,JSUnresolvedFunction,NestedConditionalExpressionJS
    typeof exports === 'object' && typeof module !== 'undefined' ? factory( exports ) :
        typeof define === 'function' && define.amd ? define( [ 'exports' ], factory ) :
            ( factory( ( global.packager = global.packager || {} ) ) )
}( this, ( /**
 *
 * @param exports
 */
function ( exports ) {
    'use strict'
//////////////////////////////////////////////////////////////////////////////////////

    const parameters = {
        APBMSequenceLength: 150,
        AATSequenceLength: 120
    }

    let zipFile
    let packageInstance

    async function download() {
        Package.disableDownloadButton()
        packageInstance = new packager.Package()
        zipFile = packageInstance.zip
        await packageInstance.buildAndDownload()
        return packageInstance
    }


    // ALL CONDITIONS DOWNLOAD THE SAME FILES
    // [NOTE1] Comment out this block and use the next block (currently commented out) for condition-specific downloads
    const orientations = {
        '1a': {
            'alcohol': [ 'left', 'right' ],
            'softDrink': [ 'left', 'right' ],
            'surrender': [ 'left', 'right' ]
        },
        '1b': {
            'alcohol': [ 'left', 'right' ],
            'softDrink': [ 'left', 'right' ],
            'surrender': [ 'left', 'right' ]
        },
        '2a': {
            'alcohol': [ 'left', 'right' ],
            'softDrink': [ 'left', 'right' ],
            'surrender': [ 'left', 'right' ]
        },
        '2b': {
            'alcohol': [ 'left', 'right' ],
            'softDrink': [ 'left', 'right' ],
            'surrender': [ 'left', 'right' ]
        },
        '3a': {
            'alcohol': [ 'left', 'right' ],
            'softDrink': [ 'left', 'right' ],
            'surrender': [ 'left', 'right' ]
        },
        '3b': {
            'alcohol': [ 'left', 'right' ],
            'softDrink': [ 'left', 'right' ],
            'surrender': [ 'left', 'right' ]
        }

    }
    // CONDITION-SPECIFIC DOWNLOADS
    // Downloads only the images that are relevant to conditions
    // Disabled, because having a single Inquisit file requires all files to be present
    // const orientations = {
    //     '1a': {
    //         'alcohol': [ 'left', 'right' ],
    //         'softDrink': [ 'left', 'right' ],
    //         'surrender': [ 'left', 'right' ]
    //     },
    //     '1b': {
    //         'alcohol': [ 'right', 'left' ],
    //         'softDrink': [ 'right', 'left' ],
    //         'surrender': [ 'right', 'left' ]
    //     },
    //     '2a': {
    //         'alcohol': [ 'left' ],
    //         'softDrink': [ 'right' ],
    //         'surrender': [ 'right', 'left' ]
    //     },
    //     '2b': {
    //         'alcohol': [ 'right' ],
    //         'softDrink': [ 'left' ],
    //         'surrender': [ 'left', 'right' ]
    //     },
    //     '3a': {
    //         'alcohol': [ 'left' ],
    //         'softDrink': [ 'left', 'right' ],
    //         'surrender': [ 'right' ]
    //     },
    //     '3b': {
    //         'alcohol': [ 'right' ],
    //         'softDrink': [ 'right', 'left' ],
    //         'surrender': [ 'left' ]
    //     }
    //
    // }




    class Package {

        constructor() {
            // Reset variables
            this.sequenceContents = new Map()

            // Create an archive
            this.zip = new JSZip()

        }


        /**
         * Downloads files as a zip file. Recursive function.
         * Implementation adapted from https://stackoverflow.com/a/38210534/3102060
         * @returns {Promise<void>}
         */
        async buildAndDownload() {

            await this.build()

            // Download the zip
            setTimeout( () => {  // This timeout block is workaround to prevent missing the last inquisitURL in
                // the first downloaded zip (currently, there are two zip files downloaded)
                this.download()
            }, 1000 )

        }


        /**
         * Adds static files (images and other formats) and dynamically generated files to zip.
         * @returns {Promise<void>}
         */
        async build() {

            // Create directories in the zip
            this.createDirectoriesInZip()

            await this.addFixedFilesToZip()

            await this.addSelectedImagesToZip()

            // Add dynamically generated files to the zip, such as sequence files
            this.generateDynamicFilesAndAddToZip()

        }


        /**
         * Traverses the file tree and creates directories in the zip that mirrors the directory hierarchy in file tree
         * @param callback {function(object, object, array)} Callback function that initializes with session
         * object, task object, and a string array for URLs. These are parameters that refer to each `session`,
         * each `task` in each session, and to each file `URL` in each task.
         * @returns {function(object, object, array<string>)}
         * @example
         * const myPackage = new packager.Package
         * myPackage.walkFileTree( ( session, task, taskURLs) => { ... })
         **/
        walkFileTree( callback = () => {} ) {

            Object.values( fileManifest.Manifest.getTree() ).forEach( session => {

                this.zip.folder( session.directoryName )
                Object.values( session.tasks ).forEach( ( task ) => {
                    return callback( session, task, task.fixedFileURLs )
                } )

            } )

        }



        /**
         * Creates directories in the zip that mirrors the directory hierarchy in file tree.
         * @returns {void}
         * @example
         * const myPackage = new packager.Package
         * myPackage.createDirectoriesInZip()
         * Object.keys( myPackage.zip.files )  //-> ['01 - Pre-assessment and first session/', ...]
         **/
        createDirectoriesInZip() {

            this.walkFileTree( ( training, task ) => {
                this.zip.folder( `${ training.directoryName }/${ task.directoryName }` )
            } )

        }


        /**
         * Adds all files specified in the file tree to appropriate directories in the zip.
         * @returns {void}
         **/
        async addFixedFilesToZip() {

            this.walkFileTree( ( session, task, fixedFileURLs ) => {

                const directoryPath = `${ session.directoryName }/${ task.directoryName }`

                fixedFileURLs.forEach( fixedFileURL => {

                    // Get file contents
                    Package.retrieveFileAsArrayBuffer( fixedFileURL, async ( data, url ) => {

                        const fileName = Package.getFilenameFromURL( url )
                        this.zip.file( `${ directoryPath }/${ fileName }`, data )

                    } )

                } )

            } )

        }


        async addSelectedImagesToZip() {


            this.walkFileTree( ( session, task, fixedFileURLs ) => {


                const directoryPath = `${ session.directoryName }/${ task.directoryName }`
                const selectedImageURLs = Package.getStaticURLsToDownload( session.key, task.key, 'images' )

                selectedImageURLs.forEach( imageURL => {

                    // Get file contents
                    Package.retrieveFileAsArrayBuffer( imageURL, async ( data, url ) => {

                        // Rename image files (so that they are ordered, such as alcohol-1, alcohol-2, etc)
                        let fileName = this.assignFileName( url, directoryPath )

                        this.zip.file( `${ directoryPath }/${ fileName }`, data )

                    } )

                } )

            } )

        }


        generateDynamicFilesAndAddToZip() {

            // Add condition and id information
            const { name, content } = Package.prepareFolderInformationFileFor( 'root' )
            this.zip.file( `${ name }`, content )

            // Add unzip note to root directory of the zip
            const unzipNoteFileName = `Please don't forget to unzip this folder before running an experiment.txt`
            this.zip.file( `${ unzipNoteFileName }`, '' )


            this.walkFileTree( ( session, task, fixedFileURLs ) => {

                const directoryPath = `${ session.directoryName }/${ task.directoryName }`

                if( task.key.includes( 'APBM' ) ) {

                    // Add condition and id information
                    const { name, content } = Package.prepareFolderInformationFileFor( 'ApBM' )
                    this.zip.file( `${ directoryPath }/${ name }`, content )

                    // Prepare the .bat file
                    // const batchFileContent = Package.prepareBatchFileFor( 'ApBM' )
                    // this.zip.file( `${ directoryPath }/silentRun.bat`, batchFileContent )

                    // Add sequence files based on condition
                    this.prepareSequenceFiles()
                    this.zip.file( `${ directoryPath }/sequence-part1.iqx`, this.sequenceContents.get( 'ApBMSequence1' ) )
                    this.zip.file( `${ directoryPath }/sequence-part2.iqx`, this.sequenceContents.get( 'ApBMSequence2' ) )

                }

                if( task.key.includes( 'AAT' ) ) {

                    // Add condition and id information
                    const { name, content } = Package.prepareFolderInformationFileFor( 'AAT' )
                    this.zip.file( `${ directoryPath }/${ name }`, content )

                    // Prepare the .bat file
                    // const batchFileContent = Package.prepareBatchFileFor( 'AAT' )
                    // this.zip.file( `${ directoryPath }/silentRun.bat`, batchFileContent )

                    // Add sequence files based on condition
                    this.prepareSequenceFiles()
                    this.zip.file( `${ directoryPath }/sequence-part1.iqx`, this.sequenceContents.get( 'AATSequence' ) )  // although there is only one part, it's still part-1 to make the file name compatibility with the script copied from ApBM

                }


            } )

        }


        download() {

            // Increment progress bar
            // These are pseudo-progress steps but the bar will not fill completely. That final step is taken only
            // after real progress.
            _.range(10).forEach(i => {
                setTimeout( () => {
                    const downloadButton = document.querySelector('#downloadButton')
                    if(downloadButton.disabled){
                        Package.incrementProgressBar( 9 )
                    }
                }, 1000 * i)
            })

            // Generate a name for the zip file
            const { cohort, sex, id } = controlPanel.Panel.parseAll()
            const manifest = new groupManifest.Manifest()
            const groupNumber = manifest.getGroupNumber( cohort, sex )
            const zipFileName = `Group ${ groupNumber } - Participant ${ id }.zip`

            this.zip.generateAsync( { type: 'blob' } )
                .then( ( content ) => {

                    saveAs( content, zipFileName )

                    // Fill the progress bar completely
                    Package.incrementProgressBar( 100 )

                    // Remove progress bar and re-enable download button
                    setTimeout( () => {
                        Package.resetAndHideProgressBar()
                        Package.enableDownloadButton()
                    }, 2000 )

                    this.content = content
                } )

        }


        static incrementProgressBar( value ) {

            const incrementValue = !!value
                ? value
                : 100 / this.getStaticURLsToDownload( 'firstTraining', 'APBM' ).length

            const progressBar = document.querySelector( '#progressBar' )
            if( !!progressBar ) {
                progressBar.style = 'visibility:visible;'
                progressBar.value += incrementValue
            }
        }


        static setProgressBarValue( value ) {
            document.querySelector( '#progressBar' ).value = value
        }


        static resetAndHideProgressBar() {
            const progressBar = document.querySelector( '#progressBar' )
            progressBar.value = 0
            progressBar.style = 'visibility:hidden;'
        }


        static enableDownloadButton() {
            const downloadButton = document.querySelector( '#downloadButton' )
            if( !!downloadButton ) downloadButton.disabled = false
        }


        static disableDownloadButton() {
            const downloadButton = document.querySelector( '#downloadButton' )
            if( !!downloadButton ) downloadButton.disabled = true
        }



        /**
         * Takes category or category and orientation and returns the count of how many entries that contain these`
         * exist in the zip file.
         * @param [category] {string}
         * @param [orientation] {string}
         * @param [directoryPath] {string} The directory to count files within. Can be used to limit the scope of
         * counting to a directory instead of the whole zip.
         * @return {number}
         * @example
         * const myPackage = new Package()
         * // Count all entries in zip
         * myPackage.howMany()
         *
         * // Count only those that match the provided category and orientation
         * myPackage.howMany( 'alcohol', 'left' )  //-> 2
         *
         * // Count only those that match the provided category and orientation in a directory
         * myPackage.howMany( 'alcohol', 'left', 'sessionA/task' )  //-> 2
         */
        howMany( category, orientation, directoryPath ) {

            // Validation
            if( !!category ) {category.must.be.a.string()}
            const possibleOrientationValues = [ 'left', 'right', 'straight' ]
            if( !!orientation ) {possibleOrientationValues.must.include( orientation )}

            const zippedFiles = Object.keys( this.zip.files )

            // Conditions //
            // Category is present in path
            // REFACTOR WARNING: Methods such as inferCategoryFromPath are not used here because it is not a path
            // that is being analyzed. This method analyzes single file name strings retrieved from the zip object.
            const categoryIsPresentIn = ( fileName ) => fileName.includes( category )
            // Orientation is present in path
            const orientationIsPresentIn = ( fileName ) => fileName.includes( orientation )
            // Orientation is asked but not present in file name
            const orientationIsStraightIn = ( fileName ) => {
                return !!directoryPath
                    ? categoryIsPresentIn( fileName ) && !fileName.includes( 'left' ) && !fileName.includes( 'right' ) && pathIsPresentIn( fileName )
                    : categoryIsPresentIn( fileName ) && !fileName.includes( 'left' ) && !fileName.includes( 'right' )

            }
            // Path is in file name
            const pathIsPresentIn = ( filePath ) => filePath.includes( directoryPath )


            let noOfMatches

            // Called with no arguments
            if( arguments.length === 0 ) {
                noOfMatches = zippedFiles.filter( () =>
                    noOfMatches = zippedFiles.length  // the number of all files in zip
                ).length
            }

            // Called with category argument
            if( arguments.length === 1 ) {

                // Match category and orientation
                noOfMatches = zippedFiles.filter( fileName =>
                    categoryIsPresentIn( fileName )
                ).length
            }


            // Called with category and orientation arguments
            if( arguments.length === 2 ) {

                let numberOfFound = 0
                // Match category and orientation
                numberOfFound = zippedFiles.filter( fileName =>
                    categoryIsPresentIn( fileName ) && orientationIsPresentIn( fileName )
                ).length

                if( numberOfFound === 0 ) {
                    numberOfFound = zippedFiles.filter( fileName =>
                        orientationIsStraightIn( fileName )
                    ).length
                }

                noOfMatches = numberOfFound

            }

            // Called with category, orientation, and path arguments
            if( arguments.length === 3 ) {

                let numberOfFound = 0
                // Match category and orientation
                numberOfFound = zippedFiles.filter( filePath =>
                    categoryIsPresentIn( filePath ) && orientationIsPresentIn( filePath ) && pathIsPresentIn( filePath )
                ).length

                if( numberOfFound === 0 ) {

                    if( orientation === 'straight' ) {
                        numberOfFound = zippedFiles.filter( fileName =>
                            orientationIsStraightIn( fileName )
                        ).length
                    }
                }

                noOfMatches = numberOfFound

            }




            return noOfMatches
        }


        /**
         * Infers category and orientation from the inputted path string and checks how many files of that category was
         * previously added to the zip. Generates and returns a filename based on this number, the inferred category
         * and the orientation.
         * @param fileURL {string} The URL of the content
         * @param directoryInZip {string} The directory to count files within. Can be used to limit the scope of
         * counting to a directory instead of the whole zip.
         * @return {string}
         * @example
         * const myPackage = new Package()
         *
         * // Add some files to zip
         * myPackage.zip.file( 'alcohol-1', 'some content' )
         * myPackage.zip.file( 'alcohol-2-right', 'some content' )
         * myPackage.zip.file( 'alcohol-3-right', 'some content' )
         * myPackage.zip.file( 'someSession/someTask/alcohol-4-right', 'some content' )
         *
         * // Add another file with automatically assigned name
         * const fileURL = './images/alcohol/right/alcohol-8-right.jpg'
         * const directoryInZip = someSession/someTask
         * myPackage.assignFileName( fileURL, directoryInZip ) //-> 'alcohol-right-2'
         *
         **/
        assignFileName( fileURL, directoryInZip ) {

            const category = Package.inferCategoryFromPath( fileURL )
            const orientation = Package.inferOrientationFromPath( fileURL )
            const order = !!directoryInZip
                ? this.howMany( category, orientation, directoryInZip ) + 1
                : this.howMany( category, orientation ) + 1
            const fileExtension = fileURL.split( /(\.)(?!.*\.)/ )[ 2 ]  // 'negative lookahead' regex to find the...
            //... last instance of '.' character.

            const fileName = `${ category }-${ orientation }-${ order }.${ fileExtension }`
            return fileName

        }



        /**
         * Gathers user-provided values in control panel and returns condition for a given task
         * @param task {string} Can be 'ApBM' or 'AAT'
         * @returns {string}
         * @example
         * Package.inferConditionForTask( 'ApBM' )  //-> '2b'
         * Package.inferConditionForTask( 'AAT' )  //-> '1b'
         */
        static inferConditionForTask( task ) {
            // Validation
            const possibleTaskValues = [ 'ApBM', 'AAT' ]
            possibleTaskValues.must.include( task )

            const conditionDigit = controlPanel.Panel.parseCondition()
            const id = controlPanel.Panel.parseID()
            const conditionLetter = Number( id ) % 2 === 1
                ? 'a'  // if odd
                : 'b'  // if even

            const APBMCondition = `${ conditionDigit }${ conditionLetter }`
            const AATCondition = `1${ conditionLetter }`  // AAT conditions can be 1a or 1b

            let result
            if( task === 'ApBM' ) result = APBMCondition
            if( task === 'AAT' ) result = AATCondition

            return result
        }



        prepareSequenceFiles() {

            const APBMCondition = Package.inferConditionForTask( 'ApBM' )
            const AATCondition = Package.inferConditionForTask( 'AAT' )

            // Prepare sequences for ApBM
            const APBMSequence1Content = Package.generateSequenceForCondition( APBMCondition, parameters.APBMSequenceLength, 'inquisitString' )
            const APBMSequence2Content = Package.generateSequenceForCondition( APBMCondition, parameters.APBMSequenceLength, 'inquisitString' )

            // Prepare sequence for AAT
            const AATSequenceContent = Package.generateSequenceForCondition( AATCondition, parameters.AATSequenceLength, 'inquisitString' )

            // Update sequence files
            this.sequenceContents.set( 'ApBMSequence1', APBMSequence1Content )
            this.sequenceContents.set( 'ApBMSequence2', APBMSequence2Content )
            this.sequenceContents.set( 'AATSequence', AATSequenceContent )

        }



        /**
         * Takes the information user entered into the control panel, then prepares and returns a string that is
         suitable to be the content of a .bat file.
         * @param task {string} Can be 'ApBM' or 'AAT'
         * @return {string}
         * @example
         * const myBatchFileContent = Package.prepareBatchFile()
         */
        static prepareBatchFileFor( task ) {

            // Validate parameters
            const possibleTaskValues = [ 'ApBM', 'AAT' ]
            possibleTaskValues.must.include( task )


            const id = controlPanel.Panel.parseID()
            const cohort = controlPanel.Panel.parseCohort()
            const sex = controlPanel.Panel.parseSex()

            const manifest = new groupManifest.Manifest()
            const groupNumber = manifest.getGroupNumber( cohort, sex )

            let targetFileName
            if( task === 'ApBM' ) targetFileName = 'Batch.iqx'
            if( task === 'AAT' ) targetFileName = '02-AAT.iqx'

            const batchFileContent = `"C:\\Program Files\\Inquisit 5\\Inquisit.exe" ".\\${targetFileName}" -s ${ id } -g ${ groupNumber }`
            return batchFileContent
        }


        /**
         * Takes the information user entered into the control panel, then prepares and returns an object. The returned
         * object contains two properties: one for 'folder information file's name, and the other for its content.
         suitable to be the content of a .bat file.
         * @param directoryType {string} Can be 'root', 'ApBM', or 'AAT'
         * @return {{name: string, content: string}}
         * @example
         * const folderInformation = Package.prepareFolderInformation( 'root' )
         */
        static prepareFolderInformationFileFor( directoryType ) {
            // Parameter validation
            const possibleDirectoryTypes = [ 'root', 'ApBM', 'AAT' ]
            possibleDirectoryTypes.must.include( directoryType )

            const { cohort, sex, id } = controlPanel.Panel.parseAll()
            const APBMCondition = Package.inferConditionForTask( 'ApBM' )
            const AATCondition = Package.inferConditionForTask( 'AAT' )
            const grpManifest = new groupManifest.Manifest()
            const groupNumber = grpManifest.getGroupNumber( cohort, sex )

            let name, content

            if( directoryType === 'root' ) {

                name = `00 - FOLDER INFO for Participant ${ id } of Group ${ groupNumber } - ApBM condition ${ APBMCondition } - AAT condition ${ AATCondition }.txt`
                content = `This is the folder for participant ${ id } of group ${ groupNumber }. The participant is a ${ sex } in the '${ cohort }' cohort (i.e., group ${ groupNumber }). This participant was assigned to experimental condition '${ APBMCondition }' for ApBM and to condition '${ AATCondition }' for AAT.

Group codes:

    1: Male, Clinical
    2: Male, Non-Clinical
    3: Female, Clinical
    4: Female, Non-Clinical
    
    
Experimental condition codes:

    Sham condition:
    - 1a: All trial types with each trial type making up [1/6] of the experiment
    - 1b: Same as 1a, except for left and right is switched (counterbalancing)

    Standard ApBM:
    - 2a: Push alcohol [3/8], Pull soft drink [3/8], Push surrender [1/8], Pull surrender [1/8]
    - 2b: Same as 2a, except for left and right is switched (counterbalancing)

    Modified ApBM:
    - 3a: Push alcohol [3/8], Pull surrender [3/8], Push soft drink [1/8], Pull soft drink right [1/8]
    - 3b: Same as 3a, except for left and right is switched (counterbalancing)
           


This folder was generated with DCBM Control Panel version ${ controlPanel.Panel.version() }.

`
            }


            if( directoryType === 'ApBM' ) {

                name = `00 - FOLDER INFO for Participant ${ id } of Group ${ groupNumber } - Condition ${ APBMCondition }.txt`
                content = `This is the folder for participant ${ id } of group ${ groupNumber }. The participant is a ${ sex } in the '${ cohort }' cohort (i.e., group ${ groupNumber }). This participant was assigned to experimental condition '${ APBMCondition }' for ApBM.

Group codes:

    1: Male, Clinical
    2: Male, Non-Clinical
    3: Female, Clinical
    4: Female, Non-Clinical
    
    
Experimental condition codes:

    Sham condition:
    - 1a: All trial types with each trial type making up [1/6] of the experiment
    - 1b: Same as 1a, except for left and right is switched (counterbalancing)

    Standard ApBM:
    - 2a: Push alcohol [3/8], Pull soft drink [3/8], Push surrender [1/8], Pull surrender [1/8]
    - 2b: Same as 2a, except for left and right is switched (counterbalancing)

    Modified ApBM:
    - 3a: Push alcohol [3/8], Pull surrender [3/8], Push soft drink [1/8], Pull soft drink right [1/8]
    - 3b: Same as 3a, except for left and right is switched (counterbalancing)
           


This folder was generated with DCBM Control Panel version ${ controlPanel.Panel.version() }.

`
            }

            if( directoryType === 'AAT' ) {

                name = `00 - FOLDER INFO for Participant ${ id } of Group ${ groupNumber } - Condition ${ AATCondition }.txt`
                content = `This is the folder for participant ${ id } of group ${ groupNumber }. The participant is a ${ sex } in the '${ cohort }' cohort (i.e., group ${ groupNumber }). This participant was assigned to experimental condition '${ AATCondition }' for AAT.

Group codes:

    1: Male, Clinical
    2: Male, Non-Clinical
    3: Female, Clinical
    4: Female, Non-Clinical
    
    
Experimental condition codes:

    Sham condition:
    - 1a: All trial types with each trial type making up [1/6] of the experiment
    - 1b: Same as 1a, except for left and right is switched (counterbalancing)

    Standard ApBM:
    - 2a: Push alcohol [3/8], Pull soft drink [3/8], Push surrender [1/8], Pull surrender [1/8]
    - 2b: Same as 2a, except for left and right is switched (counterbalancing)

    Modified ApBM:
    - 3a: Push alcohol [3/8], Pull surrender [3/8], Push soft drink [1/8], Pull soft drink right [1/8]
    - 3b: Same as 3a, except for left and right is switched (counterbalancing)
           


This folder was generated with DCBM Control Panel version ${ controlPanel.Panel.version() }.

`
            }

            return { name, content }
        }


        /**
         * Gathers URLs of user-selected images and other files to be downloaded.
         * @param session{string}
         * @param task {string} Directory name that holds the files
         * @param [fileType]{string} Can be 'images' or 'other'
         * @param tasksWithUserSelectedImages {array<string>} Only these directories will return
         * user-selected. The default values are AAT and APBM.
         * image URLs.
         * @returns {array<string>}
         * @example
         * // Get user-selected images or other files
         * Package.getFinalURLsToDownload( 'firstTraining', 'APBM','images' ) //-> array
         * Package.getFinalURLsToDownload( 'firstTraining', 'APBM', 'other' ) //-> array
         *
         * // Get both user-selected images and other files
         * Package.getFinalURLsToDownload('firstTraining', 'APBM') //-> both `images` and `other` combined in a
         * single array
         */
        static getStaticURLsToDownload( session, task, fileType, tasksWithUserSelectedImages = [ 'AAT', 'APBM' ] ) {

            validateParameters()

            // Get image paths
            const isDirWithUserSelectedImages = tasksWithUserSelectedImages.includes( task )
            let userSelectedImageURLs = []
            if( isDirWithUserSelectedImages ) {
                const condition = Package.inferConditionForTask( 'ApBM' )
                userSelectedImageURLs = Package.getFinalImagePathsForCondition( condition )
            }

            // Get paths of other files
            const staticFilesInTaskDirectory = fileManifest.Manifest.getTask( session, task, 'fixedFileURLs' )

            // Combine all paths into one object
            const URLs = { images: userSelectedImageURLs, other: staticFilesInTaskDirectory }


            const result = !!fileType
                ? URLs[ fileType ]
                : [ ...URLs.images, ...URLs.other ]
            return result



            function validateParameters() {

                // Task name
                const possibleTaskNames = fileManifest.Manifest.getAllTaskNames()
                possibleTaskNames.must.contain( task )

                // File type
                const possibleFileTypes = [ 'images', 'other' ]
                if( !!fileType ) {possibleFileTypes.must.include( fileType )}

                // Tasks with user selected images
                const nameOfTaskWithImagesIsValid = tasksWithUserSelectedImages.every( name =>
                    is.inArray( name, possibleTaskNames )
                )
                if( !nameOfTaskWithImagesIsValid ) {
                    throw Error( `The parameter 'directoriesWithUserSelectedImages' has value '${ tasksWithUserSelectedImages }' but the possible values for this parameter are '${ possibleTaskNames }'.` )
                }
            }

        }


        /**
         * Takes a file path and guesses the category from the file or directory name. Stops after finding the first
         * match.
         *
         * @param pathString {String}
         * @param substringsToLookFor {Array<String>}
         * @return {String}
         */
        static inferCategoryFromPath( pathString, substringsToLookFor = [ 'alcohol', 'softDrink', 'surrender' ] ) {
            pathString.must.be.a.string()
            substringsToLookFor.must.be.an.array()

            const foundCategory = Package._whichInPathString( pathString, substringsToLookFor )
            return foundCategory

        }


        /**
         * Takes a file path and guesses the file number from the file or directory name. Stops after finding the last
         * match in the file path (reverse-search).
         *
         * @param pathString {String}
         * @return {Number}
         */
        static inferFileNumberFromPath( pathString ) {
            pathString.must.be.a.string()

            const found = pathString.match( /([0-9]+)(?!.*[0-9]+)/ )  // find the last match
            // ('negative lookahead')
            return Number( found[ 0 ] )

        }


        /**
         * Takes a path string and infers image orientation from it by extracting specified orientation-related
         * keywords.
         *
         * @param path {String} A URL string. Can be relative.
         * @param [orientationsToLookFor] {Array<String>}
         * @param [orientationOnUndefined] {String} What to return if no orientations were found.
         * @example
         * const path = './images/alcohol/left/alcohol-1-left.jpg'
         * inferOrientationFrom( path ) //-> 'left'
         * @return {String}
         **/
        static inferOrientationFromPath( path, orientationsToLookFor = [ 'left', 'right' ], orientationOnUndefined = 'straight' ) {

            let orientationFound
            try { orientationFound = Package._whichInPathString( path, orientationsToLookFor )}
            catch ( error ) {}

            return !!orientationFound
                ? orientationFound
                : orientationOnUndefined

        }




        /**
         * Looks for a list of substrings in the given path and returns the first matching substring that it finds.
         * @param path{String}
         * @param substringsToLookFor{Array<String>}}
         * @return {String}
         * @private
         */
        static _whichInPathString( path, substringsToLookFor ) {

            // Search for the loop
            let foundSubstring

            // noinspection FunctionWithMultipleReturnPointsJS
            substringsToLookFor.some( subString => {  // some, so that the search will stop on first 'return true'
                const found = path.includes( subString )
                if( found ) {
                    foundSubstring = subString
                    return true // does not return for the function, but for the loop
                }
            } )

            if( !foundSubstring ) {
                throw new Error( `I could not find any of the substrings '${ substringsToLookFor }' in the path string '${ path }'. ` )
            }
            return foundSubstring
        }


        /**
         * Returns final image paths based on which pictures the user has selected and the condition
         * @param condition{String}
         * @return {Array<String>}
         * @example
         *  Package.getFinalImagePathsForCondition( '3b' )
         */
        static getFinalImagePathsForCondition( condition ) {

            // Validation
            [ '1a', '1b', '2a', '2b', '3a', '3b' ].must.include( condition )

            const manifest = new imageManifest.Manifest()

            // Get categories and file numbers of selected pictures
            const pathsOfSelectedImages = controlPanel.Images.listSelected()

            const finalPathList = []
            const queryMap = Package.makeQueryMapFomPathsAndCondition( pathsOfSelectedImages, condition )

            queryMap.forEach( ( fileNumber, category ) => {
                fileNumber.forEach( ( orientations, fileNumber ) => {
                    orientations.forEach( orientation => {

                        // Make a query from Manifest
                        const filePath = manifest.getManifest().get( category ).get( fileNumber ).get( orientation )
                        finalPathList.push( filePath )

                    } )
                } )
            } )

            return finalPathList

        }


        /**
         * Takes a paths array and returns an array that consists of category name strings and file numbers found n
         * the paths array.
         * @param paths{Array<String>}
         * @param condition{String}
         * @return {Map}
         * @example
         * const paths = [
         *  './images/alcohol/alcohol-1.jpg',
         *  './images/softDrink/softDrink-1.jpg',
         *  ]
         * Package.makeQueryMapFomPathsAndCondition( paths, '1a' )
         */
        static makeQueryMapFomPathsAndCondition( paths, condition ) {
            paths.must.be.an.array()
            const possibleConditions = [ '1a', '1b', '2a', '2b', '3a', '3b' ]
            possibleConditions.must.include( condition )

            const queryMap = new Map()
            queryMap.set( 'alcohol', new Map() )
            queryMap.set( 'softDrink', new Map() )
            queryMap.set( 'surrender', new Map() )


            paths.forEach( pathString => {

                const category = Package.inferCategoryFromPath( pathString )
                const fileNumber = Package.inferFileNumberFromPath( pathString )

                queryMap.get( category )
                    .set( fileNumber, orientations[ condition ][ category ] )

            } )

            return queryMap

        }


        /**
         * Creates a random sequence of numbers based on a preset. Returns the sequence in the specified output format.
         * @param condition{string} The preset to use when generating the sequence. Can be 1a, 1b, 2a, 2b, 3a, 3b.
         * @param sequenceLength {number} The length of the sequence to be created
         * @param outputFormat {string} Can be 'array', 'string', 'inquisitString'
         * @returns {array | string}
         * @example
         * Package.generateSequenceForCondition( '3b', 150 )
         */
        static generateSequenceForCondition( condition, sequenceLength, outputFormat = 'array' ) {

            validateParameters()


            // Generate sequence
            let sequence
            if( condition === '1a' || condition === '1b' ) {
                const members = [ 1, 2, 3, 4, 5, 6 ]
                const proportions = [ 1 / 6, 1 / 6, 1 / 6, 1 / 6, 1 / 6, 1 / 6 ]
                sequence = new sequenceGenerator.Sequence( members, sequenceLength, proportions )
            }
            if( condition === '2a' ) {
                const members = [ 1, 4, 5, 6 ]
                const proportions = [ 3 / 8, 3 / 8, 1 / 8, 1 / 8 ]
                sequence = new sequenceGenerator.Sequence( members, sequenceLength, proportions )
            }
            if( condition === '2b' ) {
                const members = [ 2, 3, 5, 6 ]
                const proportions = [ 3 / 8, 3 / 8, 1 / 8, 1 / 8 ]
                sequence = new sequenceGenerator.Sequence( members, sequenceLength, proportions )
            }
            if( condition === '3a' ) {
                const members = [ 1, 3, 4, 6 ]
                const proportions = [ 3 / 8, 1 / 8, 1 / 8, 3 / 8 ]
                sequence = new sequenceGenerator.Sequence( members, sequenceLength, proportions )
            }
            if( condition === '3b' ) {
                const members = [ 2, 3, 4, 5 ]
                const proportions = [ 3 / 8, 1 / 8, 1 / 8, 3 / 8 ]
                sequence = new sequenceGenerator.Sequence( members, sequenceLength, proportions )
            }

            // Format result
            let result
            if( outputFormat === 'array' ) result = sequence.array()
            if( outputFormat === 'string' ) result = sequence.string()
            if( outputFormat === 'inquisitString' ) result = sequence.inquisitString()
            return result



            function validateParameters() {
                // Validate parameters
                condition.must.be.a.string()
                outputFormat.must.be.a.string()
                const possibleConditions = [ '1a', '1b', '2a', '2b', '3a', '3b' ]
                possibleConditions.must.contain( condition )
                const possibleOutputsFormats = [ 'array', 'string', 'inquisitString' ]
                possibleOutputsFormats.must.contain( outputFormat )
            }
        }


        /**
         * Get file name from URL
         * @param url{String}
         * @returns {string}
         */
        static getFilenameFromURL( url ) {
            return url.substr( url.lastIndexOf( '/' ) + 1 )
        }


        /**
         * Retrieves a single file as arraybuffer using XMLHttpRequests.
         * It assumes cross-origin usage is allowed.
         * @url {String} The location of the file to be loaded
         * @callback {Function}
         */
        static retrieveFileAsArrayBuffer( url, callback ) {
            const xhr = new XMLHttpRequest()
            xhr.open( 'GET', url )
            xhr.responseType = 'arraybuffer'
            xhr.onerror = () => { throw new Error( 'XMLHttpRequest failed' ) }
            xhr.onload = () => {
                // noinspection MagicNumberJS
                if( xhr.status === 200 ) { callback( xhr.response, url ) }
                else { throw new Error( 'XMLHttpRequest could not load' ) }
            }
            xhr.send()
        }

    }




//// UMD FOOT ////////////////////////////////////////////////////////////////////////

    //// MODULE.EXPORTS ////
    exports.Package = Package
    exports.download = download
    exports.zipFile = zipFile
    exports.packageInstance = packageInstance
    Object.defineProperty( exports, '__esModule', { value: true } )

} ) ) )
//////////////////////////////////////////////////////////////////////////////////////

