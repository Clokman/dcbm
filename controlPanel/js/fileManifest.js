//// UMD HEAD ////////////////////////////////////////////////////////////////////////
// UMD head and foot patterns adapted from d3.js
( function ( global, factory ) {
    // noinspection JSUnresolvedVariable,JSUnresolvedFunction,NestedConditionalExpressionJS
    typeof exports === 'object' && typeof module !== 'undefined' ? factory( exports ) :
        typeof define === 'function' && define.amd ? define( [ 'exports' ], factory ) :
            ( factory( ( global.fileManifest = global.fileManifest || {} ) ) )
}( this, ( function ( exports ) {
    'use strict'
//////////////////////////////////////////////////////////////////////////////////////

    const apbmURLs = [
        './inquisit/apbm/01-START-ApBM.iqx',
        './inquisit/apbm/02-ApBM-Part1.iqx',
        './inquisit/apbm/03-ApBM-Part2.iqx',
        './inquisit/apbm/instructions-part1.iqx',
        './inquisit/apbm/practice-left.png',
        './inquisit/apbm/practice-right.png',
        './inquisit/apbm/practiceBlock.iqx',
        './inquisit/apbm/sequenceGenerator-part1.iqx',
        './inquisit/apbm/sequenceGenerator-part2.iqx'
    ]

    const aatURLs = [
        "./inquisit/aat/01-AAT.iqx",
        "./inquisit/aat/instructions-part1.iqx",
        "./inquisit/aat/practice-left.png",
        "./inquisit/aat/practice-right.png",
        "./inquisit/aat/practiceBlock.iqx",
        "./inquisit/aat/sequenceGenerator-part1.iqx"
    ]

    const ddtURLs = [
        './inquisit/ddt/DDT.iqx'
    ]

    const iatURLs = [
        './inquisit/iat/IAT.iqx',
        './inquisit/iat/intro_iat.htm'
    ]



    /**
     * Holds the names of directories to be created in the zip file. Also contains a list of files that are
     * to be downloaded regardless of user choices on the interface (unlike the user-selected images in AAT and APBM
     * directories).
     * @type {{thirdTraining: {directoryName: string, tasks: {APBM: {fixedFileURLs: string[], directoryName: string}}},
     *     fourthTraining: {directoryName: string, tasks: {APBM: {fixedFileURLs: string[], directoryName: string}, AAT:
     *     {fixedFileURLs: any[], directoryName: string}, DDT: {fixedFileURLs: any[], directoryName: string}, IAT:
     *     {fixedFileURLs: any[], directoryName: string}}}, followUp: {directoryName: string, tasks: {AAT:
     *     {fixedFileURLs: any[], directoryName: string}, DDT: {fixedFileURLs: any[], directoryName: string}, IAT:
     *     {fixedFileURLs: any[], directoryName: string}}}, firstTraining: {directoryName: string, tasks: {AAT:
     *     {fixedFileURLs: any[], directoryName: string}, APBM: {fixedFileURLs: string[], directoryName: string}, DDT:
     *     {fixedFileURLs: any[], directoryName: string}, IAT: {fixedFileURLs: any[], directoryName: string}}},
     *     secondTraining: {directoryName: string, tasks: {APBM: {fixedFileURLs: string[], directoryName: string}}}}}
     */
    const pathTree = {

        firstTraining: {
            key: '',  // automatically assigned when Manifest class is instantiated, so that key is known even if a
                      // session is assigned to its own object
            directoryName: '01 - Pre-assessment and first training',
            tasks: {

                IAT: {
                    key: '', // automatically assigned when Manifest class is instantiated, so that key is known even
                             // if a task is assigned to its own object
                    directoryName: '01 - IAT',
                    fixedFileURLs: iatURLs
                },

                AAT: {
                    key: '',
                    directoryName: '02 - AAT',
                    fixedFileURLs: aatURLs
                },

                DDT: {
                    key: '',
                    directoryName: '03 - DDT',
                    fixedFileURLs: ddtURLs
                },

                APBM: {
                    key: '',
                    directoryName: '04 - ApBM',
                    fixedFileURLs: apbmURLs
                }
            }
        },

        secondTraining: {
            key: '',
            directoryName: '02 - Second training',
            tasks: {
                APBM: {
                    key: '',
                    directoryName: '01 - ApBM',
                    fixedFileURLs: apbmURLs
                }
            }
        },


        thirdTraining: {
            key: '',
            directoryName: '03 - Third training',
            tasks: {
                APBM: {
                    key: '',
                    directoryName: '01 - ApBM',
                    fixedFileURLs: apbmURLs
                }
            }
        },

        fourthTraining: {
            key: '',
            directoryName: '04 - Fourth training - Post assessment',
            tasks: {

                APBM: {
                    key: '',
                    directoryName: '01 - ApBM',
                    fixedFileURLs: apbmURLs
                },

                DDT: {
                    key: '',
                    directoryName: '02 - DDT',
                    fixedFileURLs: ddtURLs
                },

                AAT: {
                    key: '',
                    directoryName: '03 - AAT',
                    fixedFileURLs: aatURLs
                },

                IAT: {
                    key: '',
                    directoryName: '04 - IAT',
                    fixedFileURLs: iatURLs
                }
            }
        },

        followUp: {
            key: '',
            directoryName: '05 - Four months follow-up',
            tasks: {

                AAT: {
                    key: '',
                    directoryName: '01 - AAT',
                    fixedFileURLs: aatURLs
                },

                DDT: {
                    key: '',
                    directoryName: '02 - DDT',
                    fixedFileURLs: ddtURLs
                },

                IAT: {
                    key: '',
                    directoryName: '03 - IAT',
                    fixedFileURLs: iatURLs
                }
            }
        }


    }


    // THIS FUNCTION IS CALLED RIGHT AFTER THE CLASS BODY
    function registerKeysAsOwnValues() {
        Manifest.registerKeysAsOwnValues()
    }




    class Manifest {

        constructor() {}


        /**
         * Registers keys into sub-objects, so that the keys that belong to a sub-object can be accessed even when the
         * sub-object is extracted.
         * @returns {void}
         **/
        static registerKeysAsOwnValues() {

            const tree = this.getTree()
            Object.entries( tree ).forEach( ( [ trainingKey, training ] ) => {

                // Record session's key as a value in it
                training.key = trainingKey

                // Record task's key as a value in it
                Object.entries( training.tasks ).forEach( ( [ taskKey, task ] ) => {
                    task.key = taskKey
                } )

            } )

        }


        /**
         * Takes the name of session, the task name in the session, and (optionally) the target property and
         * returns the entries from the file
         * tree.
         * @param session {string}
         * @param task {string}
         * @param [targetProperty] {string} The property to get. Can take the values 'directoryName' or 'fixedFileURLs'.
         * @returns {array | string}
         * @example
         * Manifest.getTask( 'firstTraining', 'APBM' , 'directoryName')  //> '04 - ApBM'
         * Manifest.getTask( 'firstTraining', 'APBM' , 'fixedFileURLs')  //> '04 - ApBM'
         */
        static getTask( session, task, targetProperty ) {

            // Validation
            validateParameters()


            let result

            if( arguments.length === 2 ) {
                result = pathTree[ session ].tasks[ task ]
            }
            if( arguments.length === 3 ) {
                result = pathTree[ session ].tasks[ task ][ targetProperty ]
            }

            return result



            function validateParameters() {

                // Session name
                const possibleSessionValues = [ undefined, ...Object.keys( pathTree ) ]
                possibleSessionValues.must.include( session )
                // Task name
                const possibleTaskValues = [ undefined, ...Manifest.getAllTaskNames() ]
                possibleTaskValues.must.include( task )
                // Target property
                if( !!targetProperty ) {
                    const possibleTargetPropertyNames = Object.keys( pathTree[ session ].tasks[ task ] )
                    possibleTargetPropertyNames.must.include( targetProperty )
                }
            }
        }


        /**
         * Takes the name of a session and/or a target property and retrieves the matching result from the
         * file tree.
         * @param session {string}
         * @param targetProperty {string}  The property to get. Can take the values 'directoryName' or 'fixedFileURLs'.
         **/
        static getSession( session, targetProperty ) {

            validateParameters()

            let result

            if( arguments.length === 1 ) {
                result = pathTree[ session ]
            }
            if( arguments.length === 2 ) {
                result = pathTree[ session ][ targetProperty ]
            }

            return result



            function validateParameters() {
                // Session name
                const possibleSessionNames = Object.keys( pathTree )
                possibleSessionNames.must.include( session )
                // Target property
                if( !!targetProperty ) {
                    const possibleTargetPropertyNames = Object.keys( pathTree[ session ] )
                    possibleTargetPropertyNames.must.include( targetProperty )
                }
            }

        }


        /**
         * Returns the path tree object
         * @returns {{thirdTraining: {directoryName: string, tasks: {APBM: {fixedFileURLs: string[], directoryName:
         *     string}}}, fourthTraining: {directoryName: string, tasks: {APBM: {fixedFileURLs: string[],
         *     directoryName: string}, AAT: {fixedFileURLs: any[], directoryName: string}, DDT: {fixedFileURLs: any[],
         *     directoryName: string}, IAT: {fixedFileURLs: any[], directoryName: string}}}, followUp: {directoryName:
         *     string, tasks: {AAT: {fixedFileURLs: any[], directoryName: string}, DDT: {fixedFileURLs: any[],
         *     directoryName: string}, IAT: {fixedFileURLs: any[], directoryName: string}}}, firstTraining:
         *     {directoryName: string, tasks: {AAT: {fixedFileURLs: any[], directoryName: string}, APBM:
         *     {fixedFileURLs: string[], directoryName: string}, DDT: {fixedFileURLs: any[], directoryName: string},
         *     IAT: {fixedFileURLs: any[], directoryName: string}}}, secondTraining: {directoryName: string, tasks:
         *     {APBM: {fixedFileURLs: string[], directoryName: string}}}}}
         */
        static getTree() {
            return pathTree
        }


        /**
         * Scans the path tree and lists all task keys that is found.
         * @returns {array}
         * @example
         * Manifest.getAllTaskNames()  //->[ 'IAT', 'AAT', 'DDT', 'APBM' ]
         **/
        static getAllTaskNames() {

            const result = new Set()
            Object.values( pathTree ).forEach( ( session ) => {

                const tasks = session[ 'tasks' ]
                const taskKeys = Object.keys( tasks )

                taskKeys.forEach( taskKey => {
                    result.add( taskKey )
                } )

            } )

            return Array.from( result )

        }


    }




    registerKeysAsOwnValues()


//// UMD FOOT ////////////////////////////////////////////////////////////////////////

    exports.Manifest = Manifest

    //// MODULE.EXPORTS ////
    Object.defineProperty( exports, '__esModule', { value: true } )

} ) ) )
//////////////////////////////////////////////////////////////////////////////////////





