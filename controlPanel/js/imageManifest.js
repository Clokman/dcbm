//// UMD HEAD ////////////////////////////////////////////////////////////////////////
// UMD head and foot patterns adapted from d3.js
( function ( global, factory ) {
    // noinspection JSUnresolvedVariable,JSUnresolvedFunction,NestedConditionalExpressionJS
    typeof exports === 'object' && typeof module !== 'undefined' ? factory( exports ) :
        typeof define === 'function' && define.amd ? define( [ 'exports' ], factory ) :
            ( factory( ( global.imageManifest = global.imageManifest || {} ) ) )
}( this, ( function ( exports ) {
    'use strict'
//////////////////////////////////////////////////////////////////////////////////////

    const alcoholStraightPaths = [
        './images/alcohol/alcohol-1.jpg',
        './images/alcohol/alcohol-2.jpg',
        './images/alcohol/alcohol-3.jpg',
        './images/alcohol/alcohol-4.jpg',
        './images/alcohol/alcohol-5.jpg',
        './images/alcohol/alcohol-6.jpg',
        './images/alcohol/alcohol-7.jpg',
        './images/alcohol/alcohol-8.jpg',
        './images/alcohol/alcohol-9.jpg',
        './images/alcohol/alcohol-10.jpg',
        './images/alcohol/alcohol-11.jpg',
        './images/alcohol/alcohol-12.jpg',
        './images/alcohol/alcohol-13.jpg',
        './images/alcohol/alcohol-14.jpg',
        './images/alcohol/alcohol-15.jpg',
        './images/alcohol/alcohol-16.jpg',
        './images/alcohol/alcohol-17.jpg',
        './images/alcohol/alcohol-18.jpg',
        './images/alcohol/alcohol-19.jpg',
        './images/alcohol/alcohol-20.jpg',
        './images/alcohol/alcohol-21.jpg',
        './images/alcohol/alcohol-22.jpg',
        './images/alcohol/alcohol-23.jpg',
        './images/alcohol/alcohol-24.jpg'
    ]

    const alcoholLeftPaths = [
        './images/alcohol/left/alcohol-1-left.jpg',
        './images/alcohol/left/alcohol-2-left.jpg',
        './images/alcohol/left/alcohol-3-left.jpg',
        './images/alcohol/left/alcohol-4-left.jpg',
        './images/alcohol/left/alcohol-5-left.jpg',
        './images/alcohol/left/alcohol-6-left.jpg',
        './images/alcohol/left/alcohol-7-left.jpg',
        './images/alcohol/left/alcohol-8-left.jpg',
        './images/alcohol/left/alcohol-9-left.jpg',
        './images/alcohol/left/alcohol-10-left.jpg',
        './images/alcohol/left/alcohol-11-left.jpg',
        './images/alcohol/left/alcohol-12-left.jpg',
        './images/alcohol/left/alcohol-13-left.jpg',
        './images/alcohol/left/alcohol-14-left.jpg',
        './images/alcohol/left/alcohol-15-left.jpg',
        './images/alcohol/left/alcohol-16-left.jpg',
        './images/alcohol/left/alcohol-17-left.jpg',
        './images/alcohol/left/alcohol-18-left.jpg',
        './images/alcohol/left/alcohol-19-left.jpg',
        './images/alcohol/left/alcohol-20-left.jpg',
        './images/alcohol/left/alcohol-21-left.jpg',
        './images/alcohol/left/alcohol-22-left.jpg',
        './images/alcohol/left/alcohol-23-left.jpg',
        './images/alcohol/left/alcohol-24-left.jpg'
    ]


    const alcoholRightPaths = [
        './images/alcohol/right/alcohol-1-right.jpg',
        './images/alcohol/right/alcohol-2-right.jpg',
        './images/alcohol/right/alcohol-3-right.jpg',
        './images/alcohol/right/alcohol-4-right.jpg',
        './images/alcohol/right/alcohol-5-right.jpg',
        './images/alcohol/right/alcohol-6-right.jpg',
        './images/alcohol/right/alcohol-7-right.jpg',
        './images/alcohol/right/alcohol-8-right.jpg',
        './images/alcohol/right/alcohol-9-right.jpg',
        './images/alcohol/right/alcohol-10-right.jpg',
        './images/alcohol/right/alcohol-11-right.jpg',
        './images/alcohol/right/alcohol-12-right.jpg',
        './images/alcohol/right/alcohol-13-right.jpg',
        './images/alcohol/right/alcohol-14-right.jpg',
        './images/alcohol/right/alcohol-15-right.jpg',
        './images/alcohol/right/alcohol-16-right.jpg',
        './images/alcohol/right/alcohol-17-right.jpg',
        './images/alcohol/right/alcohol-18-right.jpg',
        './images/alcohol/right/alcohol-19-right.jpg',
        './images/alcohol/right/alcohol-20-right.jpg',
        './images/alcohol/right/alcohol-21-right.jpg',
        './images/alcohol/right/alcohol-22-right.jpg',
        './images/alcohol/right/alcohol-23-right.jpg',
        './images/alcohol/right/alcohol-24-right.jpg'
    ]

    const softDrinkStraightPaths = [
        './images/softDrink/softDrink-1.jpg',
        './images/softDrink/softDrink-2.jpg',
        './images/softDrink/softDrink-3.jpg',
        './images/softDrink/softDrink-4.jpg',
        './images/softDrink/softDrink-5.jpg',
        './images/softDrink/softDrink-6.jpg',
        './images/softDrink/softDrink-7.jpg',
        './images/softDrink/softDrink-8.jpg',
        './images/softDrink/softDrink-9.jpg',
        './images/softDrink/softDrink-10.jpg',
        './images/softDrink/softDrink-11.jpg',
        './images/softDrink/softDrink-12.jpg',
        './images/softDrink/softDrink-13.jpg',
        './images/softDrink/softDrink-14.jpg',
        './images/softDrink/softDrink-15.jpg',
        './images/softDrink/softDrink-16.jpg',
        './images/softDrink/softDrink-17.jpg',
        './images/softDrink/softDrink-18.jpg',
        './images/softDrink/softDrink-19.jpg',
        './images/softDrink/softDrink-20.jpg',
        './images/softDrink/softDrink-21.jpg',
        './images/softDrink/softDrink-22.jpg',
        './images/softDrink/softDrink-23.jpg',
        './images/softDrink/softDrink-24.jpg'
    ]

    const softDrinkLeftPaths = [
        './images/softDrink/left/softDrink-1-left.jpg',
        './images/softDrink/left/softDrink-2-left.jpg',
        './images/softDrink/left/softDrink-3-left.jpg',
        './images/softDrink/left/softDrink-4-left.jpg',
        './images/softDrink/left/softDrink-5-left.jpg',
        './images/softDrink/left/softDrink-6-left.jpg',
        './images/softDrink/left/softDrink-7-left.jpg',
        './images/softDrink/left/softDrink-8-left.jpg',
        './images/softDrink/left/softDrink-9-left.jpg',
        './images/softDrink/left/softDrink-10-left.jpg',
        './images/softDrink/left/softDrink-11-left.jpg',
        './images/softDrink/left/softDrink-12-left.jpg',
        './images/softDrink/left/softDrink-13-left.jpg',
        './images/softDrink/left/softDrink-14-left.jpg',
        './images/softDrink/left/softDrink-15-left.jpg',
        './images/softDrink/left/softDrink-16-left.jpg',
        './images/softDrink/left/softDrink-17-left.jpg',
        './images/softDrink/left/softDrink-18-left.jpg',
        './images/softDrink/left/softDrink-19-left.jpg',
        './images/softDrink/left/softDrink-20-left.jpg',
        './images/softDrink/left/softDrink-21-left.jpg',
        './images/softDrink/left/softDrink-22-left.jpg',
        './images/softDrink/left/softDrink-23-left.jpg',
        './images/softDrink/left/softDrink-24-left.jpg'
    ]

    const softDrinkRightPaths = [
        './images/softDrink/right/softDrink-1-right.jpg',
        './images/softDrink/right/softDrink-2-right.jpg',
        './images/softDrink/right/softDrink-3-right.jpg',
        './images/softDrink/right/softDrink-4-right.jpg',
        './images/softDrink/right/softDrink-5-right.jpg',
        './images/softDrink/right/softDrink-6-right.jpg',
        './images/softDrink/right/softDrink-7-right.jpg',
        './images/softDrink/right/softDrink-8-right.jpg',
        './images/softDrink/right/softDrink-9-right.jpg',
        './images/softDrink/right/softDrink-10-right.jpg',
        './images/softDrink/right/softDrink-11-right.jpg',
        './images/softDrink/right/softDrink-12-right.jpg',
        './images/softDrink/right/softDrink-13-right.jpg',
        './images/softDrink/right/softDrink-14-right.jpg',
        './images/softDrink/right/softDrink-15-right.jpg',
        './images/softDrink/right/softDrink-16-right.jpg',
        './images/softDrink/right/softDrink-17-right.jpg',
        './images/softDrink/right/softDrink-18-right.jpg',
        './images/softDrink/right/softDrink-19-right.jpg',
        './images/softDrink/right/softDrink-20-right.jpg',
        './images/softDrink/right/softDrink-21-right.jpg',
        './images/softDrink/right/softDrink-22-right.jpg',
        './images/softDrink/right/softDrink-23-right.jpg',
        './images/softDrink/right/softDrink-24-right.jpg'
    ]

    const surrenderStraightPaths = [
        './images/surrender/surrender-1.jpg',
        './images/surrender/surrender-2.jpg',
        './images/surrender/surrender-3.jpg',
        './images/surrender/surrender-4.jpg',
        './images/surrender/surrender-5.jpg',
        './images/surrender/surrender-6.jpg',
        './images/surrender/surrender-7.jpg',
        './images/surrender/surrender-8.jpg',
        './images/surrender/surrender-9.jpg',
        './images/surrender/surrender-10.jpg',
        './images/surrender/surrender-11.jpg',
        './images/surrender/surrender-12.jpg',
        './images/surrender/surrender-13.jpg',
        './images/surrender/surrender-14.jpg',
        './images/surrender/surrender-15.jpg',
        './images/surrender/surrender-16.jpg',
        './images/surrender/surrender-17.jpg',
        './images/surrender/surrender-18.jpg',
        './images/surrender/surrender-19.jpg',
        './images/surrender/surrender-20.jpg',
        './images/surrender/surrender-21.jpg',
        './images/surrender/surrender-22.jpg',
        './images/surrender/surrender-23.jpg',
        './images/surrender/surrender-24.jpg'
    ]

    const surrenderLeftPaths = [
        './images/surrender/left/surrender-1-left.jpg',
        './images/surrender/left/surrender-2-left.jpg',
        './images/surrender/left/surrender-3-left.jpg',
        './images/surrender/left/surrender-4-left.jpg',
        './images/surrender/left/surrender-5-left.jpg',
        './images/surrender/left/surrender-6-left.jpg',
        './images/surrender/left/surrender-7-left.jpg',
        './images/surrender/left/surrender-8-left.jpg',
        './images/surrender/left/surrender-9-left.jpg',
        './images/surrender/left/surrender-10-left.jpg',
        './images/surrender/left/surrender-11-left.jpg',
        './images/surrender/left/surrender-12-left.jpg',
        './images/surrender/left/surrender-13-left.jpg',
        './images/surrender/left/surrender-14-left.jpg',
        './images/surrender/left/surrender-15-left.jpg',
        './images/surrender/left/surrender-16-left.jpg',
        './images/surrender/left/surrender-17-left.jpg',
        './images/surrender/left/surrender-18-left.jpg',
        './images/surrender/left/surrender-19-left.jpg',
        './images/surrender/left/surrender-20-left.jpg',
        './images/surrender/left/surrender-21-left.jpg',
        './images/surrender/left/surrender-22-left.jpg',
        './images/surrender/left/surrender-23-left.jpg',
        './images/surrender/left/surrender-24-left.jpg'
    ]

    const surrenderRightPaths = [
        './images/surrender/right/surrender-1-right.jpg',
        './images/surrender/right/surrender-2-right.jpg',
        './images/surrender/right/surrender-3-right.jpg',
        './images/surrender/right/surrender-4-right.jpg',
        './images/surrender/right/surrender-5-right.jpg',
        './images/surrender/right/surrender-6-right.jpg',
        './images/surrender/right/surrender-7-right.jpg',
        './images/surrender/right/surrender-8-right.jpg',
        './images/surrender/right/surrender-9-right.jpg',
        './images/surrender/right/surrender-10-right.jpg',
        './images/surrender/right/surrender-11-right.jpg',
        './images/surrender/right/surrender-12-right.jpg',
        './images/surrender/right/surrender-13-right.jpg',
        './images/surrender/right/surrender-14-right.jpg',
        './images/surrender/right/surrender-15-right.jpg',
        './images/surrender/right/surrender-16-right.jpg',
        './images/surrender/right/surrender-17-right.jpg',
        './images/surrender/right/surrender-18-right.jpg',
        './images/surrender/right/surrender-19-right.jpg',
        './images/surrender/right/surrender-20-right.jpg',
        './images/surrender/right/surrender-21-right.jpg',
        './images/surrender/right/surrender-22-right.jpg',
        './images/surrender/right/surrender-23-right.jpg',
        './images/surrender/right/surrender-24-right.jpg'
    ]




    class Manifest {

        constructor() {
            this._pathsMap = this._buildPathsMap()
        }


        getManifest() {
            return this._pathsMap
        }


        /**
         * Returns all images of an orientation
         * @param orientation
         */
        getPathsWithOrientation( orientation ) {

            [ 'straight', 'left', 'right' ].must.include( orientation )

            const resultMap = new Map()

            this.getManifest().forEach( ( subCategoryMap, categoryName ) => {
                const orientationMap = this.getSubcategory( categoryName, orientation )
                const key = `${ categoryName }`
                resultMap.set( key, orientationMap )
            } )

            return resultMap

        }


        /**
         * Retrieves all file URLs that belong to a category-subcategory pair, such as 'alcohol-straight' for
         * straight alcohol pictures.
         * @param category
         * @param subcategory
         * @return {*[]}
         */
        getSubcategory( category, subcategory ) {

            // Validation
            arguments.must.have.length( 2 )
            const possibleCategoryValues = [ 'alcohol', 'softDrink', 'surrender' ]
            const possibleSubCategoryValues = [ 'straight', 'left', 'right' ]
            possibleCategoryValues.must.include( category )
            possibleSubCategoryValues.must.include( subcategory )


            // Get category map
            const filePaths = []
            const categoryMap = this.getManifest().get( category )
            categoryMap.forEach( ( orientation, fileNumber ) => {
                orientation.forEach( ( filePath, orientationName ) => {
                    if( orientationName === subcategory ) {
                        filePaths.push( filePath )
                    }
                } )

            } )

            return filePaths
        }


        /**
         * Creates a nested map of straight, left-oriented, and right-oriented image paths.
         */
        _buildPathsMap() {

            const paths = new Map()

            paths.set( 'alcohol', new Map() )


            // NOTE: DO NOT REFACTOR BY ABSTRACTING MAP CREATION
            // There is repeated code segments below, but I found this necessary because implementation becomes
            // overly abstract and hard to debug if the map generated below is abstracted further.

            // Alcohol straight
            alcoholStraightPaths.forEach( ( path, i ) => {
                const j = i + 1  // so that the index starts from 1, as the file numbering usually does
                paths.get( 'alcohol' )
                    .set( j, new Map() )
                paths.get( 'alcohol' ).get( j )
                    .set( 'straight', path )
            } )

            // Alcohol left
            alcoholLeftPaths.forEach( ( path, i ) => {
                const j = i + 1
                paths.get( 'alcohol' ).get( j )
                    .set( 'left', path )
            } )

            // Alcohol right
            alcoholRightPaths.forEach( ( path, i ) => {
                const j = i + 1
                paths.get( 'alcohol' ).get( j )
                    .set( 'right', path )
            } )


            // Soft drinks
            paths.set( 'softDrink', new Map() )

            // Soft drink straight
            softDrinkStraightPaths.forEach( ( path, i ) => {
                const j = i + 1  // so that the index starts from 1, as the file numbering usually does
                paths.get( 'softDrink' )
                    .set( j, new Map() )
                paths.get( 'softDrink' ).get( j )
                    .set( 'straight', path )
            } )

            // Soft drink left
            softDrinkLeftPaths.forEach( ( path, i ) => {
                const j = i + 1
                paths.get( 'softDrink' ).get( j )
                    .set( 'left', path )
            } )

            // Soft drink right
            softDrinkRightPaths.forEach( ( path, i ) => {
                const j = i + 1
                paths.get( 'softDrink' ).get( j )
                    .set( 'right', path )
            } )


            // Surrender
            paths.set( 'surrender', new Map() )

            // Surrender straight
            surrenderStraightPaths.forEach( ( path, i ) => {
                const j = i + 1  // so that the index starts from 1, as the file numbering usually does
                paths.get( 'surrender' )
                    .set( j, new Map() )
                paths.get( 'surrender' ).get( j )
                    .set( 'straight', path )
            } )

            // Surrender left
            surrenderLeftPaths.forEach( ( path, i ) => {
                const j = i + 1
                paths.get( 'surrender' ).get( j )
                    .set( 'left', path )
            } )

            // Surrender right
            surrenderRightPaths.forEach( ( path, i ) => {
                const j = i + 1
                paths.get( 'surrender' ).get( j )
                    .set( 'right', path )
            } )

            return paths

        }

    }




//// UMD FOOT ////////////////////////////////////////////////////////////////////////

    //// MODULE.EXPORTS ////
    exports.Manifest = Manifest
    Object.defineProperty( exports, '__esModule', { value: true } )

} ) ) )
//////////////////////////////////////////////////////////////////////////////////////





