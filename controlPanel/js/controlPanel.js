//// UMD HEAD ////////////////////////////////////////////////////////////////////////
// UMD head and foot patterns adapted from d3.js

( function ( global, factory ) {
    // noinspection JSUnresolvedVariable,JSUnresolvedFunction,NestedConditionalExpressionJS
    typeof exports === 'object' && typeof module !== 'undefined' ? factory( exports ) :
        typeof define === 'function' && define.amd ? define( [ 'exports' ], factory ) :
            ( factory( ( global.controlPanel = global.controlPanel || {} ) ) )
}( this, ( function ( exports ) {
    'use strict'
//////////////////////////////////////////////////////////////////////////////////////

    const _version = '2021.08.27b'

    const selectionCounts = new Map()  // should be reset after each test for correct counts in tests that follow
    const selectionLimit = 8  // maximum number of images that can be selected


    function initialize() {
        const panel = new Panel()
        panel.listen()
    }




    class Panel {
        constructor() {}


        /**
         * Listens for clicks on images. When user clicks on an image, it updates selection counters and associated
         * control panel elements.
         */
        listen() {

            document.addEventListener( 'click', ( event ) => {

                const clickedElement = event.target
                const clickedOnImage = clickedElement.closest( '.image' )
                const imageIsAlreadySelected = clickedElement.parentElement.classList.contains( 'selected' )
                const imageIsStillUnSelected = !imageIsAlreadySelected
                const categoryOfClickedImage = clickedElement.getAttribute( 'data-category' )
                const categoryQuotaIsReached = Images.isQuotaReached( categoryOfClickedImage )
                const categoryQuotaIsNotYetFull = !categoryQuotaIsReached

                if( !!clickedOnImage ) {

                    const imageFrame = clickedElement.parentNode

                    // Assign 'selected' class to picture frame
                    if( categoryQuotaIsNotYetFull ) {
                        imageFrame.classList.toggle( 'selected' )
                        Panel.updateSelectionCounters( clickedElement )
                        Panel.updateNavbarCounterText( clickedElement )
                        Panel.updateNavbarCounterColors( clickedElement )
                        Panel.updateDownloadButton()
                    }
                    else if( categoryQuotaIsReached && imageIsAlreadySelected ) {
                        imageFrame.classList.remove( 'selected' )
                        Panel.updateSelectionCounters( clickedElement )
                        Panel.updateNavbarCounterText( clickedElement )
                        Panel.updateNavbarCounterColors( clickedElement )
                        Panel.updateDownloadButton()
                    }
                    else if( categoryQuotaIsReached && imageIsStillUnSelected ) {
                        //do nothing

                        const message = `\
You have filled the quota for this category.

If you would like to select this image, you need to deselect another one first.`
                        Panel.displayOverlayMessage( message, 5000 )
                    }

                }

            } )

        }


        // noinspection FunctionWithMultipleReturnPointsJS
        static updateDownloadButton() {

            const downloadButton = document.querySelector( '#downloadButton' )
            if( !downloadButton ) {return}  // so that not all tests require a download button

            // Check if user selected enough pictures
            const everyCategoryHasFullQuotas = Images.areAllQuotasFilled()


            // Check if user entered an id
            const idProvided = Panel.isProvided( '#idInput' )
            // Check it for condition
            const conditionProvided = Panel.isProvided( '#conditionDropdown' )
            // For cohort
            const cohortProvided = Panel.isProvided( '#cohortDropdown' )
            // And for sex
            const sexProvided = Panel.isProvided( '#sexDropdown' )

            // noinspection OverlyComplexBooleanExpressionJS
            const allRequirementsAreMet =
                everyCategoryHasFullQuotas &&
                idProvided &&
                conditionProvided &&
                cohortProvided &&
                sexProvided


            allRequirementsAreMet
                ? downloadButton.disabled = false
                : downloadButton.disabled = true

        }


        /**
         * Updates the text content of the group number span element, which is used in ID label.
         *
         **/
        static updateGroupNumberSpan() {


            const cohort = Panel.parseCohort()
            const sex = Panel.parseSex()

            const manifest = new groupManifest.Manifest()
            const groupNumber = manifest.getGroupNumber( cohort, sex )

            const groupNumberSpan = document.querySelector( '#groupNumberSpan' )
            groupNumberSpan.textContent = String( groupNumber )


        }


        /**
         * Shows the inputted message to user. The message disappears after the provided timeout.
         * @param message {string} The message text to display.
         * @param timeOut {number} The time after which the message element is removed from DOM.
         * @returns {void}
         * @example
         * const message = 'Quota is full.'
         * const timeOut = 2000
         * Panel.displayOverlayMessage( message, timeOut )
         **/
        static displayOverlayMessage( message, timeOut ) {

            // Create the message container element
            const overlayMessageContainer = document.createElement( 'DIV' )
            overlayMessageContainer.classList.add( 'overlayMessageContainer' )
            document.body.appendChild( overlayMessageContainer )

            // Create the message element
            const overlayMessage = document.createElement( 'DIV' )
            overlayMessage.innerText = message
            overlayMessage.classList.add( 'overlayMessage' )
            overlayMessageContainer.appendChild( overlayMessage )

            setTimeout( () => {
                overlayMessageContainer.remove()
            }, timeOut )

        }


        static updateSelectionCounters( clickedElement ) {

            const pictureFrame = clickedElement.parentNode

            // Increment counter if image is selected
            const categoryOfClickedElement = clickedElement.getAttribute( 'data-category' )
            if( pictureFrame.classList.contains( 'selected' ) ) {
                !!selectionCounts.get( categoryOfClickedElement )
                    ? selectionCounts.set( categoryOfClickedElement, selectionCounts.get( categoryOfClickedElement ) + 1 )
                    : selectionCounts.set( categoryOfClickedElement, 1 )
            }
            // Decrement counter if image is deselected
            else {
                !!selectionCounts.get( categoryOfClickedElement )
                    ? selectionCounts.set( categoryOfClickedElement, selectionCounts.get( categoryOfClickedElement ) - 1 )
                    : selectionCounts.set( categoryOfClickedElement, 0 )
            }
        }


        static updateNavbarCounterText( clickedElement ) {

            const categoryOfClickedElement = clickedElement.getAttribute( 'data-category' )
            const relevantCounterText = document.querySelector( `.counterText.${ categoryOfClickedElement }` )
            if( relevantCounterText ) {
                const relevantSelectionCount = selectionCounts.get( categoryOfClickedElement )
                relevantCounterText.textContent = relevantSelectionCount
            }
        }


        static updateNavbarCounterColors( clickedElement ) {
            const categoryOfClickedElement = clickedElement.getAttribute( 'data-category' )
            const relevantCounterArea = document.querySelector( `.counterArea.${ categoryOfClickedElement }` )
            if( relevantCounterArea ) {  // so that all tests do not have to have this element

                const relevantSelectionCount = selectionCounts.get( categoryOfClickedElement )

                if( relevantSelectionCount === 0 ) {
                    relevantCounterArea.classList.remove( 'someSelected' )
                    relevantCounterArea.classList.remove( 'selectionQuotaFull' )
                    relevantCounterArea.classList.add( 'noneSelected' )

                }
                if( relevantSelectionCount > 0 && relevantSelectionCount <= selectionLimit ) {
                    relevantCounterArea.classList.remove( 'noneSelected' )
                    relevantCounterArea.classList.remove( 'selectionLimitReached' )
                    relevantCounterArea.classList.add( 'someSelected' )

                }
                if( relevantSelectionCount >= selectionLimit ) {
                    relevantCounterArea.classList.remove( 'noneSelected' )
                    relevantCounterArea.classList.remove( 'someSelected' )
                    relevantCounterArea.classList.add( 'selectionLimitReached' )
                }
            }
        }


        static isProvided( selector ) {
            let result
            try { result = document.querySelector( selector ).value.length > 0 }
            catch ( error ) { result = false }
            return result
        }


        static parseAll() {

            const cohort = this.parseCohort()
            const sex = this.parseSex()
            const id = this.parseID()
            const condition = this.parseCondition()

            return { cohort, sex, id, condition }

        }


        static parseCondition() {
            return document.querySelector( '#conditionDropdown' ).value
        }


        static parseID() {
            return document.querySelector( '#idInput' ).value
        }


        static parseSex() {
            return document.querySelector( '#sexDropdown' ).value
        }


        static parseCohort() {
            return document.querySelector( '#cohortDropdown' ).value
        }


        /**
         * Updates the condition in the control panel interface based on id, sex, and cohort
         *
         **/
        static updateConditionDropdown() {

            // Parse control panel values
            const cohort = Panel.parseCohort()
            const sex = Panel.parseSex()
            const id = Panel.parseID()

            // Access group manifest
            // Ask the condition to manifest

            if( !!cohort && !!sex && !!id ) {

                const manifest = new groupManifest.Manifest()
                const condition = manifest.getCondition( cohort, sex, id )

                const conditionDropdown = document.querySelector( '#conditionDropdown' )
                conditionDropdown.value = condition

            }

        }


        /**
         * @returns {string}
         * @example
         * Panel.version()
         */
        static version() {

            return _version

        }


        /**
         * Updates version text in navbar
         * @returns {string}
         * @example
         *  Panel.updateVersion()
         */
        static updateVersion() {

            const versionSpan = document.querySelector( '#versionSpan' )
            versionSpan.textContent = _version

        }



    }




    class Images {

        constructor() {}


        /**
         * Generates HTML based on the image manifest
         */
        static generateHtmlOfImages() {

            generateSections()
            generateImages()

            function generateSections() {

                const manifest = new imageManifest.Manifest()
                const categories = manifest.getPathsWithOrientation( 'straight' )

                // Create an image section for each image category
                let i = 1
                categories.forEach( ( imageList, categoryName ) => {

                    // Create section
                    const section = document.createElement( 'div' )
                    section.setAttribute( 'class', 'section' )
                    section.setAttribute( 'id', `section${ i }` )

                    // Create section title and section's image area
                    section.innerHTML = `
                        <div class="sectionHeaderArea" id="${ categoryName }SectionHeaderArea">
                            <div class="sectionTitle" id="${ categoryName }SectionTitle">CATEGORY ${ i }</div>
                            <div class="sectionInstruction" id="${ categoryName }SectionInstruction">Please select 8 images</div>
                        </div>
                        
                        <div class="sectionImageArea" id="${ categoryName }ImageArea"></div>
                   `

                    // Put section under appropriate parent
                    const documentImageArea = document.querySelector( '#documentImageArea' )
                    documentImageArea.appendChild( section )

                    // Increment counter
                    i++
                } )
            }

            function generateImages() {

                const manifest = new imageManifest.Manifest()
                const categories = manifest.getPathsWithOrientation( 'straight' )

                categories.forEach( ( imageList, categoryName ) => {
                    imageList.forEach( imagePath => {

                        // Create frame divs
                        const imageFrame = document.createElement( 'div' )
                        imageFrame.setAttribute( 'class', 'imageFrame' )

                        // Create img elements in frames
                        imageFrame.innerHTML = `
                            <img class="${ categoryName } image" data-category="${ categoryName }" draggable="false" src="${ imagePath }" alt="">
                        `

                        // Append frame under a section
                        const imageSection = document.querySelector( `#${ categoryName }ImageArea` )
                        imageSection.appendChild( imageFrame )

                    } )
                } )
            }

        }


        /**
         * Lists paths of all images selected by user
         * @param [className]{String}
         * @returns {Array<String>}
         * @example
         * Images.listSelected( 'alcohol' )
         */
        static listSelected( className ) {

            const selectedImages = !!className
                ? document.querySelectorAll( `.selected .${ className }` )
                : document.querySelectorAll( `.selected > img` )  // not sure why img is needed here

            const imagePaths = []
            selectedImages.forEach( ( imageElement ) => {
                const imagePath = imageElement.src
                imagePaths.push( imagePath )
            } )

            return imagePaths

        }


        static numberOfSelected( className ) {

            return this.listSelected( className ).length

        }



        /**
         * Checks whether all image selection quotas are satisfied and returns a boolean.
         * @return {boolean}
         */
        static areAllQuotasFilled() {
            const maxCountsAreReached = []
            selectionCounts.forEach( ( selectionCount ) => {
                if( selectionCount >= selectionLimit ) { maxCountsAreReached.push( true ) }
            } )
            const everyCategoryHasFullQuotas = maxCountsAreReached.length === 3
            return everyCategoryHasFullQuotas
        }


        /**
         * Checks how many images user selected in the specified category.
         * @param category
         * @returns {boolean}
         * @example
         * isQuotaReached( 'alcohol' )  //-> true
         **/
        static isQuotaReached( category ) {

            const noOfSelectedImagesInCategory = Images.listSelected( category ).length
            return noOfSelectedImagesInCategory >= selectionLimit

        }


        static counts( key ) {
            return !!key
                ? selectionCounts.get( key )
                : selectionCounts
        }


    }




//// UMD FOOT ////////////////////////////////////////////////////////////////////////

    //// MODULE.EXPORTS ////
    exports.Panel = Panel
    exports.Images = Images
    exports.selectionCounts = selectionCounts
    exports.selectionLimit = selectionLimit
    exports.initialize = initialize
    exports.version = _version
    Object.defineProperty( exports, '__esModule', { value: true } )

} ) ) )
//////////////////////////////////////////////////////////////////////////////////////






