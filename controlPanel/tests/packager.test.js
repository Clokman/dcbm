//// Download ///////////////////////////////////////////////////////////////
// noinspection MagicNumberJS

global.Package = packager.Package

describe( 'Download', () => {


    // Setup
    // noinspection FunctionTooLongJS
    beforeEach( () => {

        jest.clearAllMocks()

        // Prepare DOM
        document.body.innerHTML = `
      
      
            <!-- Navbar buttons -->
            <input id="idInput" placeholder="..." type="number" min="1"/>
            <select name="condition" id="conditionDropdown">
                <option value="" selected disabled hidden>...</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>

      
            <!-- Images-->
            <div class="imageArea">
                <div class="imageFrame" id="frame1"> <img id="alcoholPicture1" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-1.jpg" alt=""> </div>
                <div class="imageFrame" id="frame2"> <img id="alcoholPicture2" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-2.jpg" alt=""> </div>
                <div class="imageFrame" id="frame3"> <img id="alcoholPicture3" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-3.jpg" alt=""> </div>
                <div class="imageFrame" id="frame4"> <img id="alcoholPicture4" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-4.jpg" alt=""> </div>
                <div class="imageFrame" id="frame5"> <img id="alcoholPicture5" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-5.jpg" alt=""> </div>
                <div class="imageFrame" id="frame6"> <img id="alcoholPicture6" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-6.jpg" alt=""> </div>
                <div class="imageFrame" id="frame7"> <img id="alcoholPicture7" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-7.jpg" alt=""> </div>
                <div class="imageFrame" id="frame8"> <img id="alcoholPicture8" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-8.jpg" alt=""> </div>
                
                <div class="imageFrame" id="frame9"> <img id="softDrinkPicture1" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-1.jpg" alt=""> </div>
                <div class="imageFrame" id="frame10"> <img id="softDrinkPicture2" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-2.jpg" alt=""> </div>
                <div class="imageFrame" id="frame11"> <img id="softDrinkPicture3" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-3.jpg" alt=""> </div>
                <div class="imageFrame" id="frame12"> <img id="softDrinkPicture4" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-4.jpg" alt=""> </div>
                <div class="imageFrame" id="frame13"> <img id="softDrinkPicture5" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-5.jpg" alt=""> </div>
                <div class="imageFrame" id="frame14"> <img id="softDrinkPicture6" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-6.jpg" alt=""> </div>
                <div class="imageFrame" id="frame15"> <img id="softDrinkPicture7" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-7.jpg" alt=""> </div>
                <div class="imageFrame" id="frame16"> <img id="softDrinkPicture8" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-8.jpg" alt=""> </div>
                   
                <div class="imageFrame" id="frame17"> <img id="surrenderPicture1" class="image surrender" data-category="surrender" src="../images/surrender/surrender-1.jpg" alt=""> </div>
                <div class="imageFrame" id="frame18"> <img id="surrenderPicture2" class="image surrender" data-category="surrender" src="../images/surrender/surrender-2.jpg" alt=""> </div>
                <div class="imageFrame" id="frame19"> <img id="surrenderPicture3" class="image surrender" data-category="surrender" src="../images/surrender/surrender-3.jpg" alt=""> </div>
                <div class="imageFrame" id="frame20"> <img id="surrenderPicture4" class="image surrender" data-category="surrender" src="../images/surrender/surrender-4.jpg" alt=""> </div>
                <div class="imageFrame" id="frame21"> <img id="surrenderPicture5" class="image surrender" data-category="surrender" src="../images/surrender/surrender-5.jpg" alt=""> </div>
                <div class="imageFrame" id="frame22"> <img id="surrenderPicture6" class="image surrender" data-category="surrender" src="../images/surrender/surrender-6.jpg" alt=""> </div>
                <div class="imageFrame" id="frame23"> <img id="surrenderPicture7" class="image surrender" data-category="surrender" src="../images/surrender/surrender-7.jpg" alt=""> </div>
                <div class="imageFrame" id="frame24"> <img id="surrenderPicture8" class="image surrender" data-category="surrender" src="../images/surrender/surrender-8.jpg" alt=""> </div>
                
            </div>

            <!-- Download Button -->
            <button id="downloadButton" onclick="packager.download()">Download</button>  
        `
        controlPanel.initialize()

        // Element references
        // noinspection DuplicatedCode
        const alcoholPicture1 = document.querySelector( '#alcoholPicture1' )
            , alcoholPicture2 = document.querySelector( '#alcoholPicture2' )
            , alcoholPicture3 = document.querySelector( '#alcoholPicture3' )
            , alcoholPicture4 = document.querySelector( '#alcoholPicture4' )
            , alcoholPicture5 = document.querySelector( '#alcoholPicture5' )
            , alcoholPicture6 = document.querySelector( '#alcoholPicture6' )
            , alcoholPicture7 = document.querySelector( '#alcoholPicture7' )
            , alcoholPicture8 = document.querySelector( '#alcoholPicture8' )

        const softDrinkPicture1 = document.querySelector( '#softDrinkPicture1' )
            , softDrinkPicture2 = document.querySelector( '#softDrinkPicture2' )
            , softDrinkPicture3 = document.querySelector( '#softDrinkPicture3' )
            , softDrinkPicture4 = document.querySelector( '#softDrinkPicture4' )
            , softDrinkPicture5 = document.querySelector( '#softDrinkPicture5' )
            , softDrinkPicture6 = document.querySelector( '#softDrinkPicture6' )
            , softDrinkPicture7 = document.querySelector( '#softDrinkPicture7' )
            , softDrinkPicture8 = document.querySelector( '#softDrinkPicture8' )

        const surrenderPicture1 = document.querySelector( '#surrenderPicture1' )
            , surrenderPicture2 = document.querySelector( '#surrenderPicture2' )
            , surrenderPicture3 = document.querySelector( '#surrenderPicture3' )
            , surrenderPicture4 = document.querySelector( '#surrenderPicture4' )
            , surrenderPicture5 = document.querySelector( '#surrenderPicture5' )
            , surrenderPicture6 = document.querySelector( '#surrenderPicture6' )
            , surrenderPicture7 = document.querySelector( '#surrenderPicture7' )
            , surrenderPicture8 = document.querySelector( '#surrenderPicture8' )

        // Emulate user providing all necessary data
        document.querySelector( '#idInput' ).value = 9
        document.querySelector( '#conditionDropdown' ).value = 3

        // Select all images up to the selection limit
        // noinspection DuplicatedCode
        alcoholPicture1.click()
        alcoholPicture2.click()
        alcoholPicture3.click()
        alcoholPicture4.click()
        alcoholPicture5.click()
        alcoholPicture6.click()
        alcoholPicture7.click()
        alcoholPicture8.click()

        softDrinkPicture1.click()
        softDrinkPicture2.click()
        softDrinkPicture3.click()
        softDrinkPicture4.click()
        softDrinkPicture5.click()
        softDrinkPicture6.click()
        softDrinkPicture7.click()
        softDrinkPicture8.click()

        surrenderPicture1.click()
        surrenderPicture2.click()
        surrenderPicture3.click()
        surrenderPicture4.click()
        surrenderPicture5.click()
        surrenderPicture6.click()
        surrenderPicture7.click()
        surrenderPicture8.click()

    } )


    test( 'Should populate the zip file when download initiated', () => {

        // This test starts in a state which 8 pictures from each category is selected
        // The condition is also set to '3', and user id is '9'


        const myPackage = new Package()
        expect( Object.keys( myPackage.zip.files ) ).toEqual( [] )
        // myPackage.buildAndDownload()
        // expect( Object.keys(myPackage.zip.files) ).toEqual( ["sequence1.iqx", "sequence2.iqx"]  )

        // TODO: buildAndDownload does not add image files to the zip file, which has to be first retrieved with
        //  XMLHttpRequest, which seems to be problematic in Jest. Going on this test with
        //  Package.getFileURLsToDownload() instead.

        // expect(controlPanel.Images.listSelected()).toBe(  )
        expect( Package.getStaticURLsToDownload( 'firstTraining', 'APBM', 'images' ) ).toEqual( [
            './images/alcohol/left/alcohol-1-left.jpg',
            './images/alcohol/right/alcohol-1-right.jpg',
            './images/alcohol/left/alcohol-2-left.jpg',
            './images/alcohol/right/alcohol-2-right.jpg',
            './images/alcohol/left/alcohol-3-left.jpg',
            './images/alcohol/right/alcohol-3-right.jpg',
            './images/alcohol/left/alcohol-4-left.jpg',
            './images/alcohol/right/alcohol-4-right.jpg',
            './images/alcohol/left/alcohol-5-left.jpg',
            './images/alcohol/right/alcohol-5-right.jpg',
            './images/alcohol/left/alcohol-6-left.jpg',
            './images/alcohol/right/alcohol-6-right.jpg',
            './images/alcohol/left/alcohol-7-left.jpg',
            './images/alcohol/right/alcohol-7-right.jpg',
            './images/alcohol/left/alcohol-8-left.jpg',
            './images/alcohol/right/alcohol-8-right.jpg',
            './images/softDrink/left/softDrink-1-left.jpg',
            './images/softDrink/right/softDrink-1-right.jpg',
            './images/softDrink/left/softDrink-2-left.jpg',
            './images/softDrink/right/softDrink-2-right.jpg',
            './images/softDrink/left/softDrink-3-left.jpg',
            './images/softDrink/right/softDrink-3-right.jpg',
            './images/softDrink/left/softDrink-4-left.jpg',
            './images/softDrink/right/softDrink-4-right.jpg',
            './images/softDrink/left/softDrink-5-left.jpg',
            './images/softDrink/right/softDrink-5-right.jpg',
            './images/softDrink/left/softDrink-6-left.jpg',
            './images/softDrink/right/softDrink-6-right.jpg',
            './images/softDrink/left/softDrink-7-left.jpg',
            './images/softDrink/right/softDrink-7-right.jpg',
            './images/softDrink/left/softDrink-8-left.jpg',
            './images/softDrink/right/softDrink-8-right.jpg',
            './images/surrender/left/surrender-1-left.jpg',
            './images/surrender/right/surrender-1-right.jpg',
            './images/surrender/left/surrender-2-left.jpg',
            './images/surrender/right/surrender-2-right.jpg',
            './images/surrender/left/surrender-3-left.jpg',
            './images/surrender/right/surrender-3-right.jpg',
            './images/surrender/left/surrender-4-left.jpg',
            './images/surrender/right/surrender-4-right.jpg',
            './images/surrender/left/surrender-5-left.jpg',
            './images/surrender/right/surrender-5-right.jpg',
            './images/surrender/left/surrender-6-left.jpg',
            './images/surrender/right/surrender-6-right.jpg',
            './images/surrender/left/surrender-7-left.jpg',
            './images/surrender/right/surrender-7-right.jpg',
            './images/surrender/left/surrender-8-left.jpg',
            './images/surrender/right/surrender-8-right.jpg'
        ] )

        expect( Package.getStaticURLsToDownload( 'firstTraining', 'APBM', 'other' ) ).toEqual( [
            "./inquisit/apbm/01-START-ApBM.iqx",
            "./inquisit/apbm/02-ApBM-Part1.iqx",
            "./inquisit/apbm/03-ApBM-Part2.iqx",
            "./inquisit/apbm/instructions-part1.iqx",
            "./inquisit/apbm/practice-left.png",
            "./inquisit/apbm/practice-right.png",
            "./inquisit/apbm/practiceBlock.iqx",
            "./inquisit/apbm/sequenceGenerator-part1.iqx",
            "./inquisit/apbm/sequenceGenerator-part2.iqx"
        ] )

    } )

    // test( 'Download button should trigger the download', async () => {
    //
    //     const downloadButtonIsEnabled = () => !document.querySelector( '#downloadButton' ).disabled
    //     expect( downloadButtonIsEnabled() ).toBeTruthy()
    //
    //     const packAndDownloadSpy = jest.spyOn( Package.prototype, 'buildAndDownload' )
    //     const retrieveFileAsArrayBufferSpy = jest.spyOn( Package, 'retrieveFileAsArrayBuffer' )
    //
    //     // expect( selectionCounts ).toBe()
    //     downloadButton.click()
    //
    //     // expect( selectionCounts ).toBe(  )
    //     await expect( packAndDownloadSpy ).toHaveBeenCalledTimes( 1 )
    //     // TODO: The mock is triggered only once, while it should be triggered 24+ times
    //     // await expect( retrieveFileAsArrayBufferSpy ).toHaveBeenCalledTimes( 1 )
    //     // expect( retrieveFileAsArrayBufferSpy.mock.calls[ 0 ][ 0 ] ).toBe(
    //     // 'http://localhost/images/alcohol/alcohol-1.jpg' )
    //
    // } )



    // test( 'Should retrieve file as array buffer', async () => {
    //
    //     const imageUrl = 'http://localhost/images/alcohol/alcohol-1.jpg'
    //     let result
    //
    //     // TODO: `retrieveFileAsArrayBuffer` method does not return anything. XMLHttpRequest in it seems not to work
    //     //  in node.
    //     Package.retrieveFileAsArrayBuffer( imageUrl, ( data, url ) => {
    //         result = url
    //     } )
    //
    //     await expect( result ).toBe( '' )
    //
    //
    // } )


    test( 'Should get file name from URL string (helper function)', () => {

        // This test starts in a state which 8 pictures from each category is selected

        const getFileName = Package.getFilenameFromURL
        const fileName = getFileName( 'http://localhost/images/alcohol/alcohol-1.jpg' )
        expect( fileName ).toBe( 'alcohol-1.jpg' )

    } )


    //// Get final static file URLs to be downloaded ///////////////////////////////////////////////////////////////

    describe( 'List final file URLs to be downloaded', () => {

        test( 'Should get URLs of all files to be downloaded in a directory', () => {

            // This test starts in a state which 8 pictures from each category is selected

            expect( Package.getStaticURLsToDownload( 'firstTraining', 'APBM' ).constructor.name ).toBe( 'Array' )
            expect( Package.getStaticURLsToDownload( 'firstTraining', 'APBM', 'other' ) ).toContain( './inquisit/apbm/01-START-ApBM.iqx' )

        } )


        test( 'If a directory is not supposed to have user-selected images in it, no such images should be returned', () => {

            Helper.initializeDOMWithControlPanel()
            Helper.clickImagesUntilQuotaIsReached()

            // User-selected images should return for AAT and APBM
            expect( Package.getStaticURLsToDownload( 'firstTraining', 'AAT', 'images' ) ).toHaveLength( 48 )
            expect( Package.getStaticURLsToDownload( 'firstTraining', 'APBM', 'images' ) ).toHaveLength( 48 )

            // No user-selected images should return for other directories
            expect( Package.getStaticURLsToDownload( 'firstTraining', 'DDT', 'images' ) ).toHaveLength( 0 )


        } )


        //// Validation ///////////////////////////////////////////////////////////////

        describe( 'Validation', () => {

            test( 'Should give an error if download list were requested for the wrong directory', () => {

                expect( () => {
                    expect( Package.getStaticURLsToDownload( 'firstTraining', 'BadDirectoryName' ) ).toBe( 'Array' )
                } ).toThrow( `["IAT","AAT","DDT","APBM"] must include "BadDirectoryName"` )


            } )

            test( 'Should give an error if download list were requested without providing a directory name', () => {

                expect( () => {
                    expect( Package.getStaticURLsToDownload() ).toBe( 'Array' )
                } ).toThrow()

            } )

            test( 'Should give an error if download list were requested without providing a directory name', () => {

                expect( () => {
                    expect( Package.getStaticURLsToDownload() ).toBe( 'Array' )
                } ).toThrow()

            } )


            test( 'Should give an error if directorieWithUserSelectedImages parameter is wrong', () => {

                expect( () => {
                    Package.getStaticURLsToDownload( 'firstTraining', 'APBM', 'other', [ 'badName', 'anotherBadName' ] )
                } ).toThrow( `The parameter 'directoriesWithUserSelectedImages' has value 'badName,anotherBadName' but the possible values for this parameter are 'IAT,AAT,DDT,APBM'.` )

            } )

        } )

    } )


    //// How Many ///////////////////////////////////////////////////////////////

    describe( 'How Many', () => {


        test( 'Should count how many files and how many files of the same category are in the zip', () => {

            const myPackage = new Package()

            // There should be no files in the zip
            expect( myPackage.howMany() ).toBe( 0 )

            // There should be no files in any category in the zip (checking 0 return case of the method)
            expect( myPackage.howMany( 'alcohol' ) ).toBe( 0 )
            expect( myPackage.howMany( 'softDrink' ) ).toBe( 0 )
            expect( myPackage.howMany( 'surrender' ) ).toBe( 0 )

            // Add some files to the zip
            myPackage.zip.file( 'alcohol-1', 'some content' )
            myPackage.zip.file( 'alcohol-2', 'some content' )
            myPackage.zip.file( 'softDrink-1', 'some content' )
            myPackage.zip.file( 'surrender-1', 'some content' )
            myPackage.zip.file( 'surrender-2', 'some content' )
            myPackage.zip.file( 'surrender-3', 'some content' )

            // There should be some files in the zip
            expect( myPackage.howMany() ).toBe( 6 )

            // There should be different number of files of each category
            expect( myPackage.howMany( 'alcohol' ) ).toBe( 2 )
            expect( myPackage.howMany( 'softDrink' ) ).toBe( 1 )
            expect( myPackage.howMany( 'surrender' ) ).toBe( 3 )
        } )


        test( 'Should count how many files and how many files of the same category AND orientation are in the zip', () => {

            const myPackage = new Package()

            // There should be no files in the zip
            expect( myPackage.howMany() ).toBe( 0 )

            // There should be no files in any category in the zip (checking 0 return case of the method)
            expect( myPackage.howMany( 'alcohol', 'left' ) ).toBe( 0 )
            expect( myPackage.howMany( 'softDrink', 'right' ) ).toBe( 0 )
            expect( myPackage.howMany( 'surrender', 'straight' ) ).toBe( 0 )

            // Add some files to the zip
            // noinspection DuplicatedCode
            myPackage.zip.file( 'alcohol-1', 'some content' )
            myPackage.zip.file( 'alcohol-2-left', 'some content' )
            myPackage.zip.file( 'alcohol-5-left', 'some content' )
            myPackage.zip.file( 'alcohol-9-right', 'some content' )
            myPackage.zip.file( 'softDrink-1-right', 'some content' )
            myPackage.zip.file( 'softDrink-2-right', 'some content' )
            myPackage.zip.file( 'softDrink-3-right', 'some content' )
            myPackage.zip.file( 'surrender-1-left', 'some content' )
            myPackage.zip.file( 'surrender-2', 'some content' )
            myPackage.zip.file( 'surrender-3-right', 'some content' )

            // There should be some files in the zip
            expect( myPackage.howMany() ).toBe( 10 )

            // There should be different number of files of each category + orientation combination
            expect( myPackage.howMany( 'alcohol', 'straight' ) ).toBe( 1 )
            expect( myPackage.howMany( 'alcohol', 'left' ) ).toBe( 2 )
            expect( myPackage.howMany( 'alcohol', 'right' ) ).toBe( 1 )
            expect( myPackage.howMany( 'softDrink', 'right' ) ).toBe( 3 )
            expect( myPackage.howMany( 'surrender', 'left' ) ).toBe( 1 )
            expect( myPackage.howMany( 'surrender', 'straight' ) ).toBe( 1 )
            expect( myPackage.howMany( 'surrender', 'right' ) ).toBe( 1 )

        } )


        test( 'Should not count items cross-directory', () => {

            const myPackage = new Package()

            // Create files in SESSION A directory
            myPackage.zip.file( 'sessionA/task/alcohol-1', 'some content' )
            myPackage.zip.file( 'sessionA/task/alcohol-2-left', 'some content' )
            myPackage.zip.file( 'sessionA/task/alcohol-3-left', 'some content' )
            myPackage.zip.file( 'sessionA/task/alcohol-4-right', 'some content' )
            myPackage.zip.file( 'sessionA/task/alcohol-5-right', 'some content' )
            // Create files in SESSION B directory
            myPackage.zip.file( 'sessionB/task/alcohol-12', 'some content' )
            myPackage.zip.file( 'sessionB/task/alcohol-5-left', 'some content' )
            myPackage.zip.file( 'sessionB/task/alcohol-6-left', 'some content' )

            // There should be some files in the zip
            expect( myPackage.howMany() ).toBe( 12 )  // includes directories
            // View structure of the zip file
            expect( Object.keys( myPackage.zip.files ) ).toEqual( [
                    'sessionA/',
                    'sessionA/task/',
                    'sessionA/task/alcohol-1',
                    'sessionA/task/alcohol-2-left',
                    'sessionA/task/alcohol-3-left',
                    'sessionA/task/alcohol-4-right',
                    'sessionA/task/alcohol-5-right',
                    'sessionB/',
                    'sessionB/task/',
                    'sessionB/task/alcohol-12',
                    'sessionB/task/alcohol-5-left',
                    'sessionB/task/alcohol-6-left'
                ]
            )


            // Check counts in the entire zip file
            expect( myPackage.howMany( 'alcohol', 'straight'  ) ).toBe( 2 )
            expect( myPackage.howMany( 'alcohol', 'left',  ) ).toBe( 4 )
            expect( myPackage.howMany( 'alcohol', 'right'  ) ).toBe( 2 )

            // Check counts in specific directories
            expect( myPackage.howMany( 'alcohol', 'straight', "sessionA" ) ).toBe( 1 )
            expect( myPackage.howMany( 'alcohol', 'straight', "sessionB/task" ) ).toBe( 1 )
            expect( myPackage.howMany( 'alcohol', 'left', "sessionA/task" ) ).toBe( 2 )
            expect( myPackage.howMany( 'alcohol', 'left', "sessionB" ) ).toBe( 2 )
            expect( myPackage.howMany( 'alcohol', 'right', "sessionA" ) ).toBe( 2 )
            expect( myPackage.howMany( 'alcohol', 'right', "sessionB" ) ).toBe( 0 )


        } )

    } )
    //// Auto-Rename Image Files ///////////////////////////////////////////////////////////////

    describe( 'Auto-Rename Image Files', () => {


        test( 'Should assign file names based on category, orientation, and how many files of the same category are in' +
            ' the zip', () => {


            const myPackage = new Package()

            // Add some files to the zip
            // noinspection DuplicatedCode
            myPackage.zip.file( 'alcohol-1', 'some content' )
            myPackage.zip.file( 'alcohol-2-left', 'some content' )
            myPackage.zip.file( 'alcohol-3-right', 'some content' )
            myPackage.zip.file( 'softDrink-1', 'some content' )
            myPackage.zip.file( 'softDrink-2-left', 'some content' )
            myPackage.zip.file( 'softDrink-2-right', 'some content' )
            myPackage.zip.file( 'surrender-1', 'some content' )
            myPackage.zip.file( 'surrender-1-left', 'some content' )
            myPackage.zip.file( 'surrender-1-right', 'some content' )

            let fileURL, assignedName

            // Add another alcohol image with auto-assigned name
            fileURL = './images/alcohol/right/alcohol-8-right.jpg'
            assignedName = myPackage.assignFileName( fileURL )
            expect( assignedName ).toBe( 'alcohol-right-2.jpg' )
            myPackage.zip.file( assignedName, 'some content' )

            // Add left alcohol
            const leftAlcoholPath = './images/alcohol/left/alcohol-8-left.jpg'
            const assignedName2 = myPackage.assignFileName( leftAlcoholPath )
            expect( assignedName2 ).toBe( 'alcohol-left-2.jpg' )
            myPackage.zip.file( assignedName2, 'some content' )

            // Add left soft drink
            const leftSoftDrinkPath = './images/softDrink/left/softDrink-8-left.jpg'
            const assignedName3 = myPackage.assignFileName( leftSoftDrinkPath )
            expect( assignedName3 ).toBe( 'softDrink-left-2.jpg' )
            myPackage.zip.file( assignedName3, 'some content' )

            // Add straight soft drink
            const straightSoftDrinkPath = './images/softDrink/softDrink-8.jpg'
            const assignedName4 = myPackage.assignFileName( straightSoftDrinkPath )
            expect( assignedName4 ).toBe( 'softDrink-straight-2.jpg' )
            myPackage.zip.file( assignedName4, 'some content' )

            // Add left surrender
            const leftSurrenderPath = './images/surrender/left/surrender-2-left.jpg'
            const assignedName5 = myPackage.assignFileName( leftSurrenderPath )
            expect( assignedName5 ).toBe( 'surrender-left-2.jpg' )
            myPackage.zip.file( assignedName5, 'some content' )


        } )

        test( 'Should assign file names by taking directories into account, so that numbering is restarted in each' +
            ' directory', () => {


            const myPackage = new Package()

            // Add some files to the zip
            // Create files in SESSION A directory
            myPackage.zip.file( 'sessionA/task/alcohol-1', 'some content' )
            myPackage.zip.file( 'sessionA/task/alcohol-2-left', 'some content' )
            myPackage.zip.file( 'sessionA/task/alcohol-4-right', 'some content' )
            myPackage.zip.file( 'sessionA/task/alcohol-5-right', 'some content' )
            myPackage.zip.file( 'sessionA/task/softDrink-1', 'some content' )
            myPackage.zip.file( 'sessionA/task/softDrink-2-left', 'some content' )
            myPackage.zip.file( 'sessionA/task/softDrink-2-right', 'some content' )
            myPackage.zip.file( 'sessionA/task/surrender-1', 'some content' )
            myPackage.zip.file( 'sessionA/task/surrender-1-left', 'some content' )
            myPackage.zip.file( 'sessionA/task/surrender-1-right', 'some content' )
            // Create files in SESSION B directory
            myPackage.zip.file( 'sessionB/task/alcohol-12', 'some content' )
            myPackage.zip.file( 'sessionB/task/alcohol-5-left', 'some content' )
            myPackage.zip.file( 'sessionB/task/alcohol-6-left', 'some content' )
            myPackage.zip.file( 'sessionB/task/alcohol-12-left', 'some content' )
            myPackage.zip.file( 'sessionB/task/softDrink-1', 'some content' )
            myPackage.zip.file( 'sessionB/task/softDrink-2-left', 'some content' )
            myPackage.zip.file( 'sessionB/task/softDrink-2-right', 'some content' )
            myPackage.zip.file( 'sessionB/task/surrender-1', 'some content' )
            myPackage.zip.file( 'sessionB/task/surrender-1-left', 'some content' )
            myPackage.zip.file( 'sessionB/task/surrender-1-right', 'some content' )


            let fileURL, directoryInZip, assignedName


            // Add an alcohol-left image to session A directory
            fileURL = './images/alcohol/right/alcohol-8-left.jpg'
            directoryInZip = 'sessionA/task'
            assignedName = myPackage.assignFileName( fileURL, directoryInZip )

            expect( assignedName ).toBe( 'alcohol-left-2.jpg' )
            myPackage.zip.file( assignedName, 'some content' )



            // Add an alcohol-right image to session A directory
            fileURL = './images/alcohol/right/alcohol-8-right.jpg'
            directoryInZip = 'sessionA/task'
            assignedName = myPackage.assignFileName( fileURL, directoryInZip )

            expect( assignedName ).toBe( 'alcohol-right-3.jpg' )
            myPackage.zip.file( assignedName, 'some content' )




            // Add an alcohol-left image to session B directory
            fileURL = './images/alcohol/right/alcohol-9-left.jpg'
            directoryInZip = 'sessionB/task'
            assignedName = myPackage.assignFileName( fileURL, directoryInZip )

            expect( assignedName ).toBe( 'alcohol-left-4.jpg' )
            myPackage.zip.file( assignedName, 'some content' )



            // Add an alcohol-right image to session B directory
            fileURL = './images/alcohol/right/alcohol-9-right.jpg'
            directoryInZip = 'sessionB/task'
            assignedName = myPackage.assignFileName( fileURL, directoryInZip )

            expect( assignedName ).toBe( 'alcohol-right-1.jpg' )
            myPackage.zip.file( assignedName, 'some content' )




        } )

    } )

} )




//// Progress Bar ///////////////////////////////////////////////////////////////

describe( 'Progress Bar', () => {

    test( 'Should increment progress bar value', () => {

        document.body.innerHTML = `
        <progress id="progressBar" value="0" max="100"></progress>
    `

        Package.incrementProgressBar( 5 )
        expect( document.querySelector( '#progressBar' ).value ).toBe( 5 )

        Package.incrementProgressBar( 5 )
        expect( document.querySelector( '#progressBar' ).value ).toBe( 10 )


    } )

    test( 'Should set progress bar value', () => {

        document.body.innerHTML = `
        <progress id="progressBar" value="0" max="100"></progress>
    `

        Package.setProgressBarValue( 100 )
        expect( document.querySelector( '#progressBar' ).value ).toBe( 100 )

        Package.setProgressBarValue( 0 )
        expect( document.querySelector( '#progressBar' ).value ).toBe( 0 )


    } )

    test( 'Should reset and hide progress bar', () => {

        document.body.innerHTML = `
        <progress id="progressBar" value="0" max="100"></progress>
    `

        Package.setProgressBarValue( 100 )
        expect( document.querySelector( '#progressBar' ).value ).toBe( 100 )

        Package.resetAndHideProgressBar()
        expect( document.querySelector( '#progressBar' ).value ).toBe( 0 )
        expect( document.querySelector( '#progressBar' ).style.visibility ).toBe( 'hidden' )


    } )

} )




//// Download Button ///////////////////////////////////////////////////////////////

describe( 'Download Button', () => {

    test( 'Should disable and enable download button', () => {

        document.body.innerHTML = `
        <button id="downloadButton" disabled onclick="packager.download()">Download</button>
    `
        expect( document.querySelector( '#downloadButton' ) ).toBeDisabled()
        Package.enableDownloadButton()
        expect( document.querySelector( '#downloadButton' ) ).toBeEnabled()
        Package.disableDownloadButton()
        expect( document.querySelector( '#downloadButton' ) ).toBeDisabled()


    } )



} )



//// Condition-Specific Behavior and Sequence Generation ///////////////////////////////////////////////////

describe( 'Condition-Specific Behavior and Sequence Generation', () => {

    global.sequenceGenerator = require( '../../sequenceGenerator/js/sequenceGenerator' )
    global._ = require( '../../sequenceGenerator/libraries/lodash-4.17.15/lodash' )

    // Teardown
    afterEach( () => {

        jest.clearAllMocks()
        document.body.innerHtml = ``

    } )

    // noinspection FunctionTooLongJS
    test( 'Should generate different trial sequences depending on selected condition', () => {

        document.body.innerHTML = `
            <select name="condition" id="conditionDropdown">
                <option value="" selected disabled hidden>...</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>
        `

        const sequenceForCondition1a = Package.generateSequenceForCondition( '1a', 150 )
        // noinspection DuplicatedCode
        expect( sequenceForCondition1a ).toHaveLength( 150 )
        expect( sequenceForCondition1a ).toContain( 1 )
        expect( sequenceForCondition1a ).toContain( 2 )
        expect( sequenceForCondition1a ).toContain( 3 )
        expect( sequenceForCondition1a ).toContain( 4 )
        expect( sequenceForCondition1a ).toContain( 5 )
        expect( sequenceForCondition1a ).toContain( 6 )

        const sequenceForCondition1b = Package.generateSequenceForCondition( '1b', 150 )
        // noinspection DuplicatedCode
        expect( sequenceForCondition1b ).toHaveLength( 150 )
        expect( sequenceForCondition1b ).toContain( 1 )
        expect( sequenceForCondition1b ).toContain( 2 )
        expect( sequenceForCondition1b ).toContain( 3 )
        expect( sequenceForCondition1b ).toContain( 4 )
        expect( sequenceForCondition1b ).toContain( 5 )
        expect( sequenceForCondition1b ).toContain( 6 )

        const sequenceForCondition2a = Package.generateSequenceForCondition( '2a', 150 )
        expect( sequenceForCondition2a ).toHaveLength( 150 )
        expect( sequenceForCondition2a ).toContain( 1 )
        expect( sequenceForCondition2a ).not.toContain( 2 )
        expect( sequenceForCondition2a ).not.toContain( 3 )
        expect( sequenceForCondition2a ).toContain( 4 )
        expect( sequenceForCondition2a ).toContain( 5 )
        expect( sequenceForCondition2a ).toContain( 6 )

        const sequenceForCondition2b = Package.generateSequenceForCondition( '2b', 150 )
        expect( sequenceForCondition2b ).toHaveLength( 150 )
        expect( sequenceForCondition2b ).not.toContain( 1 )
        expect( sequenceForCondition2b ).toContain( 2 )
        expect( sequenceForCondition2b ).toContain( 3 )
        expect( sequenceForCondition2b ).not.toContain( 4 )
        expect( sequenceForCondition2b ).toContain( 5 )
        expect( sequenceForCondition2b ).toContain( 6 )

        const sequenceForCondition3a = Package.generateSequenceForCondition( '3a', 150 )
        expect( sequenceForCondition3a ).toHaveLength( 150 )
        expect( sequenceForCondition3a ).toContain( 1 )
        expect( sequenceForCondition3a ).not.toContain( 2 )
        expect( sequenceForCondition3a ).toContain( 3 )
        expect( sequenceForCondition3a ).toContain( 4 )
        expect( sequenceForCondition3a ).not.toContain( 5 )
        expect( sequenceForCondition3a ).toContain( 6 )

        const sequenceForCondition3b = Package.generateSequenceForCondition( '3b', 150 )
        expect( sequenceForCondition3b ).toHaveLength( 150 )
        expect( sequenceForCondition3b ).not.toContain( 1 )
        expect( sequenceForCondition3b ).toContain( 2 )
        expect( sequenceForCondition3b ).toContain( 3 )
        expect( sequenceForCondition3b ).toContain( 4 )
        expect( sequenceForCondition3b ).toContain( 5 )
        expect( sequenceForCondition3b ).not.toContain( 6 )

    } )


    test( 'Should generate sequence of specified length', () => {

        expect( Package.generateSequenceForCondition( '3b', 150  ) ).toHaveLength( 150 )
        expect( Package.generateSequenceForCondition( '3b', 120  ) ).toHaveLength( 120 )

    } )

    test( 'Should spy on JSZip', () => {

        const fileSpy = jest.spyOn( JSZip.prototype, 'file' )

        const myZip = new JSZip()
        myZip.file( 'myFile.zip', 'My content' )

        expect( fileSpy ).toHaveBeenCalled()
        expect( fileSpy ).toHaveBeenCalledWith( 'myFile.zip', 'My content' )

    } )

    test( 'Should prepare sequence files based on condition', async () => {

        document.body.innerHTML = `

            <!-- Cohort Dropdown -->
            <select name="cohort" id="cohortDropdown" class="dropdownMenu">
                <option value="" selected disabled hidden>...</option>
                <option value="clinical">Clinical</option>
                <option value="nonClinical">Non-Clinical</option>
            </select>

            <!-- Sex Dropdown -->
            <select name="sex" id="sexDropdown" class="dropdownMenu">
                <option value="" selected disabled hidden>...</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
            </select>

            <!-- ID Input -->
            <input id="idInput" placeholder="..." type="number" min="1" />
            
            <!-- Condition Dropdown -->
            <select name="condition" id="conditionDropdown">
                <option value="" selected disabled hidden>...</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                
            <progress id="progressBar" value="0" max="100"></progress>
            
            </select>
        `

        // Let the user select a condition
        document.querySelector( '#conditionDropdown' ).value = '1'
        document.querySelector( '#idInput' ).value = '1'
        document.querySelector( '#cohortDropdown' ).value = 'clinical'
        document.querySelector( '#sexDropdown' ).value = 'male'

        const packageInstance = await packager.download()
        const sequenceFiles = packageInstance.sequenceContents

        // There should be two sequences generated, one for each part of the experiment
        expect( sequenceFiles.size ).toBe( 3 )

        // Sequences must ve different
        const firstSequenceFileContent = sequenceFiles.get( 'ApBMSequence1' )
        const secondSequenceFileContent = sequenceFiles.get( 'ApBMSequence2' )
        expect( firstSequenceFileContent === secondSequenceFileContent ).toBeFalsy()

    } )


    test( 'Should get conditions for ApBM and AAT based on values in id text box and condition dropdown', () => {

        document.body.innerHTML = `
        <input id="idInput" placeholder="..." type="number" min="1" />
        <select id="conditionDropdown">
            <option value="" selected disabled hidden>...</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
        `

        // 1a
        document.querySelector( '#conditionDropdown' ).value = '1'
        document.querySelector( '#idInput' ).value = '1'

        const inferCondition = (task) => Package.inferConditionForTask( task )

        // 1a
        document.querySelector( '#conditionDropdown' ).value = '1'
        document.querySelector( '#idInput' ).value = '1'

        expect( inferCondition( 'ApBM' ) ).toBe( '1a' )
        expect( inferCondition( 'AAT' ) ).toBe( '1a' )

        // 1b
        document.querySelector( '#conditionDropdown' ).value = '1'
        document.querySelector( '#idInput' ).value = '2'
        expect( inferCondition( 'ApBM' ) ).toBe( '1b' )
        expect( inferCondition( 'AAT' ) ).toBe( '1b' )

        // 2a
        document.querySelector( '#conditionDropdown' ).value = '2'
        document.querySelector( '#idInput' ).value = '3'
        expect( inferCondition( 'ApBM' ) ).toBe( '2a' )
        expect( inferCondition( 'AAT' ) ).toBe( '1a' )

        // 2b
        document.querySelector( '#conditionDropdown' ).value = '2'
        document.querySelector( '#idInput' ).value = '4'
        expect( inferCondition( 'ApBM' ) ).toBe( '2b' )
        expect( inferCondition( 'AAT' ) ).toBe( '1b' )

        // 3a
        document.querySelector( '#conditionDropdown' ).value = '3'
        document.querySelector( '#idInput' ).value = '5'
        expect( inferCondition( 'ApBM' ) ).toBe( '3a' )
        expect( inferCondition( 'AAT' ) ).toBe( '1a' )

        // 3b
        document.querySelector( '#conditionDropdown' ).value = '3'
        document.querySelector( '#idInput' ).value = '6'
        expect( inferCondition( 'ApBM' ) ).toBe( '3b' )
        expect( inferCondition( 'AAT' ) ).toBe( '1b' )

    } )


    //// Condition-based Image Lists ///////////////////////////////////////////////////////////////

    describe( 'Condition-based Image Lists', () => {


        //// Inferences ///////////////////////////////////////////////////////////////

        describe( 'Inferences', () => {

            test( 'Infer image type from path', () => {

                const inferCategoryFromPath = Package.inferCategoryFromPath

                const alcoholPathString = './images/alcohol/alcohol-1.jpg'
                expect( inferCategoryFromPath( alcoholPathString ) ).toBe( 'alcohol' )

                const softDrinkPathString = './images/softDrink/softDrink-12.jpg'
                expect( inferCategoryFromPath( softDrinkPathString ) ).toBe( 'softDrink' )


                // Error
                const anotherPathString = './images/food/food-1.jpg'
                expect( () => {
                    expect( inferCategoryFromPath( anotherPathString ) ).toBe( 'alcohol' )
                } ).toThrow( `I could not find any of the substrings 'alcohol,softDrink,surrender' in the path string './images/food/food-1.jpg'.` )


            } )


            test( 'Infer image number from path', () => {

                const inferFileNumberFromPath = Package.inferFileNumberFromPath

                const alcoholPathString = './images/alcohol/alcohol-1.jpg'
                expect( inferFileNumberFromPath( alcoholPathString ) ).toBe( 1 )

                const softDrinkPathString = './images/softDrink/softDrink-12.jpg'
                expect( inferFileNumberFromPath( softDrinkPathString ) ).toBe( 12 )


                // Should return the last number
                const somePathString = './00/11/2-16.jpg'
                expect( inferFileNumberFromPath( somePathString ) ).toBe( 16 )


            } )

            test( 'Infer orientation from path', () => {


                const inferOrientationFrom = Package.inferOrientationFromPath

                // Alcohol left
                const alcoholLeftPathString = './images/alcohol/left/alcohol-1-left.jpg'
                expect( inferOrientationFrom( alcoholLeftPathString ) ).toBe( 'left' )

                // Soft drink right
                const softDrinkRightPathString = './images/softDrink/right/softDrink-1-right.jpg'
                expect( inferOrientationFrom( softDrinkRightPathString ) ).toBe( 'right' )

                // Surrender right
                const surrenderRightPathString = './images/surrender/right/surrender-1-right.jpg'
                expect( inferOrientationFrom( surrenderRightPathString ) ).toBe( 'right' )

                // Alcohol straight
                const alcoholStraightPathString = './images/alcohol/alcohol-1.jpg'
                expect( inferOrientationFrom( alcoholStraightPathString ) ).toBe( 'straight' )

                // Soft drink straight
                const softDrinkStraightPathString = './images/softDrink/softDrink-12.jpg'
                expect( inferOrientationFrom( softDrinkStraightPathString ) ).toBe( 'straight' )


                // Surrender straight
                const surrenderStraightPathString = './images/surrender/surrender-12.jpg'
                expect( inferOrientationFrom( surrenderStraightPathString ) ).toBe( 'straight' )


            } )

        } )


        test( 'Should transform a paths array into a query map using conditions', () => {

            const paths = [
                './images/alcohol/alcohol-1.jpg',
                './images/alcohol/alcohol-2.jpg',
                './images/alcohol/alcohol-3.jpg',
                './images/softDrink/softDrink-1.jpg',
                './images/softDrink/softDrink-2.jpg',
                './images/softDrink/softDrink-3.jpg',
                './images/surrender/surrender-1.jpg',
                './images/surrender/surrender-2.jpg',
                './images/surrender/surrender-3.jpg'
            ]

            const queryMapForCondition1a = Package.makeQueryMapFomPathsAndCondition( paths, '1a' )
            // noinspection DuplicatedCode
            expect( [ ...queryMapForCondition1a.get( 'alcohol' ) ] ).toEqual( [
                [ 1, [ 'left', 'right' ] ],
                [ 2, [ 'left', 'right' ] ],
                [ 3, [ 'left', 'right' ] ]
            ] )
            expect( [ ...queryMapForCondition1a.get( 'softDrink' ) ] ).toEqual( [
                [ 1, [ 'left', 'right' ] ],
                [ 2, [ 'left', 'right' ] ],
                [ 3, [ 'left', 'right' ] ]
            ] )
            expect( [ ...queryMapForCondition1a.get( 'surrender' ) ] ).toEqual( [
                [ 1, [ 'left', 'right' ] ],
                [ 2, [ 'left', 'right' ] ],
                [ 3, [ 'left', 'right' ] ]
            ] )

            const queryMapForCondition2b = Package.makeQueryMapFomPathsAndCondition( paths, '2b' )
            // noinspection DuplicatedCode
            expect( [ ...queryMapForCondition2b.get( 'alcohol' ) ] ).toEqual( [
                [ 1, [ 'left', 'right' ] ],
                [ 2, [ 'left', 'right' ] ],
                [ 3, [ 'left', 'right' ] ]
            ] )
            // noinspection DuplicatedCode
            expect( [ ...queryMapForCondition2b.get( 'softDrink' ) ] ).toEqual( [
                [ 1, [ 'left', 'right' ] ],
                [ 2, [ 'left', 'right' ] ],
                [ 3, [ 'left', 'right' ] ]
            ] )
            expect( [ ...queryMapForCondition2b.get( 'surrender' ) ] ).toEqual( [
                [ 1, [ 'left', 'right' ] ],
                [ 2, [ 'left', 'right' ] ],
                [ 3, [ 'left', 'right' ] ]
            ] )

            const queryMapForCondition3a = Package.makeQueryMapFomPathsAndCondition( paths, '3a' )
            // noinspection DuplicatedCode
            expect( [ ...queryMapForCondition3a.get( 'alcohol' ) ] ).toEqual( [
                [ 1, [ 'left', 'right' ] ],
                [ 2, [ 'left', 'right' ] ],
                [ 3, [ 'left', 'right' ] ]
            ] )
            // noinspection DuplicatedCode
            expect( [ ...queryMapForCondition3a.get( 'softDrink' ) ] ).toEqual( [
                [ 1, [ 'left', 'right' ] ],
                [ 2, [ 'left', 'right' ] ],
                [ 3, [ 'left', 'right' ] ]
            ] )
            expect( [ ...queryMapForCondition3a.get( 'surrender' ) ] ).toEqual( [
                [ 1, [ 'left', 'right' ] ],
                [ 2, [ 'left', 'right' ] ],
                [ 3, [ 'left', 'right' ] ]
            ] )




        } )



        test( 'Get final image list based on condition and selected pictures', () => {
            // NOTE: All conditions now return all orientations. More specific behavior was disabled because
            // Disabled, because having a single Inquisit file requires all files to be present. See [NOTE1] in
            // packager.js to see more.

            document.body.innerHTML = `
                <!--   Add 3 images per image category (and make them selected)      -->
               <div class="imageFrame selected" id="frame1"> <img id="alcoholPicture1" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-1.jpg" alt=""> </div>
                <div class="imageFrame selected" id="frame2"> <img id="alcoholPicture2" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-2.jpg" alt=""> </div>
                <div class="imageFrame selected" id="frame3"> <img id="alcoholPicture3" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-3.jpg" alt=""> </div>
                <div class="imageFrame selected" id="frame4"> <img id="softDrinkPicture1" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-1.jpg" alt=""> </div>
                <div class="imageFrame selected" id="frame5"> <img id="softDrinkPicture2" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-2.jpg" alt=""> </div>
                <div class="imageFrame selected" id="frame6"> <img id="softDrinkPicture3" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-3.jpg" alt=""> </div>
                <div class="imageFrame selected" id="frame7"> <img id="surrenderPicture1" class="image surrender" data-category="surrender" src="../images/surrender/surrender-1.jpg" alt=""> </div>
                <div class="imageFrame selected" id="frame8"> <img id="surrenderPicture2" class="image surrender" data-category="surrender" src="../images/surrender/surrender-2.jpg" alt=""> </div>
                <div class="imageFrame selected" id="frame9"> <img id="surrenderPicture3" class="image surrender" data-category="surrender" src="../images/surrender/surrender-3.jpg" alt=""> </div>
            `
            const finalImageListFor1a = Package.getFinalImagePathsForCondition( '1a' )
            expect( finalImageListFor1a ).toEqual( [
                './images/alcohol/left/alcohol-1-left.jpg',
                './images/alcohol/right/alcohol-1-right.jpg',
                './images/alcohol/left/alcohol-2-left.jpg',
                './images/alcohol/right/alcohol-2-right.jpg',
                './images/alcohol/left/alcohol-3-left.jpg',
                './images/alcohol/right/alcohol-3-right.jpg',
                './images/softDrink/left/softDrink-1-left.jpg',
                './images/softDrink/right/softDrink-1-right.jpg',
                './images/softDrink/left/softDrink-2-left.jpg',
                './images/softDrink/right/softDrink-2-right.jpg',
                './images/softDrink/left/softDrink-3-left.jpg',
                './images/softDrink/right/softDrink-3-right.jpg',
                './images/surrender/left/surrender-1-left.jpg',
                './images/surrender/right/surrender-1-right.jpg',
                './images/surrender/left/surrender-2-left.jpg',
                './images/surrender/right/surrender-2-right.jpg',
                './images/surrender/left/surrender-3-left.jpg',
                './images/surrender/right/surrender-3-right.jpg'
            ] )

            const finalImageListFor1b = Package.getFinalImagePathsForCondition( '1b' )
            expect( finalImageListFor1b ).toEqual( [
                './images/alcohol/left/alcohol-1-left.jpg',
                './images/alcohol/right/alcohol-1-right.jpg',
                './images/alcohol/left/alcohol-2-left.jpg',
                './images/alcohol/right/alcohol-2-right.jpg',
                './images/alcohol/left/alcohol-3-left.jpg',
                './images/alcohol/right/alcohol-3-right.jpg',
                './images/softDrink/left/softDrink-1-left.jpg',
                './images/softDrink/right/softDrink-1-right.jpg',
                './images/softDrink/left/softDrink-2-left.jpg',
                './images/softDrink/right/softDrink-2-right.jpg',
                './images/softDrink/left/softDrink-3-left.jpg',
                './images/softDrink/right/softDrink-3-right.jpg',
                './images/surrender/left/surrender-1-left.jpg',
                './images/surrender/right/surrender-1-right.jpg',
                './images/surrender/left/surrender-2-left.jpg',
                './images/surrender/right/surrender-2-right.jpg',
                './images/surrender/left/surrender-3-left.jpg',
                './images/surrender/right/surrender-3-right.jpg'
            ] )

            const finalImageListFor2a = Package.getFinalImagePathsForCondition( '2a' )
            expect( finalImageListFor2a ).toEqual( [
                './images/alcohol/left/alcohol-1-left.jpg',
                './images/alcohol/right/alcohol-1-right.jpg',
                './images/alcohol/left/alcohol-2-left.jpg',
                './images/alcohol/right/alcohol-2-right.jpg',
                './images/alcohol/left/alcohol-3-left.jpg',
                './images/alcohol/right/alcohol-3-right.jpg',
                './images/softDrink/left/softDrink-1-left.jpg',
                './images/softDrink/right/softDrink-1-right.jpg',
                './images/softDrink/left/softDrink-2-left.jpg',
                './images/softDrink/right/softDrink-2-right.jpg',
                './images/softDrink/left/softDrink-3-left.jpg',
                './images/softDrink/right/softDrink-3-right.jpg',
                './images/surrender/left/surrender-1-left.jpg',
                './images/surrender/right/surrender-1-right.jpg',
                './images/surrender/left/surrender-2-left.jpg',
                './images/surrender/right/surrender-2-right.jpg',
                './images/surrender/left/surrender-3-left.jpg',
                './images/surrender/right/surrender-3-right.jpg'
            ] )

            const finalImageListFor2b = Package.getFinalImagePathsForCondition( '2b' )
            expect( finalImageListFor2b ).toEqual( [
                './images/alcohol/left/alcohol-1-left.jpg',
                './images/alcohol/right/alcohol-1-right.jpg',
                './images/alcohol/left/alcohol-2-left.jpg',
                './images/alcohol/right/alcohol-2-right.jpg',
                './images/alcohol/left/alcohol-3-left.jpg',
                './images/alcohol/right/alcohol-3-right.jpg',
                './images/softDrink/left/softDrink-1-left.jpg',
                './images/softDrink/right/softDrink-1-right.jpg',
                './images/softDrink/left/softDrink-2-left.jpg',
                './images/softDrink/right/softDrink-2-right.jpg',
                './images/softDrink/left/softDrink-3-left.jpg',
                './images/softDrink/right/softDrink-3-right.jpg',
                './images/surrender/left/surrender-1-left.jpg',
                './images/surrender/right/surrender-1-right.jpg',
                './images/surrender/left/surrender-2-left.jpg',
                './images/surrender/right/surrender-2-right.jpg',
                './images/surrender/left/surrender-3-left.jpg',
                './images/surrender/right/surrender-3-right.jpg'
            ] )

            const finalImageListFor3a = Package.getFinalImagePathsForCondition( '3a' )
            expect( finalImageListFor3a ).toEqual( [
                './images/alcohol/left/alcohol-1-left.jpg',
                './images/alcohol/right/alcohol-1-right.jpg',
                './images/alcohol/left/alcohol-2-left.jpg',
                './images/alcohol/right/alcohol-2-right.jpg',
                './images/alcohol/left/alcohol-3-left.jpg',
                './images/alcohol/right/alcohol-3-right.jpg',
                './images/softDrink/left/softDrink-1-left.jpg',
                './images/softDrink/right/softDrink-1-right.jpg',
                './images/softDrink/left/softDrink-2-left.jpg',
                './images/softDrink/right/softDrink-2-right.jpg',
                './images/softDrink/left/softDrink-3-left.jpg',
                './images/softDrink/right/softDrink-3-right.jpg',
                './images/surrender/left/surrender-1-left.jpg',
                './images/surrender/right/surrender-1-right.jpg',
                './images/surrender/left/surrender-2-left.jpg',
                './images/surrender/right/surrender-2-right.jpg',
                './images/surrender/left/surrender-3-left.jpg',
                './images/surrender/right/surrender-3-right.jpg'
            ] )

            const finalImageListFor3b = Package.getFinalImagePathsForCondition( '3b' )
            expect( finalImageListFor3b ).toEqual( [
                './images/alcohol/left/alcohol-1-left.jpg',
                './images/alcohol/right/alcohol-1-right.jpg',
                './images/alcohol/left/alcohol-2-left.jpg',
                './images/alcohol/right/alcohol-2-right.jpg',
                './images/alcohol/left/alcohol-3-left.jpg',
                './images/alcohol/right/alcohol-3-right.jpg',
                './images/softDrink/left/softDrink-1-left.jpg',
                './images/softDrink/right/softDrink-1-right.jpg',
                './images/softDrink/left/softDrink-2-left.jpg',
                './images/softDrink/right/softDrink-2-right.jpg',
                './images/softDrink/left/softDrink-3-left.jpg',
                './images/softDrink/right/softDrink-3-right.jpg',
                './images/surrender/left/surrender-1-left.jpg',
                './images/surrender/right/surrender-1-right.jpg',
                './images/surrender/left/surrender-2-left.jpg',
                './images/surrender/right/surrender-2-right.jpg',
                './images/surrender/left/surrender-3-left.jpg',
                './images/surrender/right/surrender-3-right.jpg'
            ] )

        } )

    } )




} )



//// Create the directory structure in Zip ///////////////////////////////////////////////////////////////

describe( 'Create the directory structure in Zip', () => {

    // Teardown
    afterEach( () => {

        jest.clearAllMocks()

    } )

    test( 'Should create the directories in zip', () => {

        const myPackage = new packager.Package

        myPackage.createDirectoriesInZip()
        expect( Object.keys( myPackage.zip.files ) ).toEqual(
            [
                '01 - Pre-assessment and first training/',
                '01 - Pre-assessment and first training/01 - IAT/',
                '01 - Pre-assessment and first training/02 - AAT/',
                '01 - Pre-assessment and first training/03 - DDT/',
                '01 - Pre-assessment and first training/04 - ApBM/',
                '02 - Second training/',
                '02 - Second training/01 - ApBM/',
                '03 - Third training/',
                '03 - Third training/01 - ApBM/',
                '04 - Fourth training - Post assessment/',
                '04 - Fourth training - Post assessment/01 - ApBM/',
                '04 - Fourth training - Post assessment/02 - DDT/',
                '04 - Fourth training - Post assessment/03 - AAT/',
                '04 - Fourth training - Post assessment/04 - IAT/',
                '05 - Four months follow-up/',
                '05 - Four months follow-up/01 - AAT/',
                '05 - Four months follow-up/02 - DDT/',
                '05 - Four months follow-up/03 - IAT/'
            ] )

    } )

    test( 'Should make XHR requests for all fixed files in the file tree while using addFixedFilesToZip method', async () => {

        const myPackage = new packager.Package

        jest.clearAllMocks()
        const xhrOpen = jest.spyOn( XMLHttpRequest.prototype, 'open' )
        const JSZipFile = jest.spyOn( JSZip.prototype, 'file' )

        await myPackage.addFixedFilesToZip()

        expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './inquisit/apbm/01-START-ApBM.iqx' )
        expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './inquisit/apbm/02-ApBM-Part1.iqx' )
        expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './inquisit/apbm/03-ApBM-Part2.iqx' )
        expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './inquisit/apbm/sequenceGenerator-part1.iqx' )
        expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './inquisit/apbm/sequenceGenerator-part2.iqx' )
        expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './inquisit/apbm/practice-left.png' )
        expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './inquisit/apbm/practice-right.png' )


    } )


    // THIS TEST WORKS ONLY WHEN RAN INDIVIDUALLY AND NOT AS PART OF A SUITE
    // UNCOMMENT TO ENABLE
    test( 'Should make XHR requests for all selected images in the file tree while using addSelectedImagesToZip' +
        ' method', async () => {


        Helper.initializeDOMWithControlPanel()
        Helper.clickImagesUntilQuotaIsReached()

        const myPackage = new packager.Package

        jest.clearAllMocks()
        const xhrOpen = jest.spyOn( XMLHttpRequest.prototype, 'open' )
        const JSZipFile = jest.spyOn( JSZip.prototype, 'file' )


        await myPackage.addSelectedImagesToZip()

        // Alcohol
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/left/alcohol-1-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/right/alcohol-1-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/left/alcohol-2-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/right/alcohol-2-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/left/alcohol-3-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/right/alcohol-3-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/left/alcohol-4-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/right/alcohol-4-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/left/alcohol-5-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/right/alcohol-5-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/left/alcohol-6-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/right/alcohol-6-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/left/alcohol-7-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/right/alcohol-7-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/left/alcohol-8-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/alcohol/right/alcohol-8-right.jpg' )
        //
        // // Soft Drink
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/left/softDrink-1-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/right/softDrink-1-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/left/softDrink-2-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/right/softDrink-2-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/left/softDrink-3-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/right/softDrink-3-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/left/softDrink-4-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/right/softDrink-4-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/left/softDrink-5-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/right/softDrink-5-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/left/softDrink-6-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/right/softDrink-6-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/left/softDrink-7-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/right/softDrink-7-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/left/softDrink-8-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/softDrink/right/softDrink-8-right.jpg' )
        //
        // // Surrender
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/left/surrender-1-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/right/surrender-1-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/left/surrender-2-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/right/surrender-2-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/left/surrender-3-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/right/surrender-3-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/left/surrender-4-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/right/surrender-4-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/left/surrender-5-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/right/surrender-5-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/left/surrender-6-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/right/surrender-6-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/left/surrender-7-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/right/surrender-7-right.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/left/surrender-8-left.jpg' )
        // expect( xhrOpen ).toHaveBeenCalledWith( 'GET', './images/surrender/right/surrender-8-right.jpg' )

    } )


} )

//// Batch file, folder information, folder name ///////////////////////////////////////////////////////////////

describe( 'Batch file, folder information, folder name', () => {

    // Setup
    beforeEach( () => {

        document.body.innerHTML = `
            <!-- Cohort Dropdown -->
            <select name="cohort" id="cohortDropdown" class="dropdownMenu">
                <option value="" selected disabled hidden>...</option>
                <option value="clinical">Clinical</option>
                <option value="nonClinical">Non-Clinical</option>
            </select>
            <!-- Sex Dropdown -->
            <select name="sex" id="sexDropdown" class="dropdownMenu">
                <option value="" selected disabled hidden>...</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
            </select>
            <!-- ID text box -->
            <input id="idInput" placeholder="..." type="number" min="1">
            <!-- Condition dropdown -->
            <div id="conditionDropdownContainer">
                <select id="conditionDropdown">
                    <option value="" selected disabled hidden>...</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
        </div>
        `

    } )

    test( 'Should prepare the batch file contents for ApBM and AAT based on user-entered values in control panel', () => {

        const idTextBox = document.querySelector( '#idInput' )
        const cohortDropdown = document.querySelector( '#cohortDropdown' )
        const sexDropdown = document.querySelector( '#sexDropdown' )

        // Set some values on control panel
        idTextBox.value = 5
        cohortDropdown.value = 'clinical'
        sexDropdown.value = 'male'
        // Batch file should contain those values
        expect( Package.prepareBatchFileFor( 'ApBM' ) ).toBe( `"C:\\Program Files\\Inquisit 5\\Inquisit.exe" ".\\Batch.iqx" -s 5 -g 1` )
        expect( Package.prepareBatchFileFor( 'AAT' ) ).toBe( `"C:\\Program Files\\Inquisit 5\\Inquisit.exe" ".\\02-AAT.iqx" -s 5 -g 1` )

        // Set other values on control panel
        idTextBox.value = 156
        cohortDropdown.value = 'nonClinical'
        sexDropdown.value = 'female'
        // Batch file should contain those new values
        expect( Package.prepareBatchFileFor( 'ApBM' ) ).toBe( `"C:\\Program Files\\Inquisit 5\\Inquisit.exe" ".\\Batch.iqx" -s 156 -g 4` )
        expect( Package.prepareBatchFileFor( 'AAT' ) ).toBe( `"C:\\Program Files\\Inquisit 5\\Inquisit.exe" ".\\02-AAT.iqx" -s 156 -g 4` )


    } )


    test( 'Should generate folder information based on user-entered values in control panel', () => {

        const idTextBox = document.querySelector( '#idInput' )
        const conditionDropdown = document.querySelector( '#conditionDropdown' )
        const cohortDropdown = document.querySelector( '#cohortDropdown' )
        const sexDropdown = document.querySelector( '#sexDropdown' )

        // Set some values on control panel
        idTextBox.value = 10
        conditionDropdown.value = 2
        cohortDropdown.value = 'clinical'
        sexDropdown.value = 'female'

        let folderInformation

        // Folder information for ROOT DIRECTORY should contain those new values
        folderInformation = Package.prepareFolderInformationFileFor( 'root' )
        expect( folderInformation.name ).toBe( '00 - FOLDER INFO for Participant 10 of Group 3 - ApBM condition 2b - AAT condition 1b.txt' )
        expect( folderInformation.content ).toBe( `This is the folder for participant 10 of group 3. The participant is a female in the 'clinical' cohort (i.e., group 3). This participant was assigned to experimental condition '2b' for ApBM and to condition '1b' for AAT.

Group codes:

    1: Male, Clinical
    2: Male, Non-Clinical
    3: Female, Clinical
    4: Female, Non-Clinical
    
    
Experimental condition codes:

    Sham condition:
    - 1a: All trial types with each trial type making up [1/6] of the experiment
    - 1b: Same as 1a, except for left and right is switched (counterbalancing)

    Standard ApBM:
    - 2a: Push alcohol [3/8], Pull soft drink [3/8], Push surrender [1/8], Pull surrender [1/8]
    - 2b: Same as 2a, except for left and right is switched (counterbalancing)

    Modified ApBM:
    - 3a: Push alcohol [3/8], Pull surrender [3/8], Push soft drink [1/8], Pull soft drink right [1/8]
    - 3b: Same as 3a, except for left and right is switched (counterbalancing)
           


This folder was generated with DCBM Control Panel version 2021.08.27b.

` )

        // For ApBM
        folderInformation = Package.prepareFolderInformationFileFor( 'ApBM' )
        expect( folderInformation.name ).toBe( '00 - FOLDER INFO for Participant 10 of Group 3 - Condition 2b.txt' )
        expect( folderInformation.content ).toBe( `This is the folder for participant 10 of group 3. The participant is a female in the 'clinical' cohort (i.e., group 3). This participant was assigned to experimental condition '2b' for ApBM.

Group codes:

    1: Male, Clinical
    2: Male, Non-Clinical
    3: Female, Clinical
    4: Female, Non-Clinical
    
    
Experimental condition codes:

    Sham condition:
    - 1a: All trial types with each trial type making up [1/6] of the experiment
    - 1b: Same as 1a, except for left and right is switched (counterbalancing)

    Standard ApBM:
    - 2a: Push alcohol [3/8], Pull soft drink [3/8], Push surrender [1/8], Pull surrender [1/8]
    - 2b: Same as 2a, except for left and right is switched (counterbalancing)

    Modified ApBM:
    - 3a: Push alcohol [3/8], Pull surrender [3/8], Push soft drink [1/8], Pull soft drink right [1/8]
    - 3b: Same as 3a, except for left and right is switched (counterbalancing)
           


This folder was generated with DCBM Control Panel version 2021.08.27b.

` )


        // For AAT
        folderInformation = Package.prepareFolderInformationFileFor( 'AAT' )
        expect( folderInformation.name ).toBe( '00 - FOLDER INFO for Participant 10 of Group 3 - Condition 1b.txt' )
        expect( folderInformation.content ).toBe( `This is the folder for participant 10 of group 3. The participant is a female in the 'clinical' cohort (i.e., group 3). This participant was assigned to experimental condition '1b' for AAT.

Group codes:

    1: Male, Clinical
    2: Male, Non-Clinical
    3: Female, Clinical
    4: Female, Non-Clinical
    
    
Experimental condition codes:

    Sham condition:
    - 1a: All trial types with each trial type making up [1/6] of the experiment
    - 1b: Same as 1a, except for left and right is switched (counterbalancing)

    Standard ApBM:
    - 2a: Push alcohol [3/8], Pull soft drink [3/8], Push surrender [1/8], Pull surrender [1/8]
    - 2b: Same as 2a, except for left and right is switched (counterbalancing)

    Modified ApBM:
    - 3a: Push alcohol [3/8], Pull surrender [3/8], Push soft drink [1/8], Pull soft drink right [1/8]
    - 3b: Same as 3a, except for left and right is switched (counterbalancing)
           


This folder was generated with DCBM Control Panel version 2021.08.27b.

` )


        // Set other values on control panel and check it for (e.g.) ApBM
        idTextBox.value = 198
        conditionDropdown.value = 1

        // Folder information should contain those new values
        const folderInformation2 = Package.prepareFolderInformationFileFor( 'ApBM' )
        expect( folderInformation2.name ).toBe( '00 - FOLDER INFO for Participant 198 of Group 3 - Condition 1b.txt' )
        expect( folderInformation2.content ).toBe( `This is the folder for participant 198 of group 3. The participant is a female in the 'clinical' cohort (i.e., group 3). This participant was assigned to experimental condition '1b' for ApBM.

Group codes:

    1: Male, Clinical
    2: Male, Non-Clinical
    3: Female, Clinical
    4: Female, Non-Clinical
    
    
Experimental condition codes:

    Sham condition:
    - 1a: All trial types with each trial type making up [1/6] of the experiment
    - 1b: Same as 1a, except for left and right is switched (counterbalancing)

    Standard ApBM:
    - 2a: Push alcohol [3/8], Pull soft drink [3/8], Push surrender [1/8], Pull surrender [1/8]
    - 2b: Same as 2a, except for left and right is switched (counterbalancing)

    Modified ApBM:
    - 3a: Push alcohol [3/8], Pull surrender [3/8], Push soft drink [1/8], Pull soft drink right [1/8]
    - 3b: Same as 3a, except for left and right is switched (counterbalancing)
           


This folder was generated with DCBM Control Panel version 2021.08.27b.

` )

    } )


} )



