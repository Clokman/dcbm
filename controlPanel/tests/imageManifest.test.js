//// Initialize and get Manifest ///////////////////////////////////////////////////////////////

const { Manifest } = require( '../js/imageManifest' )

describe( 'Initialize and get Manifest', () => {

    test( 'Should return straight paths for each category', () => {

        const myManifest = new Manifest()
        const result = myManifest.getPathsWithOrientation( 'straight' )
        expect( [ ...result ] ).toEqual(
            [
                [ 'alcohol',
                    [ './images/alcohol/alcohol-1.jpg',
                        './images/alcohol/alcohol-2.jpg',
                        './images/alcohol/alcohol-3.jpg',
                        './images/alcohol/alcohol-4.jpg',
                        './images/alcohol/alcohol-5.jpg',
                        './images/alcohol/alcohol-6.jpg',
                        './images/alcohol/alcohol-7.jpg',
                        './images/alcohol/alcohol-8.jpg',
                        './images/alcohol/alcohol-9.jpg',
                        './images/alcohol/alcohol-10.jpg',
                        './images/alcohol/alcohol-11.jpg',
                        './images/alcohol/alcohol-12.jpg',
                        './images/alcohol/alcohol-13.jpg',
                        './images/alcohol/alcohol-14.jpg',
                        './images/alcohol/alcohol-15.jpg',
                        './images/alcohol/alcohol-16.jpg',
                        './images/alcohol/alcohol-17.jpg',
                        './images/alcohol/alcohol-18.jpg',
                        './images/alcohol/alcohol-19.jpg',
                        './images/alcohol/alcohol-20.jpg',
                        './images/alcohol/alcohol-21.jpg',
                        './images/alcohol/alcohol-22.jpg',
                        './images/alcohol/alcohol-23.jpg',
                        './images/alcohol/alcohol-24.jpg'
                    ]
                ],
                [ 'softDrink',
                    [ './images/softDrink/softDrink-1.jpg',
                        './images/softDrink/softDrink-2.jpg',
                        './images/softDrink/softDrink-3.jpg',
                        './images/softDrink/softDrink-4.jpg',
                        './images/softDrink/softDrink-5.jpg',
                        './images/softDrink/softDrink-6.jpg',
                        './images/softDrink/softDrink-7.jpg',
                        './images/softDrink/softDrink-8.jpg',
                        './images/softDrink/softDrink-9.jpg',
                        './images/softDrink/softDrink-10.jpg',
                        './images/softDrink/softDrink-11.jpg',
                        './images/softDrink/softDrink-12.jpg',
                        './images/softDrink/softDrink-13.jpg',
                        './images/softDrink/softDrink-14.jpg',
                        './images/softDrink/softDrink-15.jpg',
                        './images/softDrink/softDrink-16.jpg',
                        './images/softDrink/softDrink-17.jpg',
                        './images/softDrink/softDrink-18.jpg',
                        './images/softDrink/softDrink-19.jpg',
                        './images/softDrink/softDrink-20.jpg',
                        './images/softDrink/softDrink-21.jpg',
                        './images/softDrink/softDrink-22.jpg',
                        './images/softDrink/softDrink-23.jpg',
                        './images/softDrink/softDrink-24.jpg' ]
                ],
                [ 'surrender',
                    [ './images/surrender/surrender-1.jpg',
                        './images/surrender/surrender-2.jpg',
                        './images/surrender/surrender-3.jpg',
                        './images/surrender/surrender-4.jpg',
                        './images/surrender/surrender-5.jpg',
                        './images/surrender/surrender-6.jpg',
                        './images/surrender/surrender-7.jpg',
                        './images/surrender/surrender-8.jpg',
                        './images/surrender/surrender-9.jpg',
                        './images/surrender/surrender-10.jpg',
                        './images/surrender/surrender-11.jpg',
                        './images/surrender/surrender-12.jpg',
                        './images/surrender/surrender-13.jpg',
                        './images/surrender/surrender-14.jpg',
                        './images/surrender/surrender-15.jpg',
                        './images/surrender/surrender-16.jpg',
                        './images/surrender/surrender-17.jpg',
                        './images/surrender/surrender-18.jpg',
                        './images/surrender/surrender-19.jpg',
                        './images/surrender/surrender-20.jpg',
                        './images/surrender/surrender-21.jpg',
                        './images/surrender/surrender-22.jpg',
                        './images/surrender/surrender-23.jpg',
                        './images/surrender/surrender-24.jpg'
                    ]
                ]
            ]
        )


    } )


    test( 'Should return paths when asked a category and subcategory', () => {

        const myManifest = new Manifest()
        myManifest.getManifest().keys()
        expect( [ ...myManifest.getManifest().keys() ] ).toEqual( [ 'alcohol', 'softDrink', 'surrender' ] )


        // Test a few categories //

        // Alcohol straight
        expect( [ ...myManifest.getSubcategory( 'alcohol', 'straight' ) ] ).toEqual( [
            './images/alcohol/alcohol-1.jpg',
            './images/alcohol/alcohol-2.jpg',
            './images/alcohol/alcohol-3.jpg',
            './images/alcohol/alcohol-4.jpg',
            './images/alcohol/alcohol-5.jpg',
            './images/alcohol/alcohol-6.jpg',
            './images/alcohol/alcohol-7.jpg',
            './images/alcohol/alcohol-8.jpg',
            './images/alcohol/alcohol-9.jpg',
            './images/alcohol/alcohol-10.jpg',
            './images/alcohol/alcohol-11.jpg',
            './images/alcohol/alcohol-12.jpg',
            './images/alcohol/alcohol-13.jpg',
            './images/alcohol/alcohol-14.jpg',
            './images/alcohol/alcohol-15.jpg',
            './images/alcohol/alcohol-16.jpg',
            './images/alcohol/alcohol-17.jpg',
            './images/alcohol/alcohol-18.jpg',
            './images/alcohol/alcohol-19.jpg',
            './images/alcohol/alcohol-20.jpg',
            './images/alcohol/alcohol-21.jpg',
            './images/alcohol/alcohol-22.jpg',
            './images/alcohol/alcohol-23.jpg',
            './images/alcohol/alcohol-24.jpg'
        ] )


        // Soft Drink left oriented
        expect( [ ...myManifest.getSubcategory( 'softDrink', 'left' ) ] ).toEqual( [
            './images/softDrink/left/softDrink-1-left.jpg',
            './images/softDrink/left/softDrink-2-left.jpg',
            './images/softDrink/left/softDrink-3-left.jpg',
            './images/softDrink/left/softDrink-4-left.jpg',
            './images/softDrink/left/softDrink-5-left.jpg',
            './images/softDrink/left/softDrink-6-left.jpg',
            './images/softDrink/left/softDrink-7-left.jpg',
            './images/softDrink/left/softDrink-8-left.jpg',
            './images/softDrink/left/softDrink-9-left.jpg',
            './images/softDrink/left/softDrink-10-left.jpg',
            './images/softDrink/left/softDrink-11-left.jpg',
            './images/softDrink/left/softDrink-12-left.jpg',
            './images/softDrink/left/softDrink-13-left.jpg',
            './images/softDrink/left/softDrink-14-left.jpg',
            './images/softDrink/left/softDrink-15-left.jpg',
            './images/softDrink/left/softDrink-16-left.jpg',
            './images/softDrink/left/softDrink-17-left.jpg',
            './images/softDrink/left/softDrink-18-left.jpg',
            './images/softDrink/left/softDrink-19-left.jpg',
            './images/softDrink/left/softDrink-20-left.jpg',
            './images/softDrink/left/softDrink-21-left.jpg',
            './images/softDrink/left/softDrink-22-left.jpg',
            './images/softDrink/left/softDrink-23-left.jpg',
            './images/softDrink/left/softDrink-24-left.jpg'
        ] )


        // Surrender right oriented
        expect( [ ...myManifest.getSubcategory( 'surrender', 'right' ) ] ).toEqual( [
            './images/surrender/right/surrender-1-right.jpg',
            './images/surrender/right/surrender-2-right.jpg',
            './images/surrender/right/surrender-3-right.jpg',
            './images/surrender/right/surrender-4-right.jpg',
            './images/surrender/right/surrender-5-right.jpg',
            './images/surrender/right/surrender-6-right.jpg',
            './images/surrender/right/surrender-7-right.jpg',
            './images/surrender/right/surrender-8-right.jpg',
            './images/surrender/right/surrender-9-right.jpg',
            './images/surrender/right/surrender-10-right.jpg',
            './images/surrender/right/surrender-11-right.jpg',
            './images/surrender/right/surrender-12-right.jpg',
            './images/surrender/right/surrender-13-right.jpg',
            './images/surrender/right/surrender-14-right.jpg',
            './images/surrender/right/surrender-15-right.jpg',
            './images/surrender/right/surrender-16-right.jpg',
            './images/surrender/right/surrender-17-right.jpg',
            './images/surrender/right/surrender-18-right.jpg',
            './images/surrender/right/surrender-19-right.jpg',
            './images/surrender/right/surrender-20-right.jpg',
            './images/surrender/right/surrender-21-right.jpg',
            './images/surrender/right/surrender-22-right.jpg',
            './images/surrender/right/surrender-23-right.jpg',
            './images/surrender/right/surrender-24-right.jpg'
        ] )

    } )


} )



//// Validation ///////////////////////////////////////////////////////////////

describe( 'Validation', () => {



    test( 'Should return error if a wrong category is entered', () => {


        const myManifest = new Manifest()

        // First parameter is invalid
        expect( () => {
            myManifest.getSubcategory( 'aaa', 'bbb' )
        } ).toThrow( `["alcohol","softDrink","surrender"] must include "aaa"` )

        // Second parameter is invalid
        expect( () => {
            myManifest.getSubcategory( 'alcohol', 'bbb' )
        } ).toThrow( `["straight","left","right"] must include "bbb"` )

        // Second parameter is missing
        expect( () => {
            myManifest.getSubcategory( 'alcohol' )
        } ).toThrow( `{"0":"alcohol"} must have length of 2` )

    } )


} )