//// UMD HEAD ////////////////////////////////////////////////////////////////////////
// UMD head and foot patterns adapted from d3.js
( function ( global, factory ) {
    // noinspection JSUnresolvedVariable,JSUnresolvedFunction,NestedConditionalExpressionJS
    typeof exports === 'object' && typeof module !== 'undefined' ? factory( exports ) :
        typeof define === 'function' && define.amd ? define( [ 'exports' ], factory ) :
            ( factory( ( global.helperFunctions = global.helperFunctions || {} ) ) )
}( this, ( /**
 *
 * @param exports
 */
function ( exports ) {
    'use strict'
//////////////////////////////////////////////////////////////////////////////////////


//// Helper Functions ///////////////////////////////////////////////////////////////



    class Helper {

        constructor() {}


        static initializeDOMWithControlPanel(){

            document.body.innerHTML = `
       
            
                <!-- Cohort Dropdown -->
                <select name="cohort" id="cohortDropdown" class="dropdownMenu">
                    <option value="" selected disabled hidden>...</option>
                    <option value="clinical">Clinical</option>
                    <option value="nonClinical">Non-Clinical</option>
                </select>
    
        
                <!-- Sex Dropdown -->
                <select name="sex" id="sexDropdown" class="dropdownMenu">
                    <option value="" selected disabled hidden>...</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                </select>
                
                <!-- ID Text Box -->
                <input id="idInput" placeholder="..." type="number"/>


                <!-- Condition Dropdown -->
                 <select name="condition" id="conditionDropdown">
                    <option value="" selected disabled hidden>...</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>

                   
                <div class="counterArea alcohol">Category 1:&nbsp<span class="counterText alcohol">0</span>/8</div>
                <div class="counterArea softDrink">Category 2:&nbsp<span class="counterText softDrink">0</span>/8</div>
                <div class="counterArea surrender">Category 3:&nbsp<span class="counterText surrender">0</span>/8</div>
                <style>
                    .counterArea {background-color: inherit;}
                    .counterArea.noneSelected {background-color: inherit;}
                    .counterArea.someSelected {background-color: goldenrod;}
                    .counterArea.selectionLimitReached {background-color: greenYellow;}
                 </style>
            
                <!-- Images-->
                <div class="imageArea">
                    <div class="imageFrame" id="frame1"> <img id="alcoholPicture1" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-1.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame2"> <img id="alcoholPicture2" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-2.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame3"> <img id="alcoholPicture3" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-3.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame4"> <img id="alcoholPicture4" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-4.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame5"> <img id="alcoholPicture5" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-5.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame6"> <img id="alcoholPicture6" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-6.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame7"> <img id="alcoholPicture7" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-7.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame8"> <img id="alcoholPicture8" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-8.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame9"> <img id="alcoholPicture9" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-9.jpg" alt=""> </div>
                    
                    <div class="imageFrame" id="frame9"> <img id="softDrinkPicture1" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-1.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame10"> <img id="softDrinkPicture2" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-2.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame11"> <img id="softDrinkPicture3" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-3.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame12"> <img id="softDrinkPicture4" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-4.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame13"> <img id="softDrinkPicture5" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-5.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame14"> <img id="softDrinkPicture6" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-6.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame15"> <img id="softDrinkPicture7" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-7.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame16"> <img id="softDrinkPicture8" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-8.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame17"> <img id="softDrinkPicture9" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-9.jpg" alt=""> </div>
                       
                    <div class="imageFrame" id="frame17"> <img id="surrenderPicture1" class="image surrender" data-category="surrender" src="../images/surrender/surrender-1.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame18"> <img id="surrenderPicture2" class="image surrender" data-category="surrender" src="../images/surrender/surrender-2.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame19"> <img id="surrenderPicture3" class="image surrender" data-category="surrender" src="../images/surrender/surrender-3.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame20"> <img id="surrenderPicture4" class="image surrender" data-category="surrender" src="../images/surrender/surrender-4.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame21"> <img id="surrenderPicture5" class="image surrender" data-category="surrender" src="../images/surrender/surrender-5.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame22"> <img id="surrenderPicture6" class="image surrender" data-category="surrender" src="../images/surrender/surrender-6.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame23"> <img id="surrenderPicture7" class="image surrender" data-category="surrender" src="../images/surrender/surrender-7.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame24"> <img id="surrenderPicture8" class="image surrender" data-category="surrender" src="../images/surrender/surrender-8.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame25"> <img id="surrenderPicture9" class="image surrender" data-category="surrender" src="../images/surrender/surrender-9.jpg" alt=""> </div>
                    
                </div>
    
                <!-- Download Button -->
                <button id="downloadButton" disabled="disabled" ">Download</button>
            `

            controlPanel.initialize()

            document.querySelector( '#idInput' ).value = 5
            document.querySelector( '#conditionDropdown' ).value = '2'  // not automatically updated in this test
            document.querySelector( '#cohortDropdown' ).value = 'nonClinical'
            document.querySelector( '#sexDropdown' ).value = 'female'

        }


        /**
         * Clicks 24 images, 8 from each category, until the quota is reached.
         * @example
         * clickImagesUntilQuotaIsReached()
         */
        static clickImagesUntilQuotaIsReached() {
            // Element references
            // noinspection DuplicatedCode
            const alcoholPicture1 = document.querySelector( '#alcoholPicture1' )
                , alcoholPicture2 = document.querySelector( '#alcoholPicture2' )
                , alcoholPicture3 = document.querySelector( '#alcoholPicture3' )
                , alcoholPicture4 = document.querySelector( '#alcoholPicture4' )
                , alcoholPicture5 = document.querySelector( '#alcoholPicture5' )
                , alcoholPicture6 = document.querySelector( '#alcoholPicture6' )
                , alcoholPicture7 = document.querySelector( '#alcoholPicture7' )
                , alcoholPicture8 = document.querySelector( '#alcoholPicture8' )

            const softDrinkPicture1 = document.querySelector( '#softDrinkPicture1' )
                , softDrinkPicture2 = document.querySelector( '#softDrinkPicture2' )
                , softDrinkPicture3 = document.querySelector( '#softDrinkPicture3' )
                , softDrinkPicture4 = document.querySelector( '#softDrinkPicture4' )
                , softDrinkPicture5 = document.querySelector( '#softDrinkPicture5' )
                , softDrinkPicture6 = document.querySelector( '#softDrinkPicture6' )
                , softDrinkPicture7 = document.querySelector( '#softDrinkPicture7' )
                , softDrinkPicture8 = document.querySelector( '#softDrinkPicture8' )

            const surrenderPicture1 = document.querySelector( '#surrenderPicture1' )
                , surrenderPicture2 = document.querySelector( '#surrenderPicture2' )
                , surrenderPicture3 = document.querySelector( '#surrenderPicture3' )
                , surrenderPicture4 = document.querySelector( '#surrenderPicture4' )
                , surrenderPicture5 = document.querySelector( '#surrenderPicture5' )
                , surrenderPicture6 = document.querySelector( '#surrenderPicture6' )
                , surrenderPicture7 = document.querySelector( '#surrenderPicture7' )
                , surrenderPicture8 = document.querySelector( '#surrenderPicture8' )




            // Select all images up to the selection limit
            // noinspection DuplicatedCode
            alcoholPicture1.click()
            alcoholPicture2.click()
            alcoholPicture3.click()
            alcoholPicture4.click()
            alcoholPicture5.click()
            alcoholPicture6.click()
            alcoholPicture7.click()
            alcoholPicture8.click()

            softDrinkPicture1.click()
            softDrinkPicture2.click()
            softDrinkPicture3.click()
            softDrinkPicture4.click()
            softDrinkPicture5.click()
            softDrinkPicture6.click()
            softDrinkPicture7.click()
            softDrinkPicture8.click()

            surrenderPicture1.click()
            surrenderPicture2.click()
            surrenderPicture3.click()
            surrenderPicture4.click()
            surrenderPicture5.click()
            surrenderPicture6.click()
            surrenderPicture7.click()
            surrenderPicture8.click()

        }
    }




//// UMD FOOT ////////////////////////////////////////////////////////////////////////

    //// MODULE.EXPORTS ////
    exports.Helper = Helper
    Object.defineProperty( exports, '__esModule', { value: true } )

} ) ) )
//////////////////////////////////////////////////////////////////////////////////////

