//// Get Task ///////////////////////////////////////////////////////////////

describe( 'Get Task', () => {


    test( 'Should get the tree for a task', () => {

        const Manifest = fileManifest.Manifest

        // Get APBM of firs training
        expect( Object.keys( Manifest.getTask( 'firstTraining', 'APBM' ) ) ).toEqual( [
            'key',
            'directoryName',
            'fixedFileURLs'
        ] )

    } )

    test( 'Should get properties for a task', () => {

        const Manifest = fileManifest.Manifest

        // Get directory name
        expect( Manifest.getTask( 'firstTraining', 'APBM', 'directoryName' ) ).toBe(
            '04 - ApBM'
        )

        // Get URLs
        expect( Object.values( Manifest.getTask( 'firstTraining', 'APBM', 'fixedFileURLs' ) ) ).toEqual( [
            './inquisit/apbm/01-START-ApBM.iqx',
            './inquisit/apbm/02-ApBM-Part1.iqx',
            './inquisit/apbm/03-ApBM-Part2.iqx',
            './inquisit/apbm/instructions-part1.iqx',
            './inquisit/apbm/practice-left.png',
            './inquisit/apbm/practice-right.png',
            './inquisit/apbm/practiceBlock.iqx',
            './inquisit/apbm/sequenceGenerator-part1.iqx',
            './inquisit/apbm/sequenceGenerator-part2.iqx'
        ] )

    } )



    //// Validation ///////////////////////////////////////////////////////////////

    describe( 'Validation', () => {


        test( 'Should not allow querying with a bad property name', () => {

            const Manifest = fileManifest.Manifest

            // Get directory name
            expect( () => {
                Manifest.getTask( 'firstTraining', 'APBM', 'badPropertyName' )
            } ).toThrow( `["key","directoryName","fixedFileURLs"] must include "badPropertyName"` )


        } )


    } )

} )




//// Get Training Session ///////////////////////////////////////////////////////////////

describe( 'Get Training Session', () => {

    test( 'Should get a training session', () => {

        const Manifest = fileManifest.Manifest

        expect( Object.keys( Manifest.getSession( 'firstTraining' ) ) ).toEqual(
            [ 'key', 'directoryName', 'tasks' ]
        )
    } )

    test( 'Should get a property from a training session', () => {

        const Manifest = fileManifest.Manifest

        // Get directory name
        expect( Manifest.getSession( 'firstTraining', 'directoryName' ) ).toEqual(
            '01 - Pre-assessment and first training'
        )

        // Get sub directory names
        expect( Object.keys( Manifest.getSession( 'firstTraining', 'tasks' ) ) ).toEqual(
            [ 'IAT', 'AAT', 'DDT', 'APBM' ]
        )

    } )


    //// Validation ///////////////////////////////////////////////////////////////

    describe( 'Validation', () => {

        test( 'Should not allow querying with a bad property name', () => {

            const Manifest = fileManifest.Manifest

            expect( () => {
                Manifest.getSession( 'firstTraining', 'badPropertyName' )
            } ).toThrow( `["key","directoryName","tasks"] must include "badPropertyName"` )


        } )

    } )

} )

//// Get File Tree ///////////////////////////////////////////////////////////////

describe( 'Get File Tree', () => {


    test( 'Should get the entire file tree', () => {

        const Manifest = fileManifest.Manifest

        expect( Object.values( Manifest.getTree() ) ).toHaveLength( 5 )

    } )

} )



//// Summaries and Advanced Queries on the File Tree ///////////////////////////////////////////////////////////////

describe( 'Summaries and Advanced Queries on the File Tree', () => {


    test( 'Get all possible subdirectory names', () => {

        const Manifest = fileManifest.Manifest

        expect( Manifest.getAllTaskNames() ).toEqual( [
            'IAT',
            'AAT',
            'DDT',
            'APBM'
        ] )

    } )


    test( 'Add session and task keys as values to themselves', () => {

        const Manifest = fileManifest.Manifest
        Manifest.registerKeysAsOwnValues()

        expect( Manifest.getSession( 'firstTraining' ).key ).toBe( 'firstTraining' )
        Manifest.getTask( 'firstTraining' )

    } )



} )