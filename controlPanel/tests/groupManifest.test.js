//// Group Manifest ///////////////////////////////////////////////////////////////

describe( 'Group Manifest', () => {


    test( 'Get manifest', () => {
        const myManifest = new groupManifest.Manifest()
        expect( Object.values( myManifest.groups.clinical.male ) ).toHaveLength( 255 )
        expect( Object.values( myManifest.groups.clinical.female ) ).toHaveLength( 255 )
        expect( Object.values( myManifest.groups.nonClinical.male ) ).toHaveLength( 255 )
        expect( Object.values( myManifest.groups.nonClinical.female ) ).toHaveLength( 255 )
    } )

    test( 'Get specific record from manifest', () => {
        const myManifest = new groupManifest.Manifest()
        expect( myManifest.getCondition( 'clinical', 'male', 18 ) ).toBe( 3 )
        expect( myManifest.getCondition( 'clinical', 'female', 2 ) ).toBe( 2 )
        expect( myManifest.getCondition( 'nonClinical', 'male', 225 ) ).toBe( 2 )
        expect( myManifest.getCondition( 'nonClinical', 'female', 126 ) ).toBe( 1 )
    } )

    test( `Should fail if the 'id' parameter's value is above 255`, () => {

        const myManifest = new groupManifest.Manifest()

        expect( () => {
            expect( myManifest.getCondition( 'someWrongStatus', 'male', 100 ) ).toBe( 3 )
        } ).toThrow( `["clinical","nonClinical"] must include "someWrongStatus"` )



        expect( () => {
            myManifest.getCondition( 'clinical', 'male', 345 )
        } ).toThrow( '345 must be between 1 and 255' )

    } )

    test( 'Get group number', () => {

        const myManifest = new groupManifest.Manifest()
        expect( myManifest.getGroupNumber( 'clinical', 'male' ) ).toBe( 1 )
        expect( myManifest.getGroupNumber( 'clinical', 'female' ) ).toBe( 3 )
        expect( myManifest.getGroupNumber( 'nonClinical', 'male' ) ).toBe( 2 )
        expect( myManifest.getGroupNumber( 'nonClinical', 'female' ) ).toBe( 4 )

    } )


} )