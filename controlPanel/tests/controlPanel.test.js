// Imports
// noinspection MagicNumberJS

const { Images } = require( '../js/controlPanel' )
global.controlPanel = require( '../js/controlPanel' )
global.Panel = controlPanel.Panel
controlPanel.initialize()

// Setup
beforeEach( () => {

    document.body.innerHTML = `
       
            
                <!-- Cohort Dropdown -->
                <select name="cohort" id="cohortDropdown" class="dropdownMenu">
                    <option value="" selected disabled hidden>...</option>
                    <option value="clinical">Clinical</option>
                    <option value="nonClinical">Non-Clinical</option>
                </select>
    
        
                <!-- Sex Dropdown -->
                <select name="sex" id="sexDropdown" class="dropdownMenu">
                    <option value="" selected disabled hidden>...</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                </select>
                
                <!-- ID Text Box -->
                <input id="idInput" placeholder="..." type="number"/>


                <!-- Condition Dropdown -->
                 <select hidden disabled name="condition" id="conditionDropdown">
                    <option value="" selected disabled hidden>...</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>

                   
                <div class="counterArea alcohol">Category 1:&nbsp<span class="counterText alcohol">0</span>/8</div>
                <div class="counterArea softDrink">Category 2:&nbsp<span class="counterText softDrink">0</span>/8</div>
                <div class="counterArea surrender">Category 3:&nbsp<span class="counterText surrender">0</span>/8</div>
                <style>
                    .counterArea {background-color: inherit;}
                    .counterArea.noneSelected {background-color: inherit;}
                    .counterArea.someSelected {background-color: goldenrod;}
                    .counterArea.selectionLimitReached {background-color: greenYellow;}
                 </style>
            
                <!-- Images-->
                <div class="imageArea">
                    <div class="imageFrame" id="frame1"> <img id="alcoholPicture1" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-1.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame2"> <img id="alcoholPicture2" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-2.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame3"> <img id="alcoholPicture3" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-3.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame4"> <img id="alcoholPicture4" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-4.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame5"> <img id="alcoholPicture5" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-5.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame6"> <img id="alcoholPicture6" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-6.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame7"> <img id="alcoholPicture7" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-7.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame8"> <img id="alcoholPicture8" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-8.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame9"> <img id="alcoholPicture9" class="image alcohol" data-category="alcohol" src="../images/alcohol/alcohol-9.jpg" alt=""> </div>
                    
                    <div class="imageFrame" id="frame9"> <img id="softDrinkPicture1" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-1.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame10"> <img id="softDrinkPicture2" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-2.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame11"> <img id="softDrinkPicture3" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-3.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame12"> <img id="softDrinkPicture4" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-4.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame13"> <img id="softDrinkPicture5" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-5.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame14"> <img id="softDrinkPicture6" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-6.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame15"> <img id="softDrinkPicture7" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-7.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame16"> <img id="softDrinkPicture8" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-8.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame17"> <img id="softDrinkPicture9" class="image softDrink" data-category="softDrink" src="../images/softDrink/softDrink-9.jpg" alt=""> </div>
                       
                    <div class="imageFrame" id="frame17"> <img id="surrenderPicture1" class="image surrender" data-category="surrender" src="../images/surrender/surrender-1.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame18"> <img id="surrenderPicture2" class="image surrender" data-category="surrender" src="../images/surrender/surrender-2.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame19"> <img id="surrenderPicture3" class="image surrender" data-category="surrender" src="../images/surrender/surrender-3.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame20"> <img id="surrenderPicture4" class="image surrender" data-category="surrender" src="../images/surrender/surrender-4.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame21"> <img id="surrenderPicture5" class="image surrender" data-category="surrender" src="../images/surrender/surrender-5.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame22"> <img id="surrenderPicture6" class="image surrender" data-category="surrender" src="../images/surrender/surrender-6.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame23"> <img id="surrenderPicture7" class="image surrender" data-category="surrender" src="../images/surrender/surrender-7.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame24"> <img id="surrenderPicture8" class="image surrender" data-category="surrender" src="../images/surrender/surrender-8.jpg" alt=""> </div>
                    <div class="imageFrame" id="frame25"> <img id="surrenderPicture9" class="image surrender" data-category="surrender" src="../images/surrender/surrender-9.jpg" alt=""> </div>
                    
                </div>
    
                <!-- Download Button -->
                <button id="downloadButton" disabled="disabled" ">Download</button>
            `

} )


//// Meta ///////////////////////////////////////////////////////////////

describe( 'Meta', () => {

    test( 'Should check selection limit parameter', () => {
        // If this parameter is not at its regular value (e.g., due to a temporary change to make validation less
        // stringent in the browser), some tests may fail
        expect( controlPanel.selectionLimit ).toBe( 8 )
    } )

} )


//// Parse Control Panel  ///////////////////////////////////////////////////////////////

describe( 'Parse Control Panel ', () => {


    test( 'Should return parsed parameters from control panel', () => {

        document.body.innerHTML = `
            <!-- Cohort Dropdown -->
            <select name="cohort" id="cohortDropdown" class="dropdownMenu">
                <option value="" selected disabled hidden>...</option>
                <option value="clinical">Clinical</option>
                <option value="nonClinical">Non-Clinical</option>
            </select>
    
            <!-- Sex Dropdown -->
            <select name="sex" id="sexDropdown" class="dropdownMenu">
                <option value="" selected disabled hidden>...</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
            </select>
            
            <!-- ID Text Box -->
            <input id="idInput" placeholder="..." type="number"/>

            <!-- Condition Dropdown -->
             <select hidden disabled name="condition" id="conditionDropdown">
                <option value="" selected disabled hidden>...</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>
        `
        document.querySelector( '#idInput' ).value = 5
        document.querySelector( '#conditionDropdown' ).value = '2'  // not automatically updated in this test
        document.querySelector( '#cohortDropdown' ).value = 'nonClinical'
        document.querySelector( '#sexDropdown' ).value = 'female'



        const { cohort, sex, id, condition } = Panel.parseAll()
        expect( cohort ).toBe( 'nonClinical' )
        expect( sex ).toBe( 'female' )
        expect( id ).toBe( '5' )
        expect( condition ).toBe( '2' )

    } )

    test( 'Should report the condition the user selected', () => {

        document.body.innerHTML = `
            <select hidden disabled name="condition" id="conditionDropdown">
                <option value="" selected disabled hidden>...</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>
        `

        const conditionDropdown = document.querySelector( '#conditionDropdown' )
        conditionDropdown.value = 1
        expect( Panel.parseCondition() ).toBe( '1' )

    } )

    test( 'Should report the user-provided ID', () => {

        document.body.innerHTML = `
            <input id="idInput" placeholder="..." type="number" min="1"/>
        `

        const idTextBox = document.querySelector( '#idInput' )
        idTextBox.value = 12
        expect( Panel.parseID() ).toBe( '12' )

        idTextBox.value = 14
        expect( Panel.parseID() ).toBe( '14' )

    } )


    test( 'Should get user-provided sex', () => {


        document.body.innerHTML = `
            <!-- Sex dropdown -->
            <select id="sexDropdown">
                <option value="" selected disabled hidden>...</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
            </select>
        `

        document.querySelector( '#sexDropdown' ).value = 'female'
        expect( Panel.parseSex() ).toBe( 'female' )

        document.querySelector( '#sexDropdown' ).value = 'male'
        expect( Panel.parseSex() ).toBe( 'male' )

    } )


    test( 'Should get user-provided cohort', () => {

        document.body.innerHTML = `
            <!-- Cohort dropdown -->
            <select id="cohortDropdown">
                <option value="" selected disabled hidden>...</option>
                <option value="clinical">Clinical</option>
                <option value="nonClinical">Non-Clinical</option>
            </select>
    
        `

        document.querySelector( '#cohortDropdown' ).value = 'clinical'
        expect( Panel.parseCohort() ).toBe( 'clinical' )

        document.querySelector( '#cohortDropdown' ).value = 'nonClinical'
        expect( Panel.parseCohort() ).toBe( 'nonClinical' )

    } )

} )




//// Select image ///////////////////////////////////////////////////////////////



describe( 'Select images by clicking on them', () => {

    // Teardown
    afterEach( () => {
        document.body.innerHTML = ''
        selectionCounts.clear()
    } )


    test( 'Should toggle `selected` class when images are clicked on', () => {

        // Element references
        const alcoholPicture1 = document.querySelector( '#alcoholPicture1' )
        const frame1 = alcoholPicture1.parentNode

        // Picture should not have the `selected` class if not yet clicked
        expect( frame1 ).toHaveClass( 'imageFrame' )
        expect( frame1 ).not.toHaveClass( 'selected' )

        // Picture should have the `selected` class after clicking
        alcoholPicture1.click()
        expect( frame1 ).toHaveClass( 'imageFrame' )
        expect( frame1 ).toHaveClass( 'selected' )

        // After clicking again, picture should no longer have the `selected` class
        alcoholPicture1.click()
        expect( frame1 ).toHaveClass( 'imageFrame' )
        expect( frame1 ).not.toHaveClass( 'selected' )

    } )

    test( 'Should count number of images clicked', () => {

        const { Images } = require( '../js/controlPanel' )


        // Element references
        const alcoholPicture1 = document.querySelector( '#alcoholPicture1' )
            , alcoholPicture2 = document.querySelector( '#alcoholPicture2' )
        const surrenderPicture1 = document.querySelector( '#surrenderPicture1' )

        const currentlySelected = ( className ) => Images.numberOfSelected( className )
        expect( currentlySelected() ).toBe( 0 )


        // Select image
        alcoholPicture1.click()
        expect( currentlySelected( 'alcohol' ) ).toBe( 1 )
        expect( currentlySelected( 'someOtherCategory' ) ).toBe( 0 )

        // Select more images
        alcoholPicture2.click()
        surrenderPicture1.click()
        expect( currentlySelected() ).toBe( 3 )  // no. of all selected images
        expect( currentlySelected( 'alcohol' ) ).toBe( 2 )
        expect( currentlySelected( 'surrender' ) ).toBe( 1 )

        // Deselect one image
        alcoholPicture2.click()
        expect( currentlySelected() ).toBe( 2 )  // no. of all selected images
        expect( currentlySelected( 'alcohol' ) ).toBe( 1 )
        expect( currentlySelected( 'surrender' ) ).toBe( 1 )

    } )

    test( 'JS counters should show counts', () => {

        // Element references
        const alcoholPicture1 = document.querySelector( '#alcoholPicture1' )
            , alcoholPicture2 = document.querySelector( '#alcoholPicture2' )
        const surrenderPicture1 = document.querySelector( '#surrenderPicture1' )

        const { Images } = require( '../js/controlPanel' )
        const countOf = ( category ) => Images.counts( category )

        // Select image
        alcoholPicture1.click()
        expect( countOf( 'alcohol' ) ).toBe( 1 )

        // Select more images
        alcoholPicture2.click()
        surrenderPicture1.click()
        expect( countOf( 'alcohol' ) ).toBe( 2 )
        expect( countOf( 'surrender' ) ).toBe( 1 )


        // Deselect one image
        alcoholPicture2.click()
        expect( countOf( 'alcohol' ) ).toBe( 1 )
        expect( countOf( 'surrender' ) ).toBe( 1 )

    } )

    test( 'Navbar counters should show counts', () => {

        // Element references
        const alcoholPicture1 = document.querySelector( '#alcoholPicture1' )
            , alcoholPicture2 = document.querySelector( '#alcoholPicture2' )
        const surrenderPicture1 = document.querySelector( '#surrenderPicture1' )

        // Select image
        alcoholPicture1.click()
        let alcoholCount = () => document.querySelector( '.alcohol.counterText' ).textContent
        expect( alcoholCount() ).toBe( '1' )

        // Select more images
        alcoholPicture2.click()
        expect( alcoholCount() ).toBe( '2' )

        surrenderPicture1.click()
        expect( alcoholCount() ).toBe( '2' )

        // Deselect one image
        alcoholPicture2.click()

    } )


    test( 'Should tell when all quotas are reached', () => {

        expect( Images.areAllQuotasFilled() ).toBe( false )
        Helper.clickImagesUntilQuotaIsReached()
        expect( Images.areAllQuotasFilled() ).toBe( true )


    } )

    test( 'Should tell when each quota is reached', () => {

        expect( Images.isQuotaReached( 'alcohol' ) ).toBe( false )
        Helper.clickImagesUntilQuotaIsReached()
        expect( Images.isQuotaReached( 'alcohol' ) ).toBe( true )


    } )

    test( 'Navbar colors should change when sufficient pics are selected', () => {
        // Element references
        const alcoholPicture1 = document.querySelector( '#alcoholPicture1' )
            , alcoholPicture2 = document.querySelector( '#alcoholPicture2' )
            , alcoholPicture3 = document.querySelector( '#alcoholPicture3' )
            , alcoholPicture4 = document.querySelector( '#alcoholPicture4' )
            , alcoholPicture5 = document.querySelector( '#alcoholPicture5' )
            , alcoholPicture6 = document.querySelector( '#alcoholPicture6' )
            , alcoholPicture7 = document.querySelector( '#alcoholPicture7' )
            , alcoholPicture8 = document.querySelector( '#alcoholPicture8' )

        const alcoholCounterArea = document.querySelector( '.counterArea.alcohol' )
        const alcoholCounterAreaBGColor = () => getComputedStyle( alcoholCounterArea ).backgroundColor

        // Background color when no image is selected
        expect( alcoholCounterAreaBGColor() ).toBe( 'inherit' )

        // Select image
        alcoholPicture1.click()
        expect( alcoholCounterAreaBGColor() ).toBe( 'goldenrod' )

        // Deselect image
        alcoholPicture1.click()
        expect( alcoholCounterAreaBGColor() ).toBe( 'inherit' )


        // Select all images up to the selection limit
        alcoholPicture1.click()
        alcoholPicture2.click()
        alcoholPicture3.click()
        alcoholPicture4.click()
        alcoholPicture5.click()
        alcoholPicture6.click()
        alcoholPicture7.click()
        alcoholPicture8.click()

        expect( alcoholCounterAreaBGColor() ).toBe( 'greenYellow' )

        // Deselect one image
        alcoholPicture8.click()
        expect( alcoholCounterAreaBGColor() ).toBe( 'goldenrod' )

        // Deselect all images
        alcoholPicture1.click()
        alcoholPicture2.click()
        alcoholPicture3.click()
        alcoholPicture4.click()
        alcoholPicture5.click()
        alcoholPicture6.click()
        alcoholPicture7.click()
        expect( alcoholCounterAreaBGColor() ).toBe( 'inherit' )

    } )


    it( 'Should not be possible to select any more images when the selection quota is reached', () => {

        // Click 24 images, 8 from each category, until the quota is reached
        Helper.clickImagesUntilQuotaIsReached()
        expect( Images.numberOfSelected( 'alcohol' ) ).toBe( 8 )
        expect( Images.numberOfSelected( 'surrender' ) ).toBe( 8 )
        expect( Images.numberOfSelected( 'softDrink' ) ).toBe( 8 )

        // Check one of the images; it should have the `selected` class
        const frame8 = document.querySelector( '#frame8' )
        expect( frame8 ).toHaveClass( 'selected' )

        // Click on another image
        const alcoholPicture9 = document.querySelector( '#alcoholPicture9' )
        alcoholPicture9.click()

        // The newly clicked image should not have the class `selected` in it
        const frame9 = document.querySelector( '#frame9' )
        expect( Images.numberOfSelected( 'alcohol' ) ).toBe( 8 )
        expect( frame9 ).not.toHaveClass( 'selected' )


    } )

} )




//// Download Button Activation ///////////////////////////////////////////////////////////////

describe( 'Download Button Activation', () => {

    // noinspection FunctionTooLongJS
    test( 'Download button should be disabled in the beginning, and enabled when conditions are met', () => {

        // Element references
        // noinspection DuplicatedCode
        const alcoholPicture1 = document.querySelector( '#alcoholPicture1' )
            , alcoholPicture2 = document.querySelector( '#alcoholPicture2' )
            , alcoholPicture3 = document.querySelector( '#alcoholPicture3' )
            , alcoholPicture4 = document.querySelector( '#alcoholPicture4' )
            , alcoholPicture5 = document.querySelector( '#alcoholPicture5' )
            , alcoholPicture6 = document.querySelector( '#alcoholPicture6' )
            , alcoholPicture7 = document.querySelector( '#alcoholPicture7' )
            , alcoholPicture8 = document.querySelector( '#alcoholPicture8' )

        const softDrinkPicture1 = document.querySelector( '#softDrinkPicture1' )
            , softDrinkPicture2 = document.querySelector( '#softDrinkPicture2' )
            , softDrinkPicture3 = document.querySelector( '#softDrinkPicture3' )
            , softDrinkPicture4 = document.querySelector( '#softDrinkPicture4' )
            , softDrinkPicture5 = document.querySelector( '#softDrinkPicture5' )
            , softDrinkPicture6 = document.querySelector( '#softDrinkPicture6' )
            , softDrinkPicture7 = document.querySelector( '#softDrinkPicture7' )
            , softDrinkPicture8 = document.querySelector( '#softDrinkPicture8' )

        const surrenderPicture1 = document.querySelector( '#surrenderPicture1' )
            , surrenderPicture2 = document.querySelector( '#surrenderPicture2' )
            , surrenderPicture3 = document.querySelector( '#surrenderPicture3' )
            , surrenderPicture4 = document.querySelector( '#surrenderPicture4' )
            , surrenderPicture5 = document.querySelector( '#surrenderPicture5' )
            , surrenderPicture6 = document.querySelector( '#surrenderPicture6' )
            , surrenderPicture7 = document.querySelector( '#surrenderPicture7' )
            , surrenderPicture8 = document.querySelector( '#surrenderPicture8' )

        const downloadButtonIsEnabled = () => !document.querySelector( '#downloadButton' ).disabled

        // Download button should be disabled in the beginning
        expect( downloadButtonIsEnabled() ).toBeFalsy()

        // Select all images up to the selection limit
        // noinspection DuplicatedCode
        alcoholPicture1.click()
        alcoholPicture2.click()
        alcoholPicture3.click()
        alcoholPicture4.click()
        alcoholPicture5.click()
        alcoholPicture6.click()
        alcoholPicture7.click()
        alcoholPicture8.click()

        softDrinkPicture1.click()
        softDrinkPicture2.click()
        softDrinkPicture3.click()
        softDrinkPicture4.click()
        softDrinkPicture5.click()
        softDrinkPicture6.click()
        softDrinkPicture7.click()
        softDrinkPicture8.click()

        surrenderPicture1.click()
        surrenderPicture2.click()
        surrenderPicture3.click()
        surrenderPicture4.click()
        surrenderPicture5.click()
        surrenderPicture6.click()
        surrenderPicture7.click()
        surrenderPicture8.click()

        // Download button should still not be enabled
        expect( downloadButtonIsEnabled() ).toBeFalsy()

        // User provides input for the ID text box
        document.querySelector( '#idInput' ).value = 9
        Panel.updateDownloadButton()  // Triggered automatically in actual HTML by `oninput` trigger of text box
        // Download button should still not be enabled
        expect( downloadButtonIsEnabled() ).toBeFalsy()

        // User provides input for the cohort dropdown
        document.querySelector( '#cohortDropdown' ).value = 'clinical'
        Panel.updateDownloadButton()
        Panel.updateConditionDropdown()  // Triggered automatically in actual HTML by `oninput` trigger of
        // The value of the condition dropdown should not yet be calculated
        expect( Panel.parseCondition() ).toBe( '' )
        // Download button should still not be enabled
        expect( downloadButtonIsEnabled() ).toBeFalsy()


        // User provides input for the sex dropdown
        document.querySelector( '#sexDropdown' ).value = 'female'
        Panel.updateDownloadButton()
        Panel.updateConditionDropdown()  // Triggered automatically in actual HTML by `oninput` trigger of text box
        // Download button should still not be enabled
        expect( downloadButtonIsEnabled() ).toBeFalsy()


        // The value of the condition dropdown should be calculated by now
        expect( Panel.parseCondition() ).toBe( '2' )
        Panel.updateDownloadButton()   // Triggered automatically in actual HTML by `onchange` trigger of text box
        // Download button should be ENABLED NOW
        expect( downloadButtonIsEnabled() ).toBeTruthy()


        // Deselecting one image should disable the download button
        alcoholPicture1.click()
        expect( downloadButtonIsEnabled() ).toBeFalsy()

    } )

    test( 'Should return true if user provided input (helper function)', () => {

        // Check if text box is filled
        expect( Panel.isProvided( '#idInput' ) ).toBe( false )
        document.querySelector( '#idInput' ).value = 9
        expect( Panel.isProvided( '#idInput' ) ).toBe( true )

        // Check if a dropdown item is selected
        expect( Panel.isProvided( '#conditionDropdown' ) ).toBe( false )
        document.querySelector( '#conditionDropdown' ).value = 1
        expect( Panel.isProvided( '#conditionDropdown' ) ).toBe( true )


    } )


} )




//// ID, Group, and Condition Synchronization  ///////////////////////////////////////////////////////////////

describe( 'ID, Group, and Sync ', () => {


    // Setup
    beforeEach( () => {

        document.body.innerHTML = `
        
            <!--ID Text Box-->
            <label for="idInput">No. in group<span id="groupNumberSpan"></span>:</label>
            <div id="idInputContainer">
                <input id="idInput" placeholder="..." type="number" min="1"/>
            </div>
    
            <!-- Cohort Dropdown -->
            <select id="cohortDropdown">
                <option value="" selected disabled hidden>...</option>
                <option value="clinical">Yes</option>
                <option value="nonClinical">No</option>
            </select>
    
    
            <!-- Sex Dropdown -->
            <select id="sexDropdown">
                <option value="" selected disabled hidden>...</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
            </select>
    
    
            <!-- Condition Dropdown -->
            <select hidden disabled id="conditionDropdown">
                <option value="" selected disabled hidden>...</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
            </select>
                
        `
    } )


    test( 'Changes in ID, sex, and clinical condition dropdowns should update the condition text box', () => {


        document.querySelector( '#cohortDropdown' ).value = 'nonClinical'
        document.querySelector( '#sexDropdown' ).value = 'female'
        document.querySelector( '#idInput' ).value = 12

        Panel.updateConditionDropdown()
        expect( Panel.parseCondition() ).toBe( '2' )

    } )

    test( 'Condition text box should not update if all other variables in the control panel are not yet provided', () => {

        // Sex and ID missing
        document.querySelector( '#cohortDropdown' ).value = 'clinical'
        Panel.updateConditionDropdown()
        expect( Panel.parseCondition() ).toBe( '' )

        // ID missing
        document.querySelector( '#sexDropdown' ).value = 'male'
        Panel.updateConditionDropdown()
        expect( Panel.parseCondition() ).toBe( '' )

        // All in
        document.querySelector( '#idInput' ).value = 5
        Panel.updateConditionDropdown()
        expect( Panel.parseCondition() ).toBe( '3' )

    } )


    test( 'Should update group text within ID description', () => {

        const groupNumberSpan = document.querySelector( '#groupNumberSpan' )
        expect( groupNumberSpan.textContent ).toBe( '' )

        // Set input variables associated with group number
        document.querySelector( '#cohortDropdown' ).value = 'nonClinical'
        document.querySelector( '#sexDropdown' ).value = 'female'

        Panel.updateGroupNumberSpan()
        expect( groupNumberSpan.textContent ).toBe( '4' )


        // Change input variables
        document.querySelector( '#cohortDropdown' ).value = 'clinical'
        document.querySelector( '#sexDropdown' ).value = 'male'

        Panel.updateGroupNumberSpan()
        expect( groupNumberSpan.textContent ).toBe( '1' )

    } )


} )

//// List selected images ///////////////////////////////////////////////////////////////

describe( 'List selected images', () => {

    const { Images } = require( '../js/controlPanel' )

    //Setup
    beforeEach( () => {

        document.body.innerHTML = `
<div class="imageArea">
    <div class="imageFrame"> <img id="picture1" class="image alcohol" src="../images/alcohol/alcohol-1.jpg" alt=""> </div>
    <div class="imageFrame"> <img id="picture2" class="image softDrink" src="../images/softDrink/softDrink-1.jpg" alt=""> </div>
    <div class="imageFrame"> <img id="picture3" class="image surrender" src="../images/surrender/surrender-1.jpg" alt=""> </div>
    <div class="imageFrame"> <img id="picture4" class="image surrender" src="../images/surrender/surrender-2.jpg" alt=""> </div>
</div>`

    } )

    // Teardown
    afterEach( () => {
        document.body.innerHTML = ''
    } )


    test( 'Should list selected images', () => {

        // Select elements
        const picture1 = document.querySelector( '#picture1' )
        const picture2 = document.querySelector( '#picture2' )

        // Click on two pictures
        picture1.click()
        picture2.click()

        // Selected pictures should be listed
        const selectedImages = Images.listSelected()
        expect( selectedImages ).toEqual( [
            'http://localhost/images/alcohol/alcohol-1.jpg',
            'http://localhost/images/softDrink/softDrink-1.jpg'
        ] )

    } )


    test( 'Should list selected images of a certain category', () => {

        // Select elements
        const picture1 = document.querySelector( '#picture1' )
        const picture2 = document.querySelector( '#picture2' )
        const picture3 = document.querySelector( '#picture3' )
        const picture4 = document.querySelector( '#picture4' )

        // Click on two images
        picture1.click()
        picture2.click()
        picture3.click()
        picture4.click()

        // Selected alcohol images should be listed
        const selectedAlcoholImages = Images.listSelected( 'alcohol' )
        expect( selectedAlcoholImages ).toEqual( [
            'http://localhost/images/alcohol/alcohol-1.jpg'
        ] )

        // Selected surrender images should be listed
        const selectedSurrenderImages = Images.listSelected( 'surrender' )
        expect( selectedSurrenderImages ).toEqual( [
            'http://localhost/images/surrender/surrender-1.jpg',
            'http://localhost/images/surrender/surrender-2.jpg'
        ] )

    } )


} )


//// Auto-generate HTML from images ///////////////////////////////////////////////////////////////

describe( 'Auto-generate HTML from images', () => {

    const { Images } = require( '../js/controlPanel' )

    // Setup
    beforeEach( () => {

        document.body.innerHTML = `<div id="documentImageArea"/>`

    } )

    // Teardown
    afterEach( () => {
        document.body.innerHTML = ``
    } )


    test( 'Should generate sections from image manifest', () => {

        Images.generateHtmlOfImages()

        // Sections should exist
        const section = document.querySelectorAll( '.section' )
        expect( section ).toHaveLength( 3 )

        // A section should have correct class
        const aSection = document.querySelector( '.section' )

        // A section should have the correct parent element
        // noinspection JSUnresolvedVariable
        expect( aSection.parentNode.id ).toBe( 'documentImageArea' )

        // A section should have the correct child elements
        // noinspection JSUnresolvedVariable
        expect( aSection.childNodes[ 1 ].id ).toBe( 'alcoholSectionHeaderArea' )
        // noinspection JSUnresolvedVariable
        expect( aSection.childNodes[ 3 ].id ).toBe( 'alcoholImageArea' ) // not sure why this is 5, and not 2
    } )

    test( 'Should generate images and frame divs from image manifest', () => {

        Images.generateHtmlOfImages()

        // The number of images should be correct
        const images = document.querySelectorAll( 'img' )
        expect( images.length ).toBe( 72 )

        // The number of image frames should be correct
        const imageFrames = document.querySelectorAll( '.imageFrame' )
        expect( imageFrames.length ).toBe( 72 )

    } )


} )




//// Overlay Message ///////////////////////////////////////////////////////////////

describe( 'Overlay Message', () => {


    it( 'Should show specified overlay message and then the message should be removed from DOM', () => {

        Helper.clickImagesUntilQuotaIsReached()

        jest.useFakeTimers()

        let overlayMessage = () => document.querySelector( '.overlayMessage' )
        expect( overlayMessage() ).not.toBeInTheDocument()

        // Show the message
        const message = 'Quota is full.'
        const timeOut = 2000
        Panel.displayOverlayMessage( message, timeOut )
        expect( overlayMessage() ).toBeInTheDocument()

        // After the timeout, the message should be removed from DOM
        jest.advanceTimersByTime( timeOut - 1 )
        expect( overlayMessage() ).toBeInTheDocument()
        jest.advanceTimersByTime( timeOut )
        expect( overlayMessage() ).not.toBeInTheDocument()

    } )


} )


//// Version ///////////////////////////////////////////////////////////////

describe( 'Version', () => {


    test( 'Should get version', () => {

        expect( Panel.version() ).toBe( '2021.08.27b' )

    } )

    test( 'Should automatically add version text', () => {

        document.body.innerHTML = `
            <p id="versionText">v<span id="versionSpan"></span></p>
        `

        const versionText = document.querySelector( '#versionText' )
        const versionSpan = document.querySelector( '#versionSpan' )
        expect( versionSpan.textContent ).toBe( '' )

        Panel.updateVersion()
        expect( versionSpan.textContent ).toBe( '2021.08.27b' )
        expect( versionText.textContent ).toBe( 'v2021.08.27b' )


    } )



} )