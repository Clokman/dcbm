//// UNIT TESTS ////////////////////////////////////////////////////////////////////////////////////////////////////////

const { configure } = require( '@testing-library/dom' )

configure(
    {
        asyncUtilTimeout: 2,
        testIdAttribute: 'id'
    }
)

//// Generation and Parameters ///////////////////////////////////////////////////////////////

describe( 'Generation and Parameters', () => {

    test( 'Should generate a sequence of digits', () => {

        const sequence1 = new sequenceGenerator.Sequence( [ 1, 2, 3, 4, 5, 6 ] )
        expect( sequence1.array().length ).toEqual( 10 )

        const sequence2 = new sequenceGenerator.Sequence()
        expect( sequence2.array() ).not.toEqual( sequence1.array() )

    } )

    test( 'Should return a sequence of specified length ', () => {

        // A 48-digit-long sequence
        const members = [ 1, 2, 3, 4, 5, 6 ]
        let sequenceLength = 48
        const sequence1 = new sequenceGenerator.Sequence( members, sequenceLength )
        expect( sequence1.array().length ).toEqual( 48 )

        // A 215-digit-long sequence
        sequenceLength = 215
        const sequence2 = new sequenceGenerator.Sequence( members, sequenceLength )
        expect( sequence2.array().length ).toEqual( 215 )


    } )

    test( 'Should generate numbers in the right range', () => {

        const members = [ 4, 5 ]
            , sequenceLength = 100

        // Generate sequence
        const mySequence = new sequenceGenerator.Sequence( members, sequenceLength )
        // Get generated sequence as string
        const myString = mySequence.string()

        // The digits within range should be present
        expect( myString.search( '4' ) ).not.toBe( -1 )  // -1 means no match is found
        expect( myString.search( '5' ) ).not.toBe( -1 )


        // There should be no digits outside the range
        expect( myString.search( '0' ) ).toBe( -1 )  // -1 means no match is found
        expect( myString.search( '1' ) ).toBe( -1 )
        expect( myString.search( '2' ) ).toBe( -1 )
        expect( myString.search( '3' ) ).toBe( -1 )
        expect( myString.search( '6' ) ).toBe( -1 )
        expect( myString.search( '7' ) ).toBe( -1 )
        expect( myString.search( '8' ) ).toBe( -1 )
        expect( myString.search( '9' ) ).toBe( -1 )

    } )


    test( 'Should generate each digit in equal proportions for 6 members', () => {


        const members = [ 1, 2, 3, 4, 5, 6 ]
            , sequenceLength = 150
        const mySequence = new sequenceGenerator.Sequence( members, sequenceLength )

        const sequenceArray = mySequence.array()

        // The counts should be equal
        const counts = _.countBy( sequenceArray )
        expect( counts[ '1' ] ).toEqual( counts[ '2' ] )
        expect( counts[ '2' ] ).toEqual( counts[ '3' ] )
        expect( counts[ '3' ] ).toEqual( counts[ '4' ] )
        expect( counts[ '4' ] ).toEqual( counts[ '5' ] )
        expect( counts[ '5' ] ).toEqual( counts[ '6' ] )

    } )

    test( 'Should generate each digit in equal proportions for 4 members', () => {

        const members = [ 1, 4, 5, 6 ]
            , sequenceLength = 150
        // Generate sequence
        const mySequence = new sequenceGenerator.Sequence( members, sequenceLength )
        // Get generated sequence as array
        const sequenceArray = mySequence.array()

        // The counts should be (roughly) equal
        const counts = _.countBy( sequenceArray )
        const expectedSubgroupLength = sequenceLength / members.length
        const lowestAcceptableSubgroupLength = expectedSubgroupLength - 2
        expect( counts[ '1' ] ).toBeGreaterThan( lowestAcceptableSubgroupLength )
        expect( counts[ '4' ] ).toBeGreaterThan( lowestAcceptableSubgroupLength )
        expect( counts[ '5' ] ).toBeGreaterThan( lowestAcceptableSubgroupLength )
        expect( counts[ '6' ] ).toBeGreaterThan( lowestAcceptableSubgroupLength )

    } )


    test( 'Should generate each digit in the specified proportions (proportions array in decimal notation)', () => {

        // Assign parameters
        const members = [ 1, 4, 5, 6 ]
            , proportionsInDecimalNotation = [ 0.375, 0.375, 0.125, 0.125 ]  // <- Decimal notation
            , proportionsInDivisionNotation = [ 3 / 8, 3 / 8, 1 / 8, 1 / 8 ]  // <- Decimal notation
            , sequenceLength = 150
        // It should be possible to use decimal and division notation interchangeably
        expect( proportionsInDecimalNotation ).toEqual( proportionsInDivisionNotation )
        const proportions = proportionsInDecimalNotation

        // Generate sequence
        const mySequence = new sequenceGenerator.Sequence( members, sequenceLength, proportions )
        // Get generated sequence as array
        const sequenceArray = mySequence.array()

        // The counts should be equal
        const counts = _.countBy( sequenceArray )
        // Counts for 1s
        const expectedNumberOf1s = sequenceLength * proportions[ 0 ]
            , lowestNumberOfAcceptableNumberOf1s = expectedNumberOf1s - 2
        expect( counts[ '1' ] ).toBeGreaterThan( lowestNumberOfAcceptableNumberOf1s )
        // Counts for 4s
        const expectedNumberOf4s = sequenceLength * proportions[ 1 ]
            , lowestNumberOfAcceptableNumberOf4s = expectedNumberOf4s - 2
        expect( counts[ '4' ] ).toBeGreaterThan( lowestNumberOfAcceptableNumberOf4s )
        // Counts for 5s
        const expectedNumberOf5s = sequenceLength * proportions[ 2 ]
            , lowestNumberOfAcceptableNumberOf5s = expectedNumberOf5s - 2
        expect( counts[ '5' ] ).toBeGreaterThan( lowestNumberOfAcceptableNumberOf5s )
        // Counts for 6s
        const expectedNumberOf6s = sequenceLength * proportions[ 3 ]
            , lowestNumberOfAcceptableNumberOf6s = expectedNumberOf6s - 2
        expect( counts[ '6' ] ).toBeGreaterThan( lowestNumberOfAcceptableNumberOf6s )

    } )

} )


//// Output Format ///////////////////////////////////////////////////////////////

describe( 'Output Format', () => {


    test( 'Should print sequence as a string', () => {

        const mySequence = new sequenceGenerator.Sequence()

        // The type should be string
        const myString = mySequence.string()
        expect( myString.constructor.name ).toBe( 'String' )

        // There should be no commas in the string
        expect( myString.search( ',' ) ).toBe( -1 )  // -1 means no match is found

    } )


    test( 'Should return the sequence in Inquisit format', () => {

        const mySequence = new sequenceGenerator.Sequence()

        // The type should be string
        const myString = mySequence.inquisitString()
        expect( myString.constructor.name ).toBe( 'String' )

        // There should be Inquisit syntax in the returned string
        expect( myString ).toContain( '<values>' )
        expect( myString ).toContain( '/generatedSequence' )
        expect( myString ).toContain( '</values>' )


    } )


} )


//// Generation Constraints ///////////////////////////////////////////////////////////////

describe( 'Generation Constraints', () => {

    test( 'Elements of a digit group should not be consecutively repeated three times or more', () => {

        const inGroup = sequenceGenerator.Sequence.inGroup

        const members = [ 1, 2, 3, 4, 5, 6 ]
            , sequenceLength = 10
        const mySequence = new sequenceGenerator.Sequence( members, sequenceLength )

        const sequenceArray = mySequence.array()

        // If there are three consecutive members of a group, make flag = true
        let flag = false
        sequenceArray.forEach( ( digit, i, array ) => {
            if( i > 1 &&
                inGroup( digit ) === inGroup( array[ i - 1 ] ) &&
                inGroup( digit ) === inGroup( array[ i - 2 ] )
            ) {

                flag = true

            }
        } )

        expect( flag ).toBeFalsy()

    } )

    test( 'There should be no more than three consecutive odd or even digits', () => {

        const members = [ 1, 2, 3, 4, 5, 6 ]
            , sequenceLength = 10

        const mySequence = new sequenceGenerator.Sequence( members, sequenceLength )

        const sequenceArray = mySequence.array()

        // If there are three consecutively even or odd digits, make flag = true
        let flag = false
        sequenceArray.forEach( ( digit, i, array ) => {
            if( i > 1 &&
                digit % 2 === array[ i - 1 ] % 2 &&
                digit % 2 === array[ i - 2 ] % 2 ) {

                flag = true

            }
        } )

        expect( flag ).toBeFalsy()

    } )


} )




//// Writing to File ///////////////////////////////////////////////////////////////

describe( 'Writing to File', () => {

    test( 'Should write sequence to a file and read back from it', () => {

        const mySequence = new sequenceGenerator.Sequence()
        mySequence.writeString( './tests/output/Sequence' )

        // TODO: Reading the file does not work
        // const readSequence = fs.readFile( '/Users/clokman/Code/DCBM/sequenceGenerator/sequence.md', 'utf8',( error,
        // data ) => { return error } ) expect( readSequence.length ).toBe(  )
    } )

    test( 'Should write sequence to an INQUISIT file and read back from it', () => {

        const mySequence = new sequenceGenerator.Sequence()
        mySequence.writeInquisit( './tests/output/Sequence.iqx' )

        // TODO: Reading the file does not work
        // const readSequence = fs.readFile( '/Users/clokman/Code/DCBM/sequenceGenerator/sequence.md', 'utf8',( error,
        // data ) => { return error } ) expect( readSequence.length ).toBe(  )
    } )

} )



//// Helper Functions /////////////////////////¡//////////////////////////////////////

describe( 'Helper Functions', () => {

    test( '`inGroup` Should take a digit and return its group name', () => {

        expect( sequenceGenerator.Sequence.inGroup( 1 ) ).toBe( 'group1' )
        expect( sequenceGenerator.Sequence.inGroup( 2 ) ).toBe( 'group1' )
        expect( sequenceGenerator.Sequence.inGroup( 3 ) ).toBe( 'group2' )
        expect( sequenceGenerator.Sequence.inGroup( 4 ) ).toBe( 'group2' )
        expect( sequenceGenerator.Sequence.inGroup( 5 ) ).toBe( 'group3' )
        expect( sequenceGenerator.Sequence.inGroup( 6 ) ).toBe( 'group3' )

    } )

} )