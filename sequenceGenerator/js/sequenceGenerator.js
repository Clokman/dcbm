//// UMD HEAD ////////////////////////////////////////////////////////////////////////
// UMD head and foot patterns adapted from d3.js
( function ( global, factory ) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory( exports ) :
        typeof define === 'function' && define.amd ? define( [ 'exports' ], factory ) :
            ( factory( ( global.sequenceGenerator = global.sequenceGenerator || {} ) ) )
}( this, ( function ( exports ) {
    'use strict'




//////////////////////////////////////////////////////////////////////////////////////

    class Sequence {

        /**
         * @param members {Array} The members to generate the sequence from
         * @param length {Number} The length of the sequence to be generated
         * @param proportions {Array} The proportions corresponding to each item in the members array
         */
        constructor( members, length, proportions ) {
            this._sequenceArray = this._generate( members, length, proportions )
        }


        array() {
            return this._sequenceArray
        }


        string() {
            const stringWithCommas = _.toString( this.array() )
            const stringWithoutCommas = stringWithCommas.replace( /,/g, '' )

            return stringWithoutCommas
        }


        inquisitString() {

            const string = this.string()
            const inquisitString = `
The conditions and counterbalancing is controlled by the sequence of numbers in this file.
The sequence consists of semi-random digits that determine which trials will be present in the experiment and in which order. 

The digits in the sequence and their description is as follows ('left' and 'right' indicate tilt direction):
   
    1: alcohol left
    2: alcohol right
    3: softDrink left
    4: softDrink right
    5: surrender left
    6: surrender right
      
The digits that appear in each condition, and their distributions, are as follows:

Sham condition:
    - Condition 1a: 1, 2, 3, 4, 5, 6 (all trial types with [1/6] of the generated digits per type)
    - Condition 1b: 1, 2, 3, 4, 5, 6 (all, [1/6] of digits per type)

Standard ApBM:
    - Condition 2a: 1, 4, 5, 6 (alcohol left [3/8], softDrink right [3/8], surrender left [1/8], surrender right [1/8])
    - Condition 2b: 2, 3, 5, 6 (alcohol right [3/8], softDrink left [3/8], surrender left [1/8], surrender right [1/8])

Modified ApBM:
    - Condition 3a: 1, 3, 4, 6 (alcohol left [3/8], softDrink left [1/8], softDrink right [1/8], surrender right [3/8])
    - Condition 3b: 2, 3, 4, 5 (alcohol right [3/8], softDrink left [1/8], softDrink right [1/8], surrender left [3/8])
    
    
The sequence is not entirely random, as there are two rules adhered to during sequence generation:
     
    1. A digit can be repeated a maximum of three times consecutively. This follows from constraint #1 in
    Wiers et al. 2009: "No more than three consecutive stimuli of the same category."
    
    2. There should be no more than three consecutive odd or even digits. This follows constraint #2 in
    Wiers et al. 2009: "No more than three consecutive stimuli of the same format". In the .iqx file, odd
    item codes indicate right-tilted stimuli, while even item codes indicate left-tilted ones. 
    
    
 <values>
    \t/generatedSequence = "${ string }"
</values>
`

            return inquisitString

        }


        writeString( path = 'Sequence' ) {
            this._writeStringToFile( path )
        }


        writeInquisit( path = 'Sequence.iqx' ) {
            const format = 'inquisit'
            this._writeStringToFile( path, format )
        }


        _writeStringToFile( path, format = 'string' ) {

            let stringToPrint
            if( format === 'string' ) {stringToPrint = this.string()}
            if( format === 'inquisit' ) {stringToPrint = this.inquisitString()}

            fs.writeFileSync( path, stringToPrint, ( error, data ) => {
                if( error ) {
                    console.error( error )
                    return
                }
            } )
        }


        /**
         * Generates a sequence of semi-random digits. The sequence is not completely random, as there are two rules
         * adhered to during generation:
         *  1. A digit can be repeated maximum three times consecutively. This follows from the constraint #1 in
         *  Wiers et al. 2009: "No more than three consecutive stimuli of the same category."
         *  2. There should be no more than three consecutive odd or even digits. This follows the constraint #2 in
         *  in Wiers et al. 2009: "No more than three consecutive stimuli of the same format". In the iqx file, odd
         *  item codes indicate right-tilted stimuli, while even item codes indicate left-tilted ones:
         *      1: alcohol left-tilted
         *      2: alcohol right-tilted
         *      3: softDrink left-tilted
         *      4: softDrink right-tilted
         *      5: surrender left-tilted
         *      6: surrender right-tilted
         * @param members {Array} The members to generate the sequence from
         * @param proportions{Array}
         * @param sequenceLength{Number} The length of the sequence to be generated
         * @returns {Array}
         * @private
         */
        _generate( members = [ 1, 2, 3, 4, 5, 6 ], sequenceLength = 10, proportions ) {

            const inGroup = Sequence.inGroup

            const pool = _generatePool( members, sequenceLength, proportions )
            const sequence = generateSequenceFromPool( pool, sequenceLength )

            return sequence


            /**
             * Generate a pool of members to pick from
             * @param members {Array}
             * @param sequenceLength {Number}
             * @param proportions {Array}
             * @returns {Array}
             */
            function _generatePool( members, sequenceLength, proportions ) {
                const pool = []
                members.forEach( ( member, i ) => {
                    const subPoolLength = !!proportions
                        ? _.round( sequenceLength * proportions[ i ] )
                        : _.round( sequenceLength / members.length )  // if user provided no proportions, make them
                    // equal
                    const indexArray = _.range( subPoolLength )
                    indexArray.forEach( j => {
                        pool.push( member )
                    } )
                } )
                return pool
            }

            /**
             * Pick a member based on rules and add it to sequence
             * @param pool{Array}
             * @param sequenceLength{Number} The desired length of the output sequence
             * @returns {Array}
             *
             */
            function generateSequenceFromPool( pool, sequenceLength ) {

                const pickRandomMemberFromPool = () => _.sample( pool )
                const sequence = []
                const indexArray = _.range( sequenceLength )
                indexArray.forEach( ( i ) => {

                    let pickedMember = pickRandomMemberFromPool()
                    if( !pickedMember ) return
                    const thisIsNotTheFirstItemOfTheSequence = i > 1
                        // , poolIsNotEmpty = i <= pool.length
                        , itHasBeenPossibleToPickAMember = !!pickedMember
                    if( thisIsNotTheFirstItemOfTheSequence &&
                        // poolIsNotEmpty &&
                        itHasBeenPossibleToPickAMember ) {

                        // NOTE: Wrapping this while loop in a setTimeout block breaks tests
                        let numberOfAttempts = 0
                        if( numberOfAttempts > 100 ) return  // the if rules cannot be satisfied for too long, end the
                        // sequence generation
                        while (

                            numberOfAttempts <= 100 && (  // if not tried too hard already

                                //  Randomization rule #1: A digit's group can be repeated maximum three times
                                // consecutively
                                //  Randomization rule #2: There should be no more than three consecutive odd or even
                                // digits (odd and even encode left and right tilt)

                                // If the digit's group is repeated 2 times before already
                                (
                                    inGroup( pickedMember ) === inGroup( sequence[ i - 1 ] ) &&
                                    inGroup( pickedMember ) === inGroup( sequence[ i - 2 ] )
                                )
                                ||
                                // Or if the digit has been even or odd 2 times before already
                                (
                                    pickedMember % 2 === sequence[ i - 1 ] % 2 &&
                                    pickedMember % 2 === sequence[ i - 2 ] % 2
                                )
                            )

                            // Do this
                            ) {

                            // Re-roll
                            pickedMember = pickRandomMemberFromPool()
                            numberOfAttempts++
                        }

                    }

                    // Add picked member to the sequence
                    sequence.push( pickedMember )

                    // Remove the picked member from the pool
                    const indexOfPickedMember = _.indexOf( pool, pickedMember ) // can be the index of any member that
                    // has the same value with the picked one
                    _.pullAt( pool, indexOfPickedMember )

                } )
                return sequence
            }


        }


        /**
         * @static
         * Returns the name of the group (group1, group2, or group3) a digit belongs to, following the groups
         * in the .iqx file:
         *  1: alcohol left-tilted
         *  2: alcohol right-tilted
         *  3: softDrink left-tilted
         *  4: softDrink right-tilted
         *  5: surrender left-tilted
         *  6: surrender right-tilted
         * @param digit {Number}
         * @return {string}
         */
        static inGroup( digit ) {

            // TODO: Should be generic
            const groups = new Map( [
                [ 'group1', [ 1, 2 ] ],
                [ 'group2', [ 3, 4 ] ],
                [ 'group3', [ 5, 6 ] ]
            ] )

            let result = ''
            groups.forEach( ( members, groupName ) => {
                if( _.includes( members, digit ) ) {
                    result = groupName
                }
            } )

            return result

        }

    }




//// UMD FOOT ////////////////////////////////////////////////////////////////////////

    //// MODULE.EXPORTS ////
    exports.Sequence = Sequence

    Object.defineProperty( exports, '__esModule', { value: true } )

} ) ) )
//////////////////////////////////////////////////////////////////////////////////////





