// A node.js file to generate a sequence for DCBM

// Import modules
sequenceGenerator = require( './sequenceGenerator' )
const _ = require( '../libraries/lodash-4.17.15/lodash' )
const fs = require( 'fs' )

// Generate a sequence
const length = 200
    , smallest = 1
    , largest = 6
const sequence = new sequenceGenerator.Sequence( length, smallest, largest )

// TODO: This does nothing
// Write the sequence to file
const path = '../../Sequence.iqx'
// const path = '/Users/clokman/Library/Mobile' +
//     ' Documents/com~apple~CloudDocs/__Projects__/Code/Henk-Jan/Experiments/VBShared/DCBM/Sequence.iqx'
sequence.writeInquisit( path )

console.log( `Sequence generated: ${sequence.string()}`)
console.log(`Sequence saved in ${path}.`)
